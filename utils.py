#!/usr/bin/python

import sys

from config import *

SHIELD_DIR = sys.path[0] + '/' + SHIELDS
BANNER_DIR = sys.path[0] + '/banners'
SEQUENCE_DIR = sys.path[0] + '/sequences'
COUNTY_DIR = sys.path[0] + '/counties'
TEMPLATE_DIR = sys.path[0] + '/templates'

def get_network_dir(network):
    return '{0}/{1}'.format(SHIELD_DIR, network.upper() if ':' in network else network)

def get_template_path(template):
    return '{0}/{1}'.format(TEMPLATE_DIR, template)

def shield_name_stub(ref):
    return '{0}-{1}'.format(ref.network.upper(), ref.ref)

def get_cluster_dir(orientation):
    return '{0}/clusters:{1}/'.format(SHIELD_DIR, orientation)

def shield_name(refs, orientation):
    return get_cluster_dir(orientation) + '_'.join([ shield_name_stub(ref) for ref in refs ]) + '.png'

def shield_name_single(network, ref):
    return '{0}/{1}/{2}.png'.format(SHIELD_DIR, network.upper() if ':' in network else network, ref)
