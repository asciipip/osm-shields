OSM Shields
===========

This project contains the necessary programs to make a rendered map based
on OpenStreetMap data that includes shields that match roadside signs for
various road networks.


Prerequisites
-------------

A standard Mapnik rendering stack is assumed.  The database must be in the
osm2pgsql schema with slim mode tables.

On the system that has your database database host, you will need:
PL/Python, python-future, python-pkg-resources, and python-svgwrite.
You'll also need python-setuptools and fontforge to build the Python
package for shield rendering.  You must be running at least PostgreSQL
9.0.

If you want to use the supplied osm.xml file, you'll need to add the
`network` tag to your osm2pgsql default.style file so its low zoom level
rendering optimizations will work.  If you just want to use the supplied
functions in your own stylesheets, no default.style additions are
necessary.


OSM Data
--------

The shields are derived from OpenStreetMap route relations.  Each relation
should have a `network` tag, a `ref` tag, and possibly a `modifier` tag.
The shield style is chosen based on the `network` tag; see
`website/supported.html` for a list of networks understood by the
renderer.


Configuration
-------------

Make sure you've loaded the project's submodules:

    git submodule init
    git submodule update

Edit the `config.sql` file to specify the location of created shield
images (on the database host).  Make sure the database user has write
permission to the directory.

Run `make` to build the python package (in the `dist` directory).  Install
the package onto your database host using your preferred method for
installing Python wheel archives.  (`pip` works perfectly well.)

After creating your rendering database, import all of the files with a
.sql extension, preferably in this order:

    config.sql types.sql tables.sql functions.sql data.sql

Because some of the functions use PL/Python, which is an untrusted
language, you'll have to load the files as a database superuser.  If
you're not using the `planet_osm` table prefix, you'll have to edit some
of the files to change it before importing.  (Sorry.)


Creating Shields
----------------

The SQL functions will automatically create shields on the fly as they're
called.

### Sequence File Syntax

WARNING: Not used anymore.  This section is still here as a reference
until all of the shields are converted to the new system.

Each file is divided into blocks, where each block begins with the name of
a template file in square brackets.  Following that is a comma- and
line-delimited list of reference numbers to be filled into that template.
A series of sequential numbers can be represented as
`*<start>*..*<end>*`.  Thus a hypothetical `US:ST` network with numbers 1
through 10, 15, 27, and 500 could be represented as follows:

    [US:ST_2.svg]
    1..10, 15
    27
    
    [US:ST_3.svg]
    500

If a reference number has a suffix which must be placed independently of
the main number (such as the "A" in New Hampshire's Route 16A), add a
pound sign to the reference number followed by the suffix.  (So Route 16A
would be represented as `16#A`.)  If a pound sign follows a number range,
the suffix will be applied to every number in the range.  Just as with the
reference number, numeric suffixes can be specified as a range of numbers
with two dots between them, in which case each suffix in the range will be
applied to the reference number (or range of numbers) before the pound
sign.  The `ref_suffix` variable (described below) can be used to set a
suffix for an entire block.  The `suffix_separator` variable (described
below) controls the string matched against the `ref` tag during rendering.

You can set variables in each template block with the syntax
`*<variable>*=*<value>*`.  Each variable must be on a line by itself.  A
variable's value only applies to reference numbers in the same block and
after the variable line, so it's good practice to set variables at the top
of the block.  Currently-understood variables are:

* height_scale - a number which is multiplied by the default shield height
  to get the height for the current template.  Used for things like the US
  business route shields, which need extra vertical room for the banner or
  many Canadian provincial routes, which have taller shields than the US.

* shield_modifier - used for shield templates that allow for additional
  modifier text, usually for variant routes when the variant is shown on
  the main shield rather than a separate banner.  See Georgia's templates
  for an example and the generic county templates for a different use of
  the variable (in that case, county name).  The value of the variable can
  be set to the empty string (for "no text") with double quote marks.

* network_modifier - for routes that are variant routes (Alternate or
  similar), that variant goes here.  This variable is used for matching
  against OpenStreetMap data--it gets added to the base network--and
  should probably be accompanied by a banner variable (below), a
  shield_modifier (above) or a different shield template than the mainline
  routes.  Multiple modifiers (such as for US Route 17 Business Truck)
  should be separated by semicolons.  Order doesn't matter; matching will
  be performed against every possible ordering of the modifiers.

* ref_suffix - an alternate way of specifying a suffix which applies to an
  entire block.  Can be overridden by using the pound sign syntax
  (described above) on individual reference numbers.

* suffix_separator - a string which does not appear on the shield but
  which is placed between the main reference number and the suffix for
  purposes of matching the values of `ref` tags in the OpenStreetMap data.
  As an example, New Hampshire Route 16A works with the default value, the
  empty string, but it would have to be set to a dash for Texas Business
  State Highway 6-N.

* banner - name of a banner to attach to the top of the shield, like
  US:BUSINESS or US:TRUCK.  The banner must be created by one of the
  special banner sequence files (see below).  Multiple banners can be
  separated by semicolons.

* range_filter - a filter to apply to number ranges, so you can specify a
  range of numbers but only generate shields for a subset of that range.
  For example, separating numbers with ones in them from the rest is
  annoying to write out, but it's easy to do with a filter.  Does not
  affect numbers that are not in ranges.  Supported filters are:
  * has_ones - only numbers with at least one digit '1' will be selected.
  * no_ones - only numbers with no '1' digits will be selected.
  * empty - if the filter is empty (the default), all numbers in any given
    ranges will be selected.

* no_num - set to 'yes' if the template file does not have a %num%
  replacement.  This is used for unique shield designs like the New Jersey
  Turnpike and disables the check for replacement text.

* county - set to the name of the county for which the following numbers
  apply.  More than one county may be specified by separating them with
  semicolons.  The county name is inserted into the template under the
  %mod% replacement, so this variable is incompatible with the
  shield_modifier variable.

  If the network key is different from the county name (e.g. in Ohio,
  Belmont County has a network of US:OH:BEL), separate the key element
  from the county name with a vertical bar: `county=BEL|Belmont` .


Creating Banners
----------------

WARNING: Done differently now.  This section is still here as a reference
until all of the shields are converted to the new system.

Banners are images intended to be attached to shields to make variant
shields.  This is mainly for "bannered" routes like US 50 Truck.  Banner
images are generated from files in the `banners` directory.  The format of
these files is the same as for sequence files above, with the exceptions
that the default value of `height_scale` is 0.5, and the default value of
`no_num` is 'yes'.

The name used to refer to a banner in a sequence file is the "network"
name, followed by a colon, followed by the "ref" value.  Thus, if there's
an entry in the `banners/US` file as follows:

    [banner:truck.svg]
    TRUCK

then it would be referenced as `US:TRUCK`.

The division of banners into networks can be done arbitrarily.  Ours are
segmented according to the style of the banners, so the black-on-white
style used on US Highways and many state routes is under `US`, while
Louisiana's white-on-green banners are under `US:LA`.


Special Clusters
----------------

Very occasionally, a concurrence of two routes is signed with a single,
unique shield rather than just putting the two routes' shields next to
each other.  An example is the place in New Jersey where US 1 and US 9
overlap, which is signed as "US 1-9".  These replacements are stored in a
couple of auxiliary tables and a function, shields_add_replacement(), is
provided to add new occurrences of this phenomenon.  The data.sql file
preloads the tables will all the special concurrencies that we know about.


Rendering
---------

In your layer definitions, include a call to `shields_get_filename()` in
your SQL, something like this:

    SELECT ..., shields_get_filename(osm_id, way) AS route_shield
        FROM planet_osm_line
        WHERE ...

You can then use `[route_shield]` as the `file` parameter for Mapnik's
`ShieldSymbolizer`.  e.g. in Mapnik XML:

    <Rule>
      <MaxScaleDenominator>...</MaxScaleDenominator>
      <MinScaleDenominator>...</MinScaleDenominator>
      <Filter>...</Filter>
      <ShieldSymbolizer file="[route_shield]" fontset-name="..." placement="line" spacing="..." ></ShieldSymbolizer>
    </Rule>

In MapCSS, something like this:

    ... {
        [...] {
            shield-name: '';
            shield-file: url('[route_shield]');
            shield-placement: line;
            shield-spacing: ...;
        }
    }

If you want to use the sample rendering in this project:

Make sure your `config.py` file is set properly, then run
`generate_xml.py`.  The stylesheet is osm.xml.  You can test rendering
with `render_image.py`; run `render_image.py --help` for information on
calling it.
