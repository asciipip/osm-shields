#!/usr/bin/env python3

import multiprocessing as mp
import sys

import ttystatus

import shields

directory = sys.argv[1]

refs = {
    'CA:AB': {
        '': range(1, 300) + range(500, 900) + [
            '1A', '1X', '2A', '3A',
            '11A', '13A', '16A', '20A', '28A', '41A', '64A', '93A',
        ],
    },
    'MX:MX': {
        '': range(1, 310) + ['1D', '2D', '40D', '49D', '180D'],
    },
    'US:I': {
        '': range(1, 1000) + [
            '35E', '35W', '69C', '69E',
            'A1', 'A2', 'A3',
            'H1', 'H2', 'H3', 'H201',
            'PR1', 'PR2', 'PR3'],
        'Alternate': [75],
        'Future': range(1, 100) + [295, 840],
        'Spur': [270]
    },
    'US:I:Business:Loop': {
        '': [
            5,8,
            10,15,17,19,
            20,24,25,27,29,
            30,35,
            40,44,45,
            55,
            65,69,
            70,72,75,
            80,83,84,85,86,
            90,94,95,96,
            196,
            205,229,
            376,
        ],
    },
    'US:I:Business:Spur': {
        '': [
            10,
            20,29,
            35,
            44,
            75,
            80,89,
            90,96,
            126,196,
            375,385,
            495,
            526,585,
            696,
        ],
    },
    'US:I:Downtown:Loop': {
        '': [24, 229],
    },
    'US:I:Downtown:Spur': {
        '': [29, 90],
    },
    'US:US': {
        '' : range(1, 1000) + [
            '1-9',
            '1A', '2A', '6A', '6N', '9W',
            '11E', '11W', '14A', '16A', '17-1', '19E', '19W',
            '20A', '23A', '25A', '25E', '25W',
            '31A', '31E', '31EX', '31W',
            '41A', '45E', '45W', '49B', '49E', '49W',
            '62B', '62S', '63B', '64B', '64S', '65B', '67B', '67Y',
            '70A', '70B', '70N', '70S', '71B', '74A', '79B',
            '82B', '89A',
            '165B', '167B',
            '201A', '221A', '270B', '278B',
            '321A',
            '412B'],
        'Alternate': [
            1, 2, 4, 5, 6, 7, 9,
            11, 12, 13, 14, 16, 17, 19,
            20, 22, 23, 24, 25, 27,
            30, 31,
            40, 41, 45,
            50,
            60, 66, 67, 69,
            70, 71, 72, 74, 75, 77,
            89,
            90, 91, 92, 93, 95, 97, 98, 99,
            101, 104, 111, 113, 127, 129, 169,
            220, 240, 264, 221, 231, 281,
            301, 309, 322,
            422, 441, 460,
            541,
            611,
        ],
        'Alternate;Business': [66],
        'Alternate;Bypass': [70],
        'Business': range(1, 1000) + ['1A', '11E'],
        'Business;Truck': [17],
        'Bypass': [
            1, 4, 6,
            '10S', 13, 14, 16, 17,
            20, 21, 23, 25,
            30, 31,
            40, 45,
            50, 53,
            60, 61, 62, 64, 65, 66, 67,
            71, 75, 79,
            81, 83,
            91, 98, 99,
            101, 112, 127, 129, 131, 150, 183,
            230, 231, 259, 278, 281,
            301, 309, 341,
            401, 422, 441,
        ],
        'City': [6, 12, 20, 26, 30, 61, 166],
        'Scenic': [40, 412],
        'Spur': [21, 52, 62, 71, 83, 95, 97, 281],
        'Toll': [30, 41, 98, 278],
        'Truck': [
            1, 2, 9,
            11, 17, 19,
            20, 23, 25, 27,
            40,
            50, 52,
            60,
            76, 78, 79,
            80,
            90, 92, 95, 98,
            127, 166, 176,
            219, 220, 221, 222, 231, 281,
            301, 309, 322,
            441,
            521,
            601,
        ],
    },
    'US:AK': {
        '': range(1, 12) + [98],
    },
    'US:AL': {
        '': range(1, 300) + [604, 759],
    },
    'US:AR': {
        '': range(1, 400) + range(800, 900) + [
            '1B','1C','1S','1Y','4S','4B','7S','7B','7T','8S','9S','9B',
            '10S','11B','11S','14S','16S','18B','18S',
            '20S','23C','23S','25B','25S','26S','27B','29B',
            '32B','33B','33S','39S',
            '41B','43S',
            '58E','59B','59S',
            '60S','69B','69S',
            '72S','78S',
            '81S','83S','89S',
            '94S','95S','98B',
            '102B','106S','108S','112S','117S','119Y','127S','129B',
            '130S','133T','133S','137S','146S',
            '150S','163S','164S','175S','176Y',
            '201S','226S','235S','238S','239S','243S',
            '265S','267S','276S','280S','282S','295S',
            '304N','308B','308S','365S','321S','367S','384S',
            400,440,463,
            530,549,
            600,
            917,932,935,959,980,
        ],
        'Truck': ['75S'],
    },
    'US:AZ': {
        '': range(1, 100) + [
            '79B','89A','95S',
            143,169,177,179,'180A',181,186,187,188,189,195,
            210,238,261,260,264,266,273,277,286,287,288,289,
            347,366,373,377,386,387,389,
            473,
            564,587,
            801,802,
            989,
        ],
        'Loop': [101, 202, 303],
    },
    'US:CA': {
        '': range(1, 302) + [330, 371, 905],
    },
    'US:CO': {
        '': range(1, 400) + [402, 470],
    },
    'US:CT': {
        '': range(1, 400) + [
            '2A', '14A', '17A', '71A', '182A',
        ],
    },
    'US:DC': {
        '': [295],
    },
    'US:DE': {
        '': range(1, 100) + [
            '1A', '1B', '1D', '9A', '14A',
            100, 141,
            202, 261, 273, 286, 299,
            300,
            404, 491,
            896,
        ],
        'Alternate': [5, 10, 24, 30, 54, 404, 896],
        'Business': [1, 2, 404, 896],
        'Truck': [9, 14],
    },
    'US:FL': {
        '': range(1, 1000) + [
            'A1A','G1A',
            '2A','4A','5A','8A','9A','9B',
            '10A','14A','15A','15B','15C',
            'A19A',
            '24A','25A','26A',
            '30A','30E','39A',
            '50A',
            '60A',
            '76A',
            '93A',
            '109A','115A',
            '228A','263A','289A','298A',
            '367A','372A','372B','392A',
            '406A','426A','429A',
            '500A','520A','526A','528A','545A','580A','582A','582B','590A',
            '600A','611A','611B',
            '700A','704A','707A','712A','712B','721A',
            '811A','809A','824A','867A',
            '905A','907A','939A','939B','945A',
            4080,
            4081,
            5054,5056,5098,
            9336,9823,
            'Florida\'s Turnpike',
            'Homestead Extension of Florida\'s Turnpike',
        ],
    },
    'US:FL:Toll': {
        '': [
            91,
            112,
            281,293,
            408,414,417,429,451,
            528,568,570,589,
            618,
            836,869,874,878,
            924,
            'Florida\'s Turnpike',
            'Homestead Extension of Florida\'s Turnpike',
        ],
    },
    'US:GA': {
        '': range(1, 500) + [515, 520],
        'Alternate': [3,4,7,14,15,17,52,74,75,80,85,91,120,255],
        'Business': [1,4,6,9,10,11,17,21,24,27,35,38,52,53,60,87],
        'Bypass': [3,11,12,17,24,34,62,67,73,121,135],
        'Connector': [
            1,3,7,11,13,14,15,17,19,
            21,22,25,
            31,32,33,37,38,39,
            40,41,42,43,45,46,47,49,
            52,53,54,57,
            60,
            76,77,
            85,87,88,
            90,97,
            104,119,120,122,135,136,141,149,154,162,166,197,
            206,230,240,247,
            300,
        ],
        'Loop': [1,10,15,73,120],
        'Spur': [
            7,
            14,18,
            21,20,22,23,24,25,
            38,39,
            40,42,44,
            53,54,56,
            60,
            77,
            82,85,
            91,97,
            109,119,137,138,144,151,154,169,180,192,199,
            204,242,247,253,270,273,
            302,
        ],
        'Toll': [400],
    },
    'US:HI': {
        '': range(1, 1000) + [
            '32A','32B',
            1370,1970,
            2000,
            3000,3400,3500,3800,
            7012,7101,7110,7141,7241,7310,7345,7350,7351,7401,7413,7414,7415,7439,7601,
            8930,
        ],
    },
    'US:IA': {
        '': range(1, 500) + range(900, 1000),
    },
    'US:ID': {
        '': range(1, 100) + [128, 162, 167, 200],
    },
    'US:IL': {
        '': range(1, 200) + [
            '4A',
            '42A',
            '59A',
            '70A',
            '83A', '88A', '89A', '89B', '89C',
            '94A', '94B', '95A', '97A',
            '113N', '113S', '116A', '127A', '130A', '142A',
            203, 213, 226, 242, 251, 250, 255, 267,
            316, 336, 351, 394,
            594,
        ],
    },
    'US:IN': {
        '': range(1, 600) + [
            641, 645, 650, 662, 727, 827, 912, 930, 933,
            'Indiana Toll Road',
        ],
    },
    'US:KS': {
        '': range(1, 400) + ['8N', '8S', 'Kansas Turnpike'],
    },
    'US:KY': {
        '': range(1, 1000) + range(1000, 4000) + range(6000, 6500) + ['AA'],
        'Spur': [8, 15],
    },
    'US:KY:Parkway': {
        '': [
            'Audubon Parkway',
            'Bluegrass Parkway',
            'Martha Layne Collins Blue Grass Parkway',
            'Cumberland Parkway',
            'Louie B. Nunn Cumberland Parkway',
            'Hal Rogers Parkway',
            'Mountain Parkway',
            'Bert T. Combs Mountain Parkway',
            'Natcher Parkway',
            'William H. Natcher Parkway',
            'William H. Natcher Green River Parkway',
            'Pennyrile Parkway',
            'Edward T. Breathitt Pennyrile Parkway',
            'Purchase Parkway',
            'Julian M. Carroll Purchase Parkway',
            'Western Kentucky Parkway',
            'Wendell H. Ford Western Kentucky Parkway',
        ],
    },
    'US:LA': {
        '': range(1, 3400),
    },
    'US:MA': {
        '': range(1, 200) + [
            '1A', '2A', '3A', '5A', '6A', '7A', '8A', '9A', 'C1',
            '20A', '28A', '32A',
            '110A', '114A', '115A',
            203,204,209,
            213,
            220,225,228,
            240,
            286,
            295,
            '122A', '127A', '129A',
            '146A',
            '109A',
            '128A',
        ],
    },
    'US:MD': {
        '': range(1, 1000) + [
            '2-4', '835A',
            'Historic National Road Scenic Byway',
            'Mountain Maryland Scenic Byway',
            'Chesapeake and Ohio Canal Scenic Byway',
            'Antietam Campaign Scenic Byway',
            'Catoctin Mountain Scenic Byway',
            'Old Main Streets Scenic Byway',
            'Mason and Dixon Scenic Byway',
            'Falls Road Scenic Byway',
            'Horses and Hounds Scenic Byway',
            'Lower Susquehanna Scenic Byway',
            'Charles Street Scenic Byway',
            'National Historic Seaport Scenic Byway',
            'Star-Spangled Banner Scenic Byway',
            'Booth\'s Escape Scenic Byway',
            'Roots and Tides Scenic Byway',
            'Religious Freedom Tour Scenic Byway',
            'Chesapeake Country Scenic Byway',
            'Harriet Tubman Underground Railroad Scenic Byway',
            'Blue Crab Scenic Byway',
        ],
        'Alternate': [404],
        'Business': [3, 5, 30, 355, 404],
        'Bypass': [45],
        'Truck': [2, 3, 222, 274],
    },
    'US:ME': {
        '': range(3, 239) + [
            '4A','9A','9B','11A',
            '26A',
            '100A','102A','166A',
        ],
        'Business': [25, 137],
    },
    'US:MI': {
        '': range(1, 240) + [247, 294, 553],
        'Business': [28, 32, 60],
        'Connector': [13, 44, 125],
    },
    'US:MI:Leelanau': {
        '': range(600, 700),
    },
    'US:MN': {
        '': range(1, 400) + [610],
        'Business': [371],
    },
    'US:MO': {
        '': range(1, 300) + [
            '64A', '64B',
            340,350,360,364,366,367,370,371,376,
            413,465,
            571,
            740,744,752,759,763,765,799,
        ],
    },
    'US:MO:Supplemental': {
        '': [
            'A','AA','AB','AC','AD','AE','AF','AH','AJ','AK','AM',
            'AN','AO','AP','AR','AT','AV','AU','AW','AX','AY','AZ',
            'B','BA','BB',
            'C','CC',
            'D','DD',
            'E','EE',
            'F','FF',
            'H','HH',
            'J','JJ',
            'K','KK',
            'M','MM',
            'N','NN',
            'O','OO',
            'P','PP',
            'R','RR','RB',
            'T','TT',
            'U','UU',
            'V','VV',
            'W','WW',
            'X','XX',
            'Y','YY',
            'Z','ZZ',
        ],
    },
    'US:MS': {
        '': range(1, 1000),
    },
    'US:MT': {
        '': range(1, 100) + [
            117,135,141,146,
            200,'200S',287,
        ],
    },
    'US:MT:Secondary': {
        '': range(201, 287) + range(288, 600),
    },
    'US:NC': {
        '': range(1, 300) + [
            '226A',
            304,305,306,307,308,343,344,345,381,
            400,403,410,411,461,481,
            522,540,561,581,
            610,615,690,694,
            700,704,705,710,711,731,740,742,751,770,772,
            801,
            901,902,903,904,905,
        ],
    },
    'US:ND': {
        '': range(1, 100) + [
            200, 210, 256, 281, 294, 297,
            810,
            1804, 1806,
        ],
    },
    'US:NE': {
        '': range(1, 140) + ['25A', 250, 370],
        'Link': [
            '1E','6A','7E',
            '10B','10C','10D','10E','17B','17C','17E','17F','17J',
            '20A','23D','24A','24B','24D','25A','25B','26D','28B','28E','28H','28K',
            '31D','34H',
            '40C','41D','44C','45B',
            '50A','51A','51B','51C','53A','53B','53C','53E','55K','55W','55X','56C','56D','56G','59B',
            '61D','62A','63A','67E',
            '76E','79E',
            '80E','80F','80G','80H','82A','85F',
            '91D','93B','93E',
        ],
        'Spur': [
            '1A','1B','1C','1D','2B','4A','5A','8A','9A',
            '11A','12A','12B','12C','12D','12E','12F','13A','13C','13D','13F','13H','13K','14A','14B','14C','14H','15A','16A','16B','16F','18A','18B','18C','18D','18E','18F','18G','19A','19B','19C',
            '21A','21B','21C','23A','24C','26A','26B','26E','27A','27D','28J',
            '30B','30C','30D','30H','31A','31B','31C','34A','34B','34C','34D',
            '40D','41A','41B','41C','42A','43A','45A','47A','48A','49A','49B','49C',
            '54A','54B','54D','55A','55B','55C','55D','55E','55F','55G','55H','55J','55M','56A','57A',
            '61A','64A','64B','64E','64G','65A','66A','66C','66D','66E','67A','67B','67C',
            '70A','71A','71B','71C','71F','76A','76C','76D','78B','78C','78D','78E','78F','78H','78J','79H',
            '80B','80C','80D','85A','85B','85C','85D','85E','85H','86A','86B','87A','87B','89A',
            '90A','90B','91A','91B','93A','93C','93D','93F',
        ],
    },
    'US:NE:Rec': {
        '': [
            '2A','2D','7B','9B','10H',
            '13L','13M','13N','13P','15B','15C','16C','16D',
            '23E','23F','23G','27B','27C','28C',
            '32A','34J','34K','34L','35A','35B','37A',
            '40E','44A','44B','48B',
            '50B','50C','51E','51F','51G','51H','54C','54F','55N','55P','55R','55T','55U','55V','56E','56F',
            '62B','62F','64F','64H','67D','69A',
            '71G','73A','78K',
            '82B','85G','88B','89B',
            '92A',
        ],
    },
    'US:NH': {
        '': range(10, 200) + [
            '1A','1B','3A',4,'4A',9,'9A',
            '10A',
            '11A','11B','11C','11D',
            '12A',
            '16A','16B',
            '25A','25B','25C',
            '28A',
            '101A','101B','101C','101D',
            '103A','103B',
            '107A',
            '109A',
            '110A','110B',
            '111A',
            '113A',
            '114A',
            '115A',
            '121A',
            '155A',
            '175A',
            '202A',
            236,
            286,
        ],
    },
    'US:NJ': {
        '': range(1, 200) + [
            '21A',
            208, 284,
            303, 324, 347,
            413, 439, 440, 495,
            'Garden State Parkway',
            'PIP',
            'Palisades Interstate Parkway',
            'Atlantic City Expressway',
            'New Jersey Turnpike',
            'Atlantic City-Brigantine Connector',
        ],
        'Alternate': [47],
        'Business': [33],
        'Bypass': [33],
    },
    'US:NJ:CR': {
        '': range(500, 600),
        'Alternate': [511, 524, 527, 553, 559, 561],
        'Spur': [506, 508, 524, 526, 533, 536, 549, 550, 551, 552, 577],
        'Truck': [541, 557],
    },
    'US:NJ:Bergen': {
        '': range(1, 100) + [
            'C-2', 'C-3', 'C-4',
            'C-21', 'C-22',
            'S-25', 'S-29',
            'S-30', 'S-31', 'S-32', 'S-33', 'S-37', 'S-39',
            'S-40', 'S-43', 'S-48', 'S-49',
            'S-51', 'S-55', 'S-56', 'S-59',
            'S-61', 'S-68', 'S-69',
            'S-70', 'S-73', 'S-76', 'S-79',
            'S-80', 'S-81', 'S-85', 'S-87', 'S-89',
            'S-91', 'S-93', 'S-94',
            'S-104', 'S-106', 'S-109',
            'S-110', 'S-114',
            'S-124',
        ],
    },
    'US:NJ:Monmouth': {
        '': range(1, 100) + [
            '6A', '8A', '8B',
            '12A', '13A', '13B',
            '23A', '29A',
            '40A',
        ],
    },
    'US:NM': {
        '': range(1, 700) + [1113, 5001, 6563],
    },
    'US:NV': {
        '': range(100, 900) + [28, 88],
    },
    'US:NY': {
        '': range(1, 450) + [
            '3A',
            '5A', '5B', '5S',
            '6N',
            '7A', '7B',
            '9A', '9B', '9D', '9G', '9H', '9J', '9L', '9N', '9P', '9R',
            '11A', '11B', '11C',
            '10A',
            '12A', '12B', '12D', '12E', '12F',
            '13A',
            '14A',
            '15A',
            '17A', '17B', '17C', '17K', '17M',
            '18F',
            '19A',
            '31A', '31E', '31F',
            '41A',
            '22A', '22B',
            '23A', '23B',
            '25A', '25B',
            '27A',
            '28A', '28N',
            '29A',
            '30A',
            '32A',
            '33A',
            '34B',
            '37B', '37C',
            '38A', '38B',
            '52A',
            '54A',
            '55A',
            '69A',
            '85A',
            '96A','96B',
            '100A', '100B', '100C',
            '104A', '104B',
            '120A',
            '146A',
            '157A',
            '170A',
            '248A',
            '252A',
            '365A',
            454, 458, 470, 474, 481, 488, 495,
            531, 590, 598,
            631, 635, 690, 695,
            747, 787,
            812, 825, 840, 878, 890,
            '911F','914V',
            '961F',
            '962J',
            '990L','990V',
            'B', 'Bethpage State Parkway',
            'BMP', 'Bear Mountain State Parkway',
            'BP', 'Belt Parkway',
            'BRP', 'Bronx River Parkway',
            'CCP', 'Cross Country Parkway',
            'CIP', 'Cross Island Parkway',
            'FDR', 'FDRD', 'FDR Drive', 'Franklin D. Roosevelt East River Drive',
            'GCP', 'Grand Central Parkway',
            'H', 'Heckscher State Parkway',
            'HHP', 'Henry Hudson Parkway',
            'HRD', 'Harlem River Drive',
            'HRP', 'Hutchinson River Parkway',
            'JRP', 'Jackie Robinson Parkway',
            'Korean War Veterans Parkway',
            'L', 'Loop Parkway',
            'LOSP', 'Lake Ontario State Parkway',
            'LaSalle Expressway',
            'Lake Welch Parkway',
            'M', 'Meadowbrook State Parkway',
            'MP', 'Mosholu Parkway',
            'N', 'Northern State Parkway',
            'O', 'Ocean Parkway',
            'PIP', 'Palisades Interstate Parkway',
            'PP', 'Pelham Parkway', 'Bronx and Pelham Parkway',
            'RM', 'Robert Moses Causeway',
            'RMSP', 'Robert Moses State Parkway',
            'SA', 'Sagtikos State Parkway',
            'SBP', 'Sprain Brook Parkway',
            'SM', 'Sunken Meadow State Parkway',
            'SMP', 'Saw Mill River Parkway',
            'SO', 'Southern State Parkway',
            'Seven Lakes Drive',
            'TSP', 'Taconic State Parkway',
            'W', 'Wantagh State Parkway',
        ],
    },
    'US:NY:Albany': {
        '': range(1, 500),
    },
    'US:NY:Cattaraugus': {
        '': range(1, 100) + ['86-1', '86-2'],
    },
    'US:NY:Columbia': {
        '': range(1, 100) + [
            '5A', '7A', '7D', '8A',
            '21C', '25A', '26A', '28A', '28B',
        ],
    },
    'US:NY:Dutchess': {
        '': range(1, 200),
    },
    'US:NY:Erie': {
        '': range(1, 600),
    },
    'US:NY:Fulton': {
        '': range(1, 200) + ['131A', '142A'],
    },
    'US:NY:Montgomery': {
        '': range(1, 200),
    },
    'US:NY:Saratoga': {
        '': range(1, 200) + [338, 339, 1345],
    },
    'US:NY:Schenectady': {
        '': range(1, 200),
    },
    'US:NY:Rensselaer': {
        '': range(1, 200),
    },
    'US:NY:Washington': {
        '': range(1, 200) + [
            '6A', '6B', '7A', '9A', '9B',
            '11A', '12A', '17A', '18A',
            '23A', '59A', '70A', '74A',
        ],
    },
    'US:OH': {
        '': range(1, 900) + [
            '2C','7T',
            '10C','14T','44C','48T','59T','61C','79A','83C','95A',
            '151J','150A','176J',
            '204A','241J',
            '315C','338J',
            '424C',
            '511C','518J',
            '664J','669C','669J','690J',
            'Ohio Turnpike',
            'James W. Shocknessy Ohio Turnpike',
            ],
        'Alternate': [7, 49],
        'Business': [7],
        'Bypass': [4],
        'Truck': [12, 13, 38, 73, 430, 547],
    },
    'US:OH:FUL': {
        '': range(1, 35) + [
            'A','B','C','D','E','F','G','H','I','J','K','L','M',
            'N','O','P','Q','R','S','T','U','V','W','X','Y','Z',
            'AC','DE','EF','FG','HJ','MN','NR','RS'
        ],
    },
    'US:OH:SHE': {
        '': range(1, 300) + ['25A'],
    },
    'US:OH:MOE': {
        '': range(1, 100),
    },
    'US:OH:PAU': {
        '': range(1, 400) + range(1000, 1100),
    },
    'US:OK': {
        '': range(1, 200) + [
            '3A','3B','3E','3W','5A','4B','5C','7A','7D','8A','8B','9A',
            '10A','10C','11A','17A','18A','18B','19C','19D',
            '28A','29A',
            '31A','31B','34A','34B','34C',
            '40A','47A','48A',
            '50A','50B','51A','51B','53A','54A','54B','58A','59A','59B',
            '63A','64B','64D','69A',
            '70A','70B','70C','70D','70E','70F','74A','74B','74C','74D','74E',
            '74F','77C','77D','77H','77S',
            '80A','82A','85A',
            '98S','99A','99C',
            '117A',
            209,'251A','259A',266,270,'271A','281A',
            325,
            '412A','412B','412P',
        ],
    },
    'US:OK:Turnpike': {
        '': [
            'CHEROKEE TURNPIKE',
            'CHICKASAW TURNPIKE',
            'CIMARRON TURNPIKE',
            'CREEK TURNPIKE',
            'H. E. BAILEY TURNPIKE',
            'INDIAN NATION TURNPIKE',
            'KILPATRICK TURNPIKE',
            'MUSKOGEE TURNPIKE',
            'TURNER TURNPIKE',
            'WILL ROGERS TURNPIKE',
        ],
    },
    'US:OR': {
        '': range(1, 600) + ['42S','86S','99E','99W','104S','422S'],
        'Business': [18, '99E', 126],
    },
    'US:PA': {
        '': range(1, 1000) + [
            '3A',
            '5A', '5B', '5S', 
            '6N',
            '7A', '7B',
            '9A', '9B', '9D', '9G', '9H', '9J', '9L', '9N', '9P', '9R',
            '35E', '35W',
            'Pennsylvania Turnpike',
        ],
        'Alternate': [5, 66],
        'Business': [8, 62, 66, 309],
        'Truck': [
            8, 27, 28, 42, 45, 51, 56, 61, 88,
            100, 148,
            286,
            304, 341,
            441,
            641, 653,
            711, 770,
            851,
            997,
        ],
    },
    'US:PA:Belt': {
        '': [
            'Red',
            'Orange',
            'Yellow',
            'Green',
            'Blue'
        ],
    },
    'US:PA:Turnpike': {
        '': [43, 66, 576, 'Pennsylvania Turnpike'],
    },
    'US:RI': {
        '': [
            '1A',
            2,3,4,5,7,
            10,12,14,15,
            24,
            33,37,
            51,
            77,78,
            81,
            91,94,96,98,99,
            100,101,102,103,'103A',104,107,108,
            110,112,113,114,'114A',115,116,117,'117A',118,
            120,121,122,123,126,128,
            136,138,'138A',
            146,'146A',
            152,
            165,
            177,179,
            214,216,238,246,
            401,402,403,
        ],
    },
    'US:SC': {
        '': range(1, 1000),
    },
    'US:SD': {
        '': range(1, 200) + [
            '15A','15Y','16B','19A','20A','24A','37A','38A','40A','47W',
            203,204,
            224,
            230,232,235,236,238,239,
            240,244,245,247,248,249,
            251,253,258,
            262,264,
            271,
            273,
            296,298,
            314,324,365,377,391,
            407,423,435,437,439,445,471,473,
            514,
            1804,1806,
        ],
    },
    'US:TN': {
        '': range(1, 500) + [840],
    },
    'US:TN:Secondary': {
        '': range(1, 477),
    },
    'US:TX': {
        '': range(1, 366) + [495, 550, 824, 'OSR'],
        'Business': [
            6,'6-N','6-R','6-S',
            7,'7-B',

            19,'19-J',
            21,'21-H',
            24,'24-B','24-D',
            31,'31-H',
            35,'35-C','35-E','35-H','35-L','35-M',
            36,'36-E','36-J',
            37,'37-C','37-E',
            44,'44-C','44-D',
            46,'46-C',
            64,'64-E',
            66,'66-D',
            70,'70-G',
            71,'71-E','71-F',
            72,'72-B',
            78,'78-D','78-E','78-F','78-G',

            101,'101-B',
            105,'105-T',
            114,'114-B','114-H','114-J','114-K','114-L',
            121,'121-D','121-H',
            123,'123-B','123-D',
            146,'146-D','146-E',
            152,'152-B',
            158,'158-B',
            171,'171-E',

            208,'208-B',
            214,'214-A',
            224,'224-B',
            249,'249-B',
            286,'286-A',
            288,'288-B',
            289,'289-C','289-D',

            349,'349-C',
            359,'359-B',
        ],
    },
    'US:TX:Beltway': {
        '': [8],
    },
    'US:TX:FM': {
        '': range(1, 4000),
        'Business': [
            1187,'1187-C',
            1431,'1431-J',
            1960,'1960-A',
        ],
    },
    'US:TX:Loop': {
        '': range(1, 600) + [635, 820, 1604],
    },
    'US:TX:NASA': {
        '': [1],
    },
    'US:TX:Park': {
        '': range(1, 77) + [100, 1836],
    },
    'US:TX:RM': {
        '': range(1, 4000),
    },
    'US:TX:Recreational': {
        '': range(1, 12) + [255],
    },
    'US:TX:Spur': {
        '': range(1, 602) + [729],
    },
    'US:TX:Toll': {
        '': [1, 8, 45, 49, 99, 121, 122, 130, 161, '183A', 255, 550],
    },
    'US:UT': {
        '': range(1, 321) + [900, 901, '181A', '182A', '183A', '184A'],
    },
    'US:VA': {
        '': range(1, 421) + [
            '6Y',
            '132Y', '180Y',
            '205Y',
            '300Y',
            457,
            510,
            598, 599,
            785,
            895,
        ],
        'Alternate': [16, 39, 57, 220, 258, 259, 337],
        'Business': [3, 7, 10, 20, 40, 42, 91, 168, 207, 234],
        'Bypass': [156],
    },
    'US:VA:Secondary': {
        '': range(600, 10000),
    },
    'US:VT': {
        '': range(1, 200) + [
            '2A', '2B', '4A', '5A', '7A', '7B', '8A',
            '10A', '12A', '15A', 
            '22A', '25A', '25B',
            '44A',
            '67A',
            207,
            215,
            225,
            232,235,236,
            242,243,244,
            253,
            279,
            289,
            315,
            346,
            'F-5',
        ],
        'Alternate': [122],
        'Connector': [127],
    },
    'US:WA': {
        '': range(1, 600) + [
            702,704,706,
            821,823,
            900,902,903,904,906,
            970,971,
        ],
    },
    'US:WI': {
        '': range(1, 200) + [
            213, 241, 243, 253,
            310, 312, 341, 351,
            441,
            794,
        ],
        'Business': [11, 13, 16, 23, 29, 35, 40, 42],
        'Spur': [42, 794],
    },
    'US:WV': {
        '': range(1, 200) + [
            210, 211, 214, 218,
            230,
            251, 252, 259,
            270, 273, 279,

            305, 307, 310, 311, 331,
            480,
            501, 527, 598,
            601, 612, 618, 622, 635,
            705,
            807, 817, 869, 891, 892,
            901, 956, 971, 972,
        ],
        'Alternate': [10, 27, 34],
    },
    'US:WY': {
        '': range(1, 500) + [530, 585, 789],
    },
}

for county in 'DuPage;Kane;Macon'.split(';'):
    refs['US:IL:' + county] = {'': range(1, 100)}
for county in 'Cook;Lake;McHenry'.split(';'):
    partial_refs = []
    for letter in 'ABTV':
        for num in range(1, 100):
            partial_refs.append(letter + str(num))
    refs['US:IL:' + county] = {'': partial_refs}
for county in 'Atlantic;Burlington;Camden;Cape May;Cumberland;Essex;Gloucester;Hudson;Mercer;Middlesex;Morris;Ocean;Passaic;Somerset;Sussex;Union;Warren'.split(';'):
    refs['US:NJ:' + county] = {'': range(600, 900)}
for county in 'Allegany;Broome;Chautauqua;Chemung;Chenango;Madison;Orange;Rockland;Warren'.split(';'):
    refs['US:NY:' + county] = {'': range(1, 100)}
for county in 'COL;JEF;MAH;OTT;SUM;TUS;BEL;GUE;HEN;WIL;NOB'.split(';'):
    refs['US:OH:' + county] = {'': range(1, 1000) + ['22A']}
for county in 'AUG;FAI;HAS;HOC;HOL;KNO;LAW;LOG;MRW;PER;WAY'.split(';'):
    refs['US:OH:' + county] = {'': range(1, 1000) + ['26A', '94A', '144A']}
for county in 'MRW:Harmony;LOG:Liberty;LOG:Monroe;HOL:Paint;MRW:South_Bloomfield;LOG:Pleasant'.split(';'):
    refs['US:OH:' + county] = {'': range(1, 1000) + ['21A']}
    
# Berkeley County, WV
county_refs = list(range(1, 100))
for x in range(1, 41):
    for y in range(1, 58):
        county_refs.append('{}:{}'.format(x, y))
for x, yl in [(45, 31), (51, 19), (81, 7)]:
    for y in range(1, yl+1):
        county_refs.append('{}:{}'.format(x, y))
refs['US:WV:Berkeley'] = {'': county_refs}

# Preston County, WV
county_refs = list(range(1, 100))
for x in range(1, 100):
    for y in range(1, 100):
        county_refs.append('{}:{}'.format(x, y))
refs['US:WV:Preston'] = {'': county_refs}


pool = mp.Pool()
results = []

for network, network_refs in refs.items():
    for modifier, refs in network_refs.items():
        for ref in refs:
            r = pool.apply_async(shields.render_and_save, (network, modifier, str(ref), directory))
            results.append((r, network, modifier, ref))

ts = ttystatus.TerminalStatus(period=0.1)
ts.format('%ElapsedTime() %String(network) %String(ref) %String(mod) %ProgressBar(done,total) ETA: %RemainingTime(done,total)')
ts['done'] = 0
ts['total'] = len(results)
for result, network, modifier, ref in results:
    ts['network'] = network
    ts['mod'] = modifier
    ts['ref'] = ref
    result.wait()
    ts['done'] += 1
ts.finish()
