CREATE EXTENSION plpython3u;


------
-- Configuration
--

CREATE OR REPLACE FUNCTION shields_set_python_dirs()
RETURNS VOID
LANGUAGE plpython3u
AS $$
  GD['image_dir'] = '/srv/shields'
$$;
SELECT shields_set_python_dirs();
