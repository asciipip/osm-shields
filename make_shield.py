#!/usr/bin/env python3

import shields

import argparse

parser = argparse.ArgumentParser()
parser.add_argument('-n', '--network', required=True)
parser.add_argument('-r', '--ref', required=True)
parser.add_argument('-m', '--modifier', default='')
parser.add_argument('-s', '--style', default='guide')
parser.add_argument('filename')
args = parser.parse_args()

drawing = shields.render(args.network, args.modifier, args.ref, args.style)
if drawing is None:
    print('Could not generate shield image.')
else:
    drawing.saveas(args.filename)
