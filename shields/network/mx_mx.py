#!/usr/bin/python

# http://www.sct.gob.mx/normatecaNew/wp-content/uploads/2014/11/SCT_NIS_0419.pdf#page=120
#
# Measurements:
#  | Purpose          | H  | T  | Highway |  Toll |
#  |------------------+----+----+---------+-------|
#  | width            | A  | A  |      45 |    45 |
#  | height           | B  | B  |      60 |    60 |
#  | diagonal_width   | a  | a  |       6 |     6 |
#  | upper_arc_width  | b  | b  |    16.5 |  16.5 |
#  | diagonal_height  | c  | c  |       6 |     6 |
#  | side_line_height | d  | d  |      12 |    12 |
#  | side_arc_height  | e  | e  |      11 |    11 |
#  | lower_arc_height | f  | f  |      30 |    30 |
#  | ref_height       | g  | g  |      20 |    20 |
#  | ref_offset       | h  | h  |       7 |     3 |
#  | mx_margin        | i  | i  |    1.75 |  1.75 |
#  | mx_height        | j  | j* |      10 |    10 |
#  | d_height         |    | j* |         |    10 |
#  | mx_offset        | k  | k  |     4.5 |   4.5 |
#  | lower_arc_radius | R1 | R1 |   29.25 | 29.25 |
#  | side_arc_radius  | R2 | R2 |       9 |     9 |
#  | upper_arc_radius | R3 | R3 |      15 |    15 |
#  | margin           |    |    |       1 |     1 |
#  | border_width     |    |    |       1 |     1 |
#  |------------------+----+----+---------+-------|
#  | ref_series, 1D   |    |    |       1 |     1 |
#  | ref_series, 2D   |    |    |       4 |     4 |
#  | ref_series, 3D   |    |    |       5 |     5 |
#  | mx_series        |    |    |       4 |     4 |


from ..geometry import *
from ..shieldbase import ShieldBase
from ..utils import *

def register(network_registry, style):
    network_registry['MX:MX'] = lambda m,r: MXEscudoFederal(m, r).make_shield()

class MXEscudoFederal(ShieldBase):
    def __init__(self, modifier, ref):
        super(MXEscudoFederal, self).__init__(modifier, ref)
        self.toll = self.ref.endswith('D')
        
        self.scale = 1/2.54
        
        if len(self.ref) <= 1:
            self.ref_series = 'D' # should be "serie 1"
        elif len(self.ref) == 2:
            self.ref_series = 'C' # should be "serie 4"
        else:
            self.ref_series = 'B' # should be "serie 5"
        self.mx_series = 'C' # should be "serie 4"
        self.d_series = self.mx_series

        self.margin = 1.0
        self.border_width = 1.0
        
        self.width = 45.0  # A
        self.height = 60.0 # B

        self.diagonal_width = 6.0    # a
        self.upper_arc_width = 16.5  # b
        self.diagonal_height = 6.0   # c
        self.side_line_height = 12.0 # d
        self.side_arc_height = 11.0  # e
        self.lower_arc_height = 30.0 # f
        self.ref_height = 20.0       # g
        if self.toll:
            ref_offset = 3.0         # h
        else:
            ref_offset = 7.0         # h
        mx_margin = 1.75             # i
        self.mx_height = 10.0        # j
        mx_offset = 4.5              # k
        self.mx_y = mx_offset + mx_margin
        self.divider_y = self.mx_y + self.mx_height + mx_margin + self.border_width/2
        self.ref_y = self.divider_y + self.border_width/2 + ref_offset

        self.lower_arc_radius = 29.25 # R1
        self.side_arc_radius = 9.0    # R2
        self.upper_arc_radius = 15.0  # R3

        self.calc_arc_params()
        text_bound_side_arc = self.side_arc.add_radius(self.margin + self.border_width + self.margin)
        text_bound_lower_arc = self.lower_arc.add_radius(-(self.margin + self.border_width + self.margin))
        
        self.ref_x = self.margin + self.border_width + self.margin
        self.ref_width = self.width - self.ref_x * 2
        self.mx_x = self.margin + self.border_width + self.margin
        self.mx_width = self.width - self.mx_x * 2

        if self.toll:
            self.d_y = self.ref_y + self.ref_height + ref_offset
            self.d_height = self.mx_height
            self.d_x = text_bound_lower_arc.point_on_circle(y=self.d_y + self.d_height)[0].real
            self.d_width = self.width - self.d_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'mx_mx'

    def calc_arc_params(self):
        upper_arc_right = self.width/2 + 0j
        upper_arc_left = upper_arc_right - self.upper_arc_width
        self.upper_arc = Circle.from_chord_and_radius(
            upper_arc_left, upper_arc_right, self.upper_arc_radius)

        side_arc_bottom = 0 + (self.height - self.lower_arc_height) * 1j
        side_arc_top = side_arc_bottom - self.side_arc_height * 1j
        self.side_arc = Circle.from_chord_and_radius(
            side_arc_bottom, side_arc_top, self.side_arc_radius)

        lower_arc_bottom = self.width/2 + self.height * 1j
        self.lower_arc = Circle.from_chord_and_radius(
            side_arc_bottom, lower_arc_bottom, self.lower_arc_radius + self.border_width + self.margin)
        
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        border_offset = self.margin + self.border_width / 2
        
        outer_p1 = 0 + self.diagonal_height * 1j
        outer_p2 = self.diagonal_width + 0j
        diagonal_line = Line.from_points(outer_p1, outer_p2).translate(unit_vector(outer_p1, outer_p2) * -1j * border_offset)
        upper_arc_left = Circle(self.upper_arc.center, self.upper_arc.radius + border_offset)
        upper_arc_right = Circle(upper_arc_left.center + self.upper_arc_width, upper_arc_left.radius)
        ul_corner_left = unpoint(diagonal_line.point_on_line(x=border_offset))
        ul_corner_right = unpoint(upper_arc_left.intersect_line(diagonal_line)[0])
        center_peak = unpoint(upper_arc_left.intersect_circle(upper_arc_right)[0])
        ur_corner_left = (self.width - ul_corner_right[0], ul_corner_right[1])
        ur_corner_right = (self.width - ul_corner_left[0], ul_corner_left[1])

        side_arc = Circle(self.side_arc.center, self.side_arc.radius + border_offset)
        l_side_arc_top = unpoint(side_arc.point_on_circle(x=border_offset)[0])
        l_side_arc_bottom = unpoint(side_arc.point_on_circle(x=border_offset)[1])
        r_side_arc_top = (self.width - l_side_arc_top[0], l_side_arc_top[1])
        r_side_arc_bottom = (self.width - l_side_arc_bottom[0], l_side_arc_bottom[1])

        l_lower_arc = Circle(self.lower_arc.center, self.lower_arc.radius - border_offset)
        r_lower_arc = Circle((self.width - l_lower_arc.center).conjugate(),
                             l_lower_arc.radius)
        lower_peak = unpoint(l_lower_arc.intersect_circle(r_lower_arc)[1])
        
        outline = self.drawing.path(
            fill=self.bg, stroke=self.fg, stroke_width=self.border_width)
        outline.push('M', ul_corner_left)
        outline.push('L', ul_corner_right)
        outline.push_arc(center_peak, 0, upper_arc_left.radius, large_arc=False,
                         angle_dir='-', absolute=True)
        outline.push_arc(ur_corner_left, 0, upper_arc_left.radius, large_arc=False,
                         angle_dir='-', absolute=True)
        outline.push('L', ur_corner_right)
        outline.push('V', r_side_arc_top[1])
        outline.push_arc(r_side_arc_bottom, 0, side_arc.radius, large_arc=False,
                         angle_dir='-', absolute=True)
        outline.push_arc(lower_peak, 0, r_lower_arc.radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push_arc(l_side_arc_bottom, 0, l_lower_arc.radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push_arc(l_side_arc_top, 0, side_arc.radius, large_arc=False,
                         angle_dir='-', absolute=True)
        outline.push('Z')
        bg_g.add(outline)

        bg_g.add(self.drawing.line(
            (border_offset, self.divider_y),
            (self.width - border_offset, self.divider_y),
            stroke=self.fg, stroke_width=self.border_width))

        bg_g.add(render_text(
            self.drawing, 'MEXICO', self.mx_x, self.mx_y, self.mx_width,
            self.mx_height, self.mx_series, self.fg))
        
        return bg_g

    def make_ref(self):
        if not self.toll:
            return render_text(
                self.drawing, self.ref, self.ref_x, self.ref_y,
                self.ref_width, self.ref_height, self.ref_series,
                self.fg)
        
        ref_g = self.drawing.g()
        ref_g.add(render_text(
            self.drawing, self.ref[:-1], self.ref_x, self.ref_y,
            self.ref_width, self.ref_height, self.ref_series,
            self.fg))
        ref_g.add(render_text(
            self.drawing, self.ref[-1:], self.d_x, self.d_y,
            self.d_width, self.d_height, self.d_series,
            self.fg))
        return ref_g
