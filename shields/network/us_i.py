#!/usr/bin/python

# FHWA Standard Highway Signs:
#   https://mutcd.fhwa.dot.gov/SHSe/Guide.pdf#page=1
# Page 1: M1-1 (Interstate Route)
# Page 2: M1-2 (Off-Interstate Business Route (Loop))
# Page 3: M1-3 (Off_interstate Business Route (Spur))
#
# For good measure, we'll do a non-lop, non-spur business route shield,
# too, to pick up some of the tagging in the database.
# 
# Measurements:
#  | Design                 | 1  | 2,3 |     1 |     2 |     3 |      1 |     2 |     3 |
#  | Digits                 | *  | *   |   1,2 |   1,2 |   1,2 |      3 |     3 |     3 |
#  |------------------------+----+-----+-------+-------+-------+--------+-------+-------|
#  | width                  | A  | A   |    24 |    24 |    24 |     30 |    30 |    30 |
#  | height                 | B  | B   |    24 |    24 |    24 |     24 |    24 |    24 |
#  | border_width           | C* | C*  |    .5 |    .5 |    .5 |     .5 |    .5 |    .5 |
#  | divider_width          | C* | C*  |    .5 |    .5 |    .5 |     .5 |    .5 |    .5 |
#  | ref_y                  | D  | D   | 7.625 |     8 |     8 |  7.625 |     8 |     8 |
#  | ref_height             | E  | E   |  10 D |  10 D |  10 D |   10 C |  10 D |  10 D |
#  | ref_space_below        | F  | F   | 6.375 |     6 |     6 |  6.375 |     6 |     6 |
#  | transition_space_above | G  | G   |     5 |     5 |     5 |      5 |     5 |     5 |
#  | u_arc_radius           | H  | H   |    15 |    15 |    15 |     24 |    24 |    24 |
#  | l_arc_radius           | J  | J   |    15 |    15 |    15 |     17 |    17 |    17 |
#  | header_y               | K  | K   |     2 |     2 |     2 |      2 |     2 |     2 |
#  | header_height          | L  | L   | 2.5 C | 2.5 C | 2.5 C |  2.5 E | 2.5 C | 2.5 C |
#  | header_half_width      | M  | N   | 7.658 | 6.625 | 6.625 | 17.455 | 6.625 | 6.625 |
#  | modifier_space_above   |    | C*  |       |    .5 |    .5 |        |    .5 |    .5 |
#  | modifier_height        |    | M   |       | 1.5 D | 1.5 D |        | 1.5 D | 1.5 D |
#  | modifier_half_width    |    | P   |       |   2.5 | 2.563 |        |   2.5 | 2.563 |
# 
# Colors:
#  | Design  | 1     | 2,3   |
#  |---------+-------+-------|
#  | Border  | White | White |
#  | Letters | White | White |
#  | Top BG  | Red   | Green |
#  | Bot BG  | Blue  | Green |
#
# Notes:
#  * We'll use the standard series for numbers: 1-2: D; 3, with a 1: D; 3
#    without any 1s: C; 4+: B
#  * We don't need separate widths for the different headers or modifiers;
#    they just have to be centered.
#  * For consistency, we'll use series E for all 3-digit headers, not just
#    M1-1.

import math
import svgwrite

from ..geometry import *
from ..shieldbase import ShieldBase
from ..utils import *

def register(network_registry, style):
    # We can ignore the style parameter because:
    #  * the guide and independent designs are the same, which makes the
    #    cutout design the same, too.
    #  * "generic" only applies to state and local designs.  (This *is*
    #    the generic design.)
    network_registry['US:I'] = make_shield_func()
    for header in ('BUSINESS', 'DOWNTOWN'):
        network_registry['US:I:{}'.format(header)] = make_shield_func(header)
        for subheader in ('LOOP', 'SPUR'):
            network_registry['US:I:{}:{}'.format(header, subheader)] = make_shield_func(header, subheader)

def make_shield_func(header=None, subheader=None):
    if header is None:
        return lambda m,r: USInterstateShield(m, r).make_shield()
    if subheader is None:
        return lambda m,r: USInterstateShield(m, r, header).make_shield()
    return lambda m,r: USInterstateShield(m, r, header, subheader).make_shield()

class USInterstateShield(ShieldBase):
    def __init__(self, modifier, ref, header='INTERSTATE', subheader=''):
        super(USInterstateShield, self).__init__(modifier, ref)
        self.header = header
        self.subheader = subheader
        self.interstate = self.header == 'INTERSTATE'
        self.business = not self.interstate

        if self.narrow:
            self.width = 24.0
            self.u_arc_radius = 15.0
            self.l_arc_radius = 15.0
            self.header_series = 'C'
        else:
            self.width = 30.0
            self.u_arc_radius = 24.0
            self.l_arc_radius = 17.0
            self.header_series = 'E'
            
        if self.interstate:
            self.ref_y = 7.625
        else:
            self.ref_y = 8.0
            
        self.height = 24.0
        self.border_width = 0.5
        self.divider_width = 0.5
        self.ref_height = 10.0
        self.ref_series = self.standard_ref_series()
        transition_space_above = 5.0
        self.transition_y = transition_space_above + self.divider_width/2
        self.header_y = 2.0
        self.header_height = 2.5

        subheader_space_above = 0.5
        self.subheader_y = transition_space_above + self.divider_width + subheader_space_above
        self.subheader_height = 1.5
        self.subheader_series = 'D'

        self.calc_arc_params()
        
        self.fg = COLOR_WHITE
        if self.interstate:
            self.bg = COLOR_BLUE
            self.header_bg = COLOR_RED
        else:
            self.bg = COLOR_GREEN
            self.header_bg = COLOR_GREEN

    @property
    def blank_key(self):
        if self.interstate:
            return 'us_i-{}'.format(2 if self.narrow else 3)
        else:
            return 'us_i-business-{}-{}'.format(self.subheader, 2 if self.narrow else 3)
    
    def calc_arc_params(self):
        self.l_arc_center = (
            self.l_arc_radius,
            self.height - pythagoras(h=self.l_arc_radius,
                                     x=self.l_arc_radius - self.width/2))
        self.l_circle = Circle(point(*self.l_arc_center), self.l_arc_radius)
        self.ul_peak_tip = unpoint(self.l_circle.point_on_circle(y=0)[0])

        self.u_circle = Circle.from_chord_and_radius(
            point(*self.ul_peak_tip), self.width/2 + 0j, self.u_arc_radius)
            
        self.text_bounding_circle = Circle(
            self.l_circle.center,
            self.l_circle.radius - self.border_width - REF_MARGIN)
        
        self.header_x = self.left_text_bound(self.header_y, self.header_height)
        self.header_width = self.width - self.header_x * 2

        self.subheader_x = self.left_text_bound(self.subheader_y, self.subheader_height)
        self.subheader_width = self.width - self.subheader_x * 2

        self.ref_x = self.left_text_bound(self.ref_y, self.ref_height)
        self.ref_width = self.width - self.ref_x * 2
        needed_width = text_length(self.ref, self.ref_height, self.ref_series)
        if needed_width > self.ref_width:
            # We need to shrink the text.  The best placement for the text
            # will maintain the text's aspect ratio while placing the
            # lext's lower-left corner along the bounding circle.  Since
            # that corner will always lay along a line from the text's
            # center to the current corner (because we're preserving the
            # aspect ratio), all we have to do is intersect that line and
            # circle.
            ll_corner = self.text_bounding_circle.intersect_line(Line.from_points(
                (self.width/2 - needed_width/2) + (self.ref_y + self.ref_height) * 1j,
                (self.width/2) + (self.ref_y + self.ref_height/2) * 1j))[0]
            self.ref_x = ll_corner.real
            self.ref_width = self.width - self.ref_x * 2

    def left_text_bound(self, y, height):
        ul = self.text_bounding_circle.point_on_circle(y=y)[0]
        ll = self.text_bounding_circle.point_on_circle(y=y + height)[0]
        return max(ul.real, ll.real)
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        border_offset = self.border_width/2
        u_arc_left = Circle(self.u_circle.center, self.u_circle.radius + border_offset)
        u_arc_right = Circle((self.width - u_arc_left.center).conjugate(), u_arc_left.radius)
        l_arc_left = Circle(self.l_circle.center, self.l_circle.radius - border_offset)
        l_arc_right = Circle((self.width - l_arc_left.center).conjugate(), l_arc_left.radius)
        
        ul_peak = unpoint(u_arc_left.intersect_circle(l_arc_left)[0])
        uc_peak = unpoint(u_arc_left.intersect_circle(u_arc_right)[0])
        ur_peak = (self.width - ul_peak[0], ul_peak[1])
        l_peak = unpoint(l_arc_left.intersect_circle(l_arc_right)[1])
    
        transition_left = unpoint(l_arc_left.point_on_circle(y=self.transition_y)[0])
        transition_right = (self.width - transition_left[0], transition_left[1])

        if self.interstate:
            lower_outline = self.drawing.path(
                stroke=self.fg, fill=self.bg, stroke_width=self.border_width)
            lower_outline.push('M', transition_left)
            lower_outline.push_arc(
                l_peak, 0, l_arc_left.radius, angle_dir='-', large_arc=False,
                absolute=True)
            lower_outline.push_arc(
                transition_right, 0, l_arc_left.radius, angle_dir='-',
                large_arc=False, absolute=True)
            lower_outline.push('Z')
            bg_g.add(lower_outline)
    
            upper_outline = self.drawing.path(
                stroke=self.fg, fill=self.header_bg, stroke_width=self.border_width)
            upper_outline.push('M', transition_right)
            upper_outline.push_arc(ur_peak, 0, l_arc_left.radius, angle_dir='-',
                                   large_arc=False, absolute=True)
            upper_outline.push_arc(uc_peak, 0, u_arc_left.radius, angle_dir='+',
                                   large_arc=False, absolute=True)
            upper_outline.push_arc(ul_peak, 0, u_arc_left.radius, angle_dir='+',
                                   large_arc=False, absolute=True)
            upper_outline.push_arc(transition_left, 0, l_arc_left.radius, angle_dir='-',
                                   large_arc=False, absolute=True)
            upper_outline.push('Z')
            bg_g.add(upper_outline)
        else:
            outline = self.drawing.path(
                stroke=self.fg, fill=self.bg, stroke_width=self.border_width)
            outline.push('M', l_peak)
            outline.push_arc(ur_peak, 0, l_arc_right.radius, angle_dir='-',
                             large_arc=False, absolute=True)
            outline.push_arc(uc_peak, 0, u_arc_right.radius, angle_dir='+',
                             large_arc=False, absolute=True)
            outline.push_arc(ul_peak, 0, u_arc_left.radius, angle_dir='+',
                             large_arc=False, absolute=True)
            outline.push_arc(l_peak, 0, l_arc_left.radius, angle_dir='-',
                             large_arc=False, absolute=True)
            
            outline.push('Z')
            bg_g.add(outline)
            bg_g.add(self.drawing.line(
                transition_left, transition_right, stroke=self.fg,
                stroke_width=self.divider_width))

        bg_g.add(render_text(
            self.drawing, self.header, self.header_x, self.header_y,
            self.header_width, self.header_height, self.header_series,
            self.fg))

        if self.business and self.subheader != '':
            bg_g.add(render_text(
                self.drawing, self.subheader, self.subheader_x, self.subheader_y,
                self.subheader_width, self.subheader_height, self.subheader_series,
                self.fg))

        return bg_g
