#!/usr/bin/python

# http://www.dot.state.mn.us/trafficeng/publ/signsmanual/2017/ssmmseries.pdf#page=10

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..banner import banner
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:MN'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:MN'] = lambda m,r: MNStateShield(m, r).make_shield()

class MNStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(MNStateShield, self).__init__(modifier, ref)

        self.business = self.modifier == 'BUSINESS'
        
        self.corner_radius = 1.5
        if len(self.ref) <= 2:
            self.width = 24.0
            if self.business:
                self.mn_x = 6.3
                self.mn_width = 16.0
            else:
                self.mn_x = 5.5
                self.mn_width = 17.5
            self.mn_series = 'C'
        else:
            self.width = 30.0
            if self.business:
                self.mn_x = 8.0
                self.mn_width = 19.0
            else:
                self.mn_x = 7.2
                self.mn_width = 21.0
            self.mn_series = 'D'
        self.height = 24.0
        self.outline_x = 0.5
        self.outline_y = 0.5
        self.outline_height = 5.0
        self.bg_y = self.outline_y * 2 + self.outline_height
        self.bg_x = 0.5
        self.mn_y = 1.5
        ref_offset = 3.7
        self.ref_y = self.bg_y + ref_offset
        self.mn_height = 3.0
        self.ref_height = 10.0
        
        self.ref_x = 1.0
        self.ref_width = self.width - self.ref_x * 2
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C'])

        self.fg = COLOR_WHITE
        if self.business:
            self.border_color = COLOR_WHITE
            self.bg = COLOR_GREEN
            self.mn_text = 'BUSINESS'
        else:
            self.border_color = COLOR_YELLOW
            self.bg = COLOR_BLUE
            self.mn_text = 'MINNESOTA'

    @property
    def blank_key(self):
        if self.business:
            return 'us_mn-{}-business'.format(int(self.width))
        else:
            return 'us_mn-{}'.format(int(self.width))
    
    def make_shield(self):
        if self.ref is None:
            return None
        
        if self.modifier == '' or self.modifier == 'BUSINESS':
            return self.make_base_shield()
        else:
            shield_svg = self.make_base_shield()
            banner_svgs = [banner(mod, int(shield_svg['width']), fg=self.fg, bg=self.bg,
                                  border=self.border_color)
                           for mod in self.modifier.split(';')]
            return compose_shields(banner_svgs + [shield_svg])

    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.drawing.rect(
            (0, 0), (self.width, self.height), self.corner_radius,
            fill=self.border_color))
        bg2 = self.drawing.path(fill=self.bg)
        bg_g.add(bg2)
        bg2.push('M', (self.bg_x, self.bg_y))
        bg2.push('V', self.height - self.corner_radius)
        bg2.push_arc((self.corner_radius, self.height - self.bg_x), 0,
                     self.corner_radius - self.bg_x, large_arc=False,
                     angle_dir='-', absolute=True)
        bg2.push('H', self.width - self.corner_radius)
        bg2.push_arc((self.width - self.bg_x, self.height - self.corner_radius), 0,
                     self.corner_radius - self.bg_x, large_arc=False,
                     angle_dir='-', absolute=True)
        bg2.push('V', self.bg_y)
        bg2.push('Z')

        bg_g.add(self.drawing.path(
            d=MN_OUTLINE, fill=self.bg,
            transform='translate({} {})'.format(self.outline_x, self.outline_y)))
        bg_g.add(render_text(
            self.drawing, self.mn_text, self.mn_x, self.mn_y, self.mn_width,
            self.mn_height, self.mn_series, color=self.bg))
        
        return bg_g

MN_OUTLINE_WIDTH = 4.39
MN_OUTLINE = 'M 0,0.31269172 0.00664628,0.39621151 0.02321237,0.45533136 0.03967926,0.5459978 0.04374645,0.60650432 0.02866835,0.71365072 0.02370837,0.81283713 0.02678356,0.99347668 0.02370837,1.0210633 0.03273541,1.1377163 0.08888167,1.3489158 0.10534856,1.3975157 0.14185301,1.5552486 0.14889699,1.6252751 0.16536361,1.8846745 0.15941163,2.0313008 0.18688889,2.1014339 0.19046088,2.1159139 0.18034224,2.1834604 0.18748489,2.33722 0.20196752,2.4253131 0.22349413,2.5038729 0.27904466,2.6260859 0.30612593,2.720619 0.31356591,2.7597123 0.3170379,2.8763653 0.30711793,2.9660451 0.29551129,3.0170316 0.26446203,3.0901381 0.24343275,3.1182047 0.22647013,3.1312047 0.15584097,3.1972712 0.15048365,3.2072845 0.14641699,3.2317911 0.14939299,3.2448844 0.15792363,3.265911 0.20295952,3.3394175 0.22994212,3.3719641 0.26743802,3.393484 0.31356591,3.4300839 0.32814721,3.4465506 0.33856318,3.4636172 0.34759116,3.4831638 0.36068446,3.5346437 0.36137912,3.5466436 V 5 H 3.5351312 L 3.5310512,4.965479 V 4.7752168 L 3.5235179,4.7482356 3.5049579,4.7132183 3.4473314,4.6256265 3.4239181,4.6060839 3.4013048,4.5915026 3.314705,4.5494414 3.2831585,4.5294041 3.1830654,4.4558976 3.1511188,4.4163177 3.1260255,4.3657272 3.1020256,4.3297179 3.0759323,4.3026367 3.0439857,4.2826981 3.0248391,4.2651394 2.9462793,4.2205995 2.9118527,4.1900463 2.8751595,4.1509624 2.8486662,4.1379678 2.8210929,4.1279491 2.7970797,4.1238811 2.7720931,4.1229891 2.7535331,4.1174331 2.7255599,4.1049345 2.6749733,4.0683306 2.5998802,4.0012721 2.5918402,3.9872855 2.5804402,3.9557402 2.5778536,3.920723 2.5812269,3.6027904 2.5913469,3.522644 2.5974002,3.497644 2.6084135,3.4746307 2.6193201,3.4555775 2.6353867,3.4120309 2.6368801,3.385551 2.6314268,3.3670043 2.6173335,3.3454711 2.5708136,3.3048978 2.544827,3.2789112 2.5267737,3.2578846 2.5208137,3.2398313 2.5197337,3.223258 2.5208137,3.2048048 2.524787,3.1893381 2.5768669,3.0710851 2.5928402,3.0460985 2.6324134,2.9964986 2.8181196,2.8678323 2.8312129,2.8533523 2.8381596,2.837379 2.8421196,2.8218057 2.8546262,2.3647002 2.8876528,2.319167 3.2116386,2.0012344 3.4078515,1.791035 3.4969313,1.6863819 3.5219312,1.6523553 3.5665711,1.6088087 3.6722108,1.5271689 3.8428371,1.4180492 3.8598904,1.4110092 3.8789436,1.4009959 4.1177164,1.3143828 4.1402363,1.3033828 4.3895157,1.1336499 4.3745424,1.1291833 4.3419958,1.1247166 4.2664093,1.12313 4.2258361,1.1301833 4.2038095,1.1306766 4.1827829,1.1247166 4.1697962,1.1161966 4.1527296,1.1076633 4.0866631,1.0615301 3.7306373,1.0626234 3.7141707,1.0555835 3.7001841,1.0460635 3.6365976,0.96749035 3.6221176,0.9615437 3.6061443,0.96045037 3.5904777,0.96391702 3.5725177,0.96997034 3.5404778,0.98802363 3.5235179,1.0020103 3.4709447,1.0655968 3.4463447,1.0866234 3.4278914,1.09665 3.4119181,1.10369 3.3017051,1.1267033 3.2837585,1.12621 3.2627185,1.1201566 3.2481452,1.1126233 3.2236386,1.0951567 3.1684787,1.0766101 3.1620388,1.0675834 3.1511188,1.0555835 3.1420921,1.0365302 3.1239455,1.0095569 3.0168125,0.97046367 3.0003458,0.95747704 2.9643326,0.89983718 2.9442926,0.88189056 2.936266,0.87633058 2.9283193,0.87286392 2.9061994,0.8668106 H 2.8912261 L 2.8762395,0.86987726 2.8391462,0.88437056 2.8312129,0.89389053 2.8281329,0.90390384 2.8255596,0.97146367 2.8230796,0.97602366 2.8176263,0.97999698 2.7970797,0.98643697 2.7796264,0.97999698 2.7675197,0.96391702 2.7634531,0.96045037 2.7295332,0.88189056 2.7170265,0.82682403 2.7045332,0.81482406 2.6973866,0.80977074 2.6915333,0.80729075 2.6163468,0.7997441 2.6048402,0.79270412 2.6008668,0.78417081 2.6014668,0.77574416 2.6102935,0.75025089 2.6153601,0.72862428 2.6142668,0.72069096 2.6113868,0.71562431 2.3611074,0.63002452 2.3331342,0.62506454 2.3059609,0.62297121 2.2659743,0.63002452 2.2468277,0.63706451 2.2199478,0.65313113 2.1774012,0.69013104 2.1613213,0.7187043 2.1422813,0.7336776 2.0992281,0.74965089 2.0341482,0.76374419 1.9830684,0.76075753 1.9590551,0.74667757 1.9516151,0.73765092 1.9454685,0.70313101 1.9380285,0.6796244 1.9330685,0.67158442 1.9124419,0.65165114 1.8965619,0.64311783 1.883962,0.63905117 1.7217757,0.62803786 1.6997491,0.61603789 1.6832825,0.60105126 1.6682158,0.59103796 1.6557092,0.5884513 1.6216826,0.58647797 1.6001627,0.59302462 1.588656,0.59450461 1.5686161,0.59499795 1.5470828,0.58905129 1.5030429,0.56653135 1.4950163,0.5599847 1.4819096,0.54193141 1.4705097,0.51891814 1.3840032,0.09465253 C 1.3799366,0.07490591 1.3792432,0.04474599 1.3604966,0.02927936 1.34859,0.01935938 1.3236941,0.0147994 1.3111954,0.01221274 L 1.2552476,0.00309276 1.1836264,1.2766666e-5 V 0.31269198 Z'
