#!/usr/bin/python

# http://transportation.wv.gov/highways/engineering/Manuals/Traffic/SIGNFAB/M_SERIES.PDF#page=8
# http://transportation.wv.gov/highways/engineering/StandardDetails/Vol2/1998english/1998SD2e.pdf#page=32

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:WV'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:WV'] = lambda m,r: WVStateShield(m, r, style).make_shield()

class WVStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(WVStateShield, self).__init__(modifier, ref)
        self.guide = style == STYLE_GUIDE

        if len(self.ref) <= 2:
            self.width = 24.0
            self.ref_series = 'D'
        else:
            self.width = 30.0
            if len(self.ref) == 3:
                if '1' in ref:
                    self.ref_series = 'D'
                else:
                    self.ref_series = 'C'
            else:
                self.ref_series = 'B'
        self.height = 24.0
        if self.guide:
            self.stroke_width = 0.5
        else:
            self.stroke_width = 2.0
        self.ref_space_above = 4.0
        self.ref_height = 12.0
        self.corner_radius = 1.5

        self.ref_x = self.stroke_width * 1.5
        self.ref_width = self.width - 2 * self.ref_x
        self.ref_y = self.height/2 - self.ref_height/2

        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        
    @property
    def blank_key(self):
        return 'us_wv-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        if self.guide:
            border_offset = self.stroke_width/2
            return self.drawing.rect(
                (border_offset, border_offset),
                (self.width - border_offset * 2, self.height - border_offset * 2),
                self.corner_radius - border_offset,
                stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
        
        background_g = self.drawing.g()

        # The inner and outer corners of the border have different centers for
        # their radii, so we can't just make one rectangle for them.  We have
        # to do two.
        black_outline = self.drawing.rect(
            (0, 0), (self.width, self.height), self.corner_radius,
            stroke='none', fill=self.fg)
        background_g.add(black_outline)

        white_fill = self.drawing.rect(
            (self.stroke_width, self.stroke_width),
            (self.width - self.stroke_width*2, self.height - self.stroke_width*2),
            self.corner_radius,
            stroke='none', fill=self.bg)
        background_g.add(white_fill)
        
        return background_g
