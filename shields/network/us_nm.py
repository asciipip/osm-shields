#!/usr/bin/python

# http://dot.state.nm.us/content/dam/nmdot/Infrastructure/SignandStripingManual.pdf#page=93
#
# 2.4.10 Design of Route Signs [2D.11]
# State Route (M1-NM-5)
#
# For post-mounted route marker assemblies, use the State Route (M1-NM-5)
# sign, which consists of black numerals within a round red zia symbol
# surrounded by a black border.  The standard size for the M1-NM-5 marker
# is 24 by 24 inches for one- and two-digit route numbers, and 30 by 30
# inches for three-digit route numbers. The NMDOT Sign Code Listing
# illustrates the M1-NM-5 sign on Page 23/38 of Appendix D.

# http://dot.state.nm.us/content/dam/nmdot/Plans_Specs_Estimates/Standard_Drawings/701.pdf#page=30
# http://dot.state.nm.us/content/dam/nmdot/Infrastructure/2005_sign_code_listing.pdf#page=76

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield, STYLE_ELLIPSE
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:NM'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_GUIDE:
        network_registry['US:NM'] = lambda m,r: USGenericStateShield(m, r, STYLE_ELLIPSE).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:NM'] = lambda m,r: NMStateShield(m, r, sign=True).make_shield()
    else:
        network_registry['US:NM'] = lambda m,r: NMStateShield(m, r).make_shield()

class NMStateShield(ShieldBase):
    def __init__(self, modifier, ref, sign=False):
        super(NMStateShield, self).__init__(modifier, ref)
        self.sign = sign
        
        if len(self.ref) <= 2:
            self.width = 24.0
            self.height = 24.0
        else:
            self.width = 30.0
            self.height = 30.0

        self.border = 0.5
        self.zia_width = 5.0/8.0
        self.zia_diameter = self.width - 8.0
        self.zia_arm_spacing = 0.75
        self.zia_outer_arm_length = 1.5
        self.zia_inner_arm_length = 2.0
        
        self.ref_height = 8.0
        self.ref_series = 'D'
        self.ref_width = text_length(self.ref, self.ref_height, self.ref_series)

        w, h = rectangle_in_ellipse_with_ratio(self.zia_diameter, self.zia_diameter,
                                               self.ref_width/self.ref_height)
        if h < self.ref_height:
            self.ref_height = h
            self.ref_width = w
        self.ref_x = (self.width - self.ref_width)/2
        self.ref_y = self.height/2 - self.ref_height/2

        self.corner_radius = 0.5
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        self.zia_color = COLOR_RED

    @property
    def blank_key(self):
        return 'us_nm-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g(transform='translate({} {})'.format(self.width/2, self.height/2))

        if self.sign:
            bg_g.add(self.drawing.rect(
                (-self.width/2, -self.height/2), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            
        bg_g.add(self.drawing.circle(
            (0, 0), self.width/2 - self.border/2,
            stroke=self.fg, fill=self.bg, stroke_width=self.border))

        bg_g.add(self.drawing.circle(
            (0, 0), self.zia_diameter/2 + self.zia_width/2,
            stroke=self.zia_color, fill='none', stroke_width=self.zia_width))

        self.add_zia_arms()
        for r in (0, 90, 180, 270):
            bg_g.add(self.drawing.use('#us_nm-zia', transform='rotate({})'.format(r)))
        
        return bg_g

    def add_zia_arms(self):
        arm = self.drawing.path(id='us_nm-zia', stroke=self.zia_color, stroke_width=self.zia_width)

        radius = self.zia_diameter/2 + self.zia_width/2
        arm_locs = [
            (- self.zia_width * 1.5 - self.zia_arm_spacing * 1.5, self.zia_outer_arm_length),
            (- self.zia_width * 0.5 - self.zia_arm_spacing * 0.5, self.zia_inner_arm_length),
            (self.zia_width * 0.5 + self.zia_arm_spacing * 0.5, self.zia_inner_arm_length),
            (self.zia_width * 1.5 + self.zia_arm_spacing * 1.5, self.zia_outer_arm_length),
        ]
        for x, l in arm_locs:
            y = math.sqrt(radius**2 - (x)**2)
            arm.push('M', (x, y))
            arm.push('v', l + self.zia_width/2)

        self.drawing.defs.add(arm)
