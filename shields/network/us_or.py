#!/usr/bin/python

# ftp://ftp.odot.state.or.us/techserv/roadway/web_drawings/traffic/2018_01/tm212.pdf

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:OR'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:OR'] = lambda m,r: ORStateShield(m, r, style).make_shield()

class ORStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(ORStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        
        if len(self.ref) <= 2 or (len(self.ref) == 3 and self.ref.count('1') >= 2):
            self.width = 24.0              # B
            self.upper_radius = 21.5       # C
            lower_center_offset_x = 3.5    # D
            lower_center_offset_y = 8.875  # E
            corner_center_offset_x = 5.375 # G
            corner_center_offset_y = 7.5   # H
            self.corner_radius = 6.5       # I
            self.ref_height = 12.0         # A (ref)
            self.ref_y = 4.5               # B (ref)
        else:
            self.width = 30.0              # B
            self.upper_radius = 35.25      # C
            lower_center_offset_x = 0.5    # D
            lower_center_offset_y = 8.5    # E
            corner_center_offset_x = 8.25  # G
            corner_center_offset_y = 8.0   # H
            self.corner_radius = 6.75      # I
            self.ref_height = 11.0         # A (ref)
            self.ref_y = 5.0               # B (ref)
        self.height = 25.0                 # A
        self.lower_radius = 15.5           # F
        self.border_width = 0.5            # J
        self.ref_series = 'C'              # A (ref)

        self.ref_x = 0
        self.ref_width = self.width - self.ref_x * 2
        
        self.upper_center = (self.width/2, self.upper_radius)
        self.corner_center = (self.width/2 - corner_center_offset_x, corner_center_offset_y)
        self.lower_center = (self.width/2 + lower_center_offset_x, lower_center_offset_y)
        self.lower_peak = (self.width/2, math.sqrt(self.lower_radius**2 - lower_center_offset_x**2) + lower_center_offset_y)
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_or-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        if self.sign:
            outline = self.drawing.path(fill=self.bg)
            border_offset = self.border_width
        else:
            outline = self.drawing.path(
                stroke=self.fg, fill=self.bg, stroke_width=self.border_width)
            border_offset = self.border_width/2

        # radii
        upper_radius = self.upper_radius - border_offset
        corner_radius = self.corner_radius - border_offset
        lower_radius = self.lower_radius - border_offset

        # Find arc transition points.  Each such point will be collinear
        # with the two arc centers.
        uc = self.upper_center[0] + self.upper_center[1] * 1j
        cc = self.corner_center[0] + self.corner_center[1] * 1j
        lc = self.lower_center[0] + self.lower_center[1] * 1j
        u2c = cc + (cc - uc) / abs(cc - uc) * corner_radius
        c2l = cc + (cc - lc) / abs(cc - lc) * corner_radius
        # For completeness
        l2l = self.lower_peak[0] + (self.lower_peak[1] - border_offset) * 1j
        
        # Drawing
        outline.push('M', (l2l.real, l2l.imag))
        outline.push_arc((c2l.real, c2l.imag), 0, lower_radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push_arc((u2c.real, u2c.imag), 0, corner_radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push_arc((self.width - u2c.real, u2c.imag), 0, upper_radius,
                         large_arc=False, angle_dir='+', absolute=True)
        outline.push_arc((self.width - c2l.real, c2l.imag), 0, corner_radius,
                         large_arc=False, angle_dir='+', absolute=True)
        outline.push_arc((l2l.real, l2l.imag), 0, lower_radius, large_arc=False,
                         angle_dir='+', absolute=True)
        outline.push('Z')

        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(outline)
            return bg_g
        else:
            return outline
