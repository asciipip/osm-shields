#!/usr/bin/python

# Guam!
#
# I haven't been able to locate an official government source for route
# marker design in Guam.  The stuff below is based on photos of signs.

import math
import svgwrite

from ..geometry import *
from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:GU'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:GU'] = lambda m,r: USGuamShield(m, r, style).make_shield()

class USGuamShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(USGuamShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN

        self.width = 15.0
        self.height = 21.5
        self.border_width = 0.5
        self.corner_radius = 0.75

        self.calc_arc_params()

        self.ref_y = 4.5
        self.ref_height = 9.5
        text_bound_circle = Circle(
            self.arc.center, self.arc.radius - self.border_width * 2.5)
        text_ul_corner = text_bound_circle.point_on_circle(y=self.ref_y)[0]
        if len(self.ref) == 1:
            self.ref_series = 'D'
        elif len(self.ref) == 2:
            if '1 ' in self.ref:
                self.ref_series = 'D'
            else:
                self.ref_series = 'C'
        else:
            self.ref_series = 'B'
        self.ref_x = text_ul_corner.real
        self.ref_width = self.width - self.ref_x * 2
        needed_width = text_length(self.ref, self.ref_height, self.ref_series)
        if self.ref_width < needed_width:
            self.old_ref_x = self.ref_x
            self.old_ref_y = self.ref_y
            self.old_ref_height = self.ref_height
            ref_center_bottom = point(self.width/2, self.ref_y + self.ref_height)
            ref_diagonal = Line.from_points(
                point(self.width/2 - needed_width/2, self.ref_y),
                ref_center_bottom)
            new_ul_corner = text_bound_circle.intersect_line(ref_diagonal)[0]
            self.ref_x = new_ul_corner.real
            self.ref_width = self.width - self.ref_x * 2
            self.ref_height = ref_center_bottom.imag - new_ul_corner.imag
            self.ref_y = new_ul_corner.imag

        gu_margin = 1.0
        self.gu_y = self.ref_y + self.ref_height + gu_margin
        self.gu_height = 2.0
        self.gu_x = self.ref_x
        self.gu_width = self.width - self.gu_x * 2
        self.gu_series = 'D'
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_BLUE

    @property
    def blank_key(self):
        return 'us_gu'

    def calc_arc_params(self):
        # https://math.stackexchange.com/questions/564058
        l = self.height
        h = self.width/2 - self.border_width
        arc_radius = (4 * h**2 + l**2) / (8 * h)

        self.arc = Circle(
            (self.border_width + arc_radius) + self.height/2 * 1j,
            arc_radius)
        
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.make_sign_background())
            
        outline = self.drawing.path(fill=self.bg)
        outline.push('M', (self.width/2, 0))
        outline.push_arc(
            (self.width/2, self.height), 0, self.arc.radius,
            large_arc=False, angle_dir='-', absolute=True)
        outline.push_arc(
            (self.width/2, 0), 0, self.arc.radius,
            large_arc=False, angle_dir='-', absolute=True)
        outline.push('Z')
        bg_g.add(outline)

        lining_offset = self.border_width * 1.5
        lining_circle = Circle(self.arc.center, self.arc.radius - lining_offset)
        lining_top = unpoint(lining_circle.point_on_circle(x=self.width/2)[0])
        lining_bottom = (lining_top[0], self.height - lining_top[1])
        lining = self.drawing.path(
            stroke=self.fg, fill='none', stroke_width=self.border_width)
        lining.push('M', lining_top)
        lining.push_arc(
            lining_bottom, 0, lining_circle.radius, large_arc=False,
            angle_dir='-', absolute=True)
        lining.push_arc(
            lining_top, 0, lining_circle.radius, large_arc=False,
            angle_dir='-', absolute=True)
        lining.push('Z')
        bg_g.add(lining)

        bg_g.add(render_text(
            self.drawing, 'GUAM', self.gu_x, self.gu_y, self.gu_width,
            self.gu_height, self.gu_series, self.fg))

        return bg_g
