#!/usr/bin/python

# This one was a rabbit's warren.
#
# Colorado's supplement to the federal MUTCD:
#   https://www.codot.gov/library/traffic/traffic-manuals-and-guidelines/fed-state-co-traffic-manuals/mutcd/MUTCD_2003_Colorado_Supplement.pdf#page=6
# says:
# "Section 2D.11 Design of Route Signs.
# The following is added to Paragraph 01: The State Highway System Route
# Marker shall consist of a square plate or panel, at least 24 inches in
# size, carrying in the upper portion a representation of the Colorado
# State flag and in the lower portion the State Highway route number in
# black on a white background. County Road Markers shall be
# distinguishable from the State Highway System Route Marker."
#
# From that, we can split the shield in half and put the route number on
# the bottom half.  (Based on surveys of signs in the wild, it appears
# that they use FHWA series E when possible, and step down as needed.)
# But what about the Colorado flag?
#
# C.R.S. 24-80-904:
#   https://advance.lexis.com/api/document/collection/statutes-legislation/id/5PC1-8450-004D-12D4-00008-00?cite=C.R.S.%2024-80-904&context=1000516
# says:
# "The flag shall consist of three alternate stripes to be of equal width
# and at right angles to the staff, the two outer stripes to be blue of
# the same color as in the blue field of the national flag and the middle
# stripe to be white, the proportion of the flag being a width of
# two-thirds of its length. At a distance from the staff end of the flag
# of one-fifth of the total length of the flag there shall be a circular
# red C, of the same color as the red in the national flag of the United
# States. The diameter of the letter shall be two-thirds of the width of
# the flag. The inner line of the opening of the letter C shall be
# three-fourths of the width of its body or bar, and the outer line of the
# opening shall be double the length of the inner line thereof. Completely
# filling the open space inside the letter C shall be a golden disk"
#
# No guidance is given as to how widely the C should open relative to the
# size of the flag.  A value of 75% the width of a single strip seems to
# match many actual flags, so that's what we're going with.
#
# If we follow the bit about the C being "a distance from the staff ... of
# one-fifth of the total length of the flag", it puts the C a bit further
# to the right than most signs (and, indeed, the example in the CDOT Sign
# Design Manual) put it.  In the absence of official documentation on
# where the C should go, however, we're choosing to follow the flag
# definitions.

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:CO'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:CO'] = lambda m,r: COStateShield(m, r).make_shield()

class COStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(COStateShield, self).__init__(modifier, ref)

        self.height = 24.0
        self.width = 24.0
        # Parameters determined largely by looking at photos of signs and
        # estimating.
        self.margin_width = 0.375
        self.border_width = 0.675
        self.separator_width = 0.5
        self.corner_radius = 1.5

        self.ref_height = 8.0
        ref_vertical_space = self.height/2 - self.separator_width/2 - self.border_width - self.margin_width - self.ref_height
        self.ref_y = self.height/2 + self.separator_width/2 + ref_vertical_space/2
        self.ref_x = self.margin_width + self.border_width + ref_vertical_space/2
        self.ref_width = self.width - self.ref_x * 2
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['E', 'D', 'C', 'B'])

        self.c_opening_to_stripe_ratio = 0.75
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_co'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.drawing.rect(
            (self.margin_width + self.border_width/2, self.margin_width + self.border_width/2),
            (self.width - self.margin_width * 2 - self.border_width, self.height - self.margin_width * 2 - self.border_width),
            self.corner_radius - self.margin_width - self.border_width/2,
            stroke=self.fg, fill=self.bg, stroke_width=self.border_width))
        bg_g.add(self.make_co_flag())
        bg_g.add(self.drawing.line(
            (self.margin_width + self.border_width/2, self.height/2),
            (self.width - self.margin_width - self.border_width/2, self.height/2),
            stroke=self.fg, stroke_width=self.separator_width))
        
        return bg_g

    def make_co_flag(self):
        flag_g = self.drawing.g()

        flag_width = self.width - (self.margin_width + self.border_width) * 2
        flag_height = self.height/2 - self.margin_width - self.border_width - self.separator_width/2
        stripe_height = flag_height/3

        stripe_x = self.margin_width + self.border_width
        stripe_width = self.width - stripe_x * 2
        stripe1_y = self.margin_width + self.border_width
        stripe2_y = stripe1_y + stripe_height
        stripe3_y = stripe2_y + stripe_height

        c_height = flag_height * 2 / 3
        c_opening_outer_height = stripe_height * self.c_opening_to_stripe_ratio
        c_opening_inner_height = c_opening_outer_height / 2
        c_thickness = c_opening_inner_height * 4 / 3
        c_x = self.margin_width + self.border_width + flag_width/5
        c_y = self.margin_width + self.border_width + flag_height/2 - c_height/2
        c_center = (c_x + c_height/2, c_y + c_height/2)
        c_outer_point_x = c_x + c_height/2 + \
                          math.sqrt((c_height/2)**2 - (c_opening_outer_height/2)**2)
        c_inner_point_x = \
            c_outer_point_x - \
            math.sqrt(c_thickness**2 - ((c_opening_outer_height - c_opening_inner_height)/2)**2)

        disk_radius = math.sqrt((c_inner_point_x - c_center[0])**2 + (c_opening_inner_height/2)**2)
        
        # Upper stripe needs to be a path to accommodate the curved border.
        stripe1 = self.drawing.path(fill=COLOR_BLUE)
        flag_g.add(stripe1)
        stripe1.push('M', (stripe_x, stripe2_y))
        stripe1.push('V', self.corner_radius)
        stripe1.push_arc(
            (self.corner_radius, self.margin_width + self.border_width),
            0, self.corner_radius - self.margin_width - self.border_width,
            large_arc=False, angle_dir='+', absolute=True)
        stripe1.push('H', self.width - self.corner_radius)
        stripe1.push_arc(
            (self.width - self.margin_width - self.border_width, self.corner_radius),
            0, self.corner_radius - self.margin_width - self.border_width,
            large_arc=False, angle_dir='+', absolute=True)
        stripe1.push('V', stripe2_y)
        stripe1.push('Z')
        flag_g.add(self.drawing.rect(
            (stripe_x, stripe3_y), (stripe_width, stripe_height), fill=COLOR_BLUE))

        c_path = self.drawing.path(
            stroke=COLOR_RED, fill='none', stroke_width=c_thickness)
        flag_g.add(c_path)
        c_path.push('M', ((c_outer_point_x + c_inner_point_x)/2,
                          c_center[1] + (c_opening_outer_height + c_opening_inner_height) / 4))
        c_path.push_arc(
            (0, -(c_opening_outer_height + c_opening_inner_height) / 2),
            0, disk_radius + (c_height/2 - disk_radius)/2,
            large_arc=True, angle_dir='+', absolute=False)
        flag_g.add(self.drawing.circle(
            c_center, disk_radius, fill=COLOR_YELLOW))
        
        return flag_g
