#!/usr/bin/python

# https://connect.ncdot.gov/resources/Specifications/2012%20Roadway%20Standard%20Drawings/Division%2009%20-%20Signing.pdf#page=5

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:NC'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:NC'] = lambda m,r: NCStateShield(m, r).make_shield()

class NCStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(NCStateShield, self).__init__(modifier, ref)

        self.width = 24.0
        self.height = 24.0
        if len(ref) == 1:
            self.ref_height = 12.0
        elif len(ref) == 2:
            self.ref_height = 10.0
        else:
            self.ref_height = 8.0
        self.corner_radius = 1.5
        self.stroke_width = 0.5  # assumed
        self.ref_y = self.height/2 - self.ref_height/2

        corner_extra_length = math.sqrt(2 * self.corner_radius**2) - self.corner_radius
        border_offset = math.sqrt(2 * self.stroke_width**2)
        bound_ll = 0 - corner_extra_length + border_offset + self.height/2 * 1j
        bound_ur = self.width/2 + bound_ll.real * 1j
        ref_top_l = 0 + self.ref_y * 1j
        ref_top_r = self.width + self.ref_y * 1j
        ref_ul = intersect_lines(bound_ll, bound_ur, ref_top_l, ref_top_r)
        self.ref_x = ref_ul.real
        self.ref_width = self.width - self.ref_x * 2
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_nc'
    
    def make_blank_shield(self):
        corner_extra_length = math.sqrt(2 * self.corner_radius**2) - self.corner_radius
        diagonal = self.height + corner_extra_length*2
        side = math.sqrt(diagonal**2 / 2)
        corner = (self.stroke_width/2, self.stroke_width/2)
        dims = (side - corner[0]*2, side - corner[1]*2)
        radius = self.corner_radius - self.stroke_width/2
        offset_corner = (corner[0] + self.width/2 - side/2, corner[1] + self.width/2 - side/2)
        outline = self.drawing.rect(
            offset_corner, dims, radius,
            stroke=self.fg, fill=self.bg,
            stroke_width=self.stroke_width,
            transform='rotate({} {} {})'.format(45, self.width/2, self.height/2))
        return outline
