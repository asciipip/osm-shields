#!/usr/bin/python


import math
import svgwrite
import syslog

from ..utils import *

def register(network_registry, style):
    network_registry['NCN'] = make_ncn_shield

def make_ncn_shield(modifier, ref):
    if ref == 'ECG':
        return read_template('ncn:ECG')
    syslog.syslog('unknown ref: (NCN;{};{})'.format(ref, modifier))
    return None
