#!/usr/bin/python

# https://outside.vermont.gov/agency/vtrans/external/CADD/WebFiles/Downloads/Standards/English/PDF/stde136b.pdf
# Also:
# https://outside.vermont.gov/agency/vtrans/external/CADD/WebFiles/Downloads/Standards/English/PDF/stde136c.pdf
# But E-136C is identical to the "elongated" style of US state shields,
# so we just call our us_state shield-making function for those.

# Town-maintained portions of state routes are signed differently from
# state-maintained portions of the same routes.  There's no OSM tagging
# consensus on how (or whether!) to differentiate the two, so we use the
# state design unless the entire route is town-maintained.

import math
import re
import svgwrite

from ..banner import banner
from ..utils import *
from ..us_state import STYLE_ELONGATED, USGenericStateShield

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:VT'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:VT'] = lambda m,r: make_shield(m, r, style)

LEFT_CURVE_CHARS = '346890'
RIGHT_CURVE_CHARS = '346890'

def short_ref(ref):
    return '1' in ref or \
        (ref[0] in LEFT_CURVE_CHARS and ref[-1] in RIGHT_CURVE_CHARS)

def very_short_ref(ref):
    return '1' in ref and \
        (ref[0] in LEFT_CURVE_CHARS and ref[-1] in RIGHT_CURVE_CHARS)

class VTRouteParams:
    def __init__(self, ref, style):
        self.ref = ref
        self.guide = style == STYLE_GUIDE

        alt_marker = re.search('[^0-9]', ref) is not None
        if len(ref) <= 2 or (len(ref) == 3 and alt_marker):
            digits = 2
        else:
            digits = 3
            
        if digits == 2:
            self.width = 24.0        # A
        else:
            self.width = 30.0        # A
        self.height = 24.0           # B
        self.vt_bg_y = 1.5           # C
        self.vt_bg_height = 6.0      # D
        bg_spacing = 1.0             # E
        self.ref_bg_y = self.vt_bg_y + self.vt_bg_height + bg_spacing
        self.ref_bg_height = 14.0    # F
        # don't need bottom spacing  # G
        self.vt_height = 4.0         # H
        if digits == 2:
            self.vt_series = 'C'     # H
        else:
            self.vt_series = 'D'     # H
        self.ref_height = 12.0       # J
        self.vt_bg_x = 1.0           # K
        self.ref_bg_x = 1.0          # K
        if digits == 2:
            self.vt_bg_width = 22.0  # L
            self.ref_bg_width = 22.0 # L
        else:
            self.vt_bg_width = 28.0  # L
            self.ref_bg_width = 28.0 # L
        self.ref_bg_radius = 11.0    # M
        if digits == 2:
            self.ref_bg_circle_x = self.width / 2
        else:
            bg_circle_x_offset = 3.0 # N
            self.ref_bg_circle_x = self.width / 2 - bg_circle_x_offset
        self.corner_radius = 1.5     # P
        # don't need guide border    # Q
        self.letter_height = 7.0     # R
        self.letter_series = 'B'     # R
        self.margin = 0.375
        
        self.vt_x = self.vt_bg_x
        self.vt_y = self.vt_bg_y + self.vt_bg_height/2 - self.vt_height/2
        self.vt_width = self.width - self.vt_x * 2
        
        self.ref_x = self.ref_bg_x * 2
        self.ref_y = self.ref_bg_y + self.ref_bg_height/2 - self.ref_height/2
        self.ref_width = self.width - self.ref_x * 2

        # Adjust the ref positioning
        if alt_marker:
            if len(ref) == 2:
                self.ref_series = 'D'
                self.letter_series = 'D'
            elif '1' in ref:
                self.ref_series = 'C'
                self.letter_series = 'C'
            else:
                self.ref_series = 'B'
                self.letter_series = 'B'
        else:
            if len(ref) == 1 or \
               (len(ref) == 2 and short_ref(ref)) or \
               (len(ref) == 3 and very_short_ref(ref)):
                self.ref_series = 'D'
            elif len(ref) <= 3:
                self.ref_series = 'C'
            else:
                self.ref_series = 'B'
                
        # Figure out where the ref bg curve hits the top and bottom lines.
        ref_bg_horiz = math.sqrt(self.ref_bg_radius**2 - (self.ref_bg_height/2)**2)
        self.ref_bg_corner_x = self.ref_bg_circle_x - ref_bg_horiz
        
        self.fg = COLOR_GREEN
        self.bg = COLOR_WHITE


# Last updated 2018-04-04 from https://en.wikipedia.org/wiki/List_of_state_highways_in_Vermont
TOWN_MAINTAINED_ROUTES = [
    '8A', '23', '35', '53',
    '121', '127', '139', '143', '144', '153',
    '215', '225', '235', '315',
    'F-5',
]
def make_shield(modifier, ref, style):
    if ref in TOWN_MAINTAINED_ROUTES:
        return USGenericStateShield(modifier, ref, STYLE_ELONGATED, sign=style == STYLE_SIGN).make_shield()
    
    if modifier == '':
        return make_base_shield(ref, style)
    else:
        shield_svg = make_base_shield(ref, style)
        banner_svgs = [banner(mod, int(shield_svg['width']), fg=COLOR_GREEN) for mod in modifier.split(';')]
        return compose_shields(banner_svgs + [shield_svg])

def make_base_shield(ref, style):
    params = VTRouteParams(ref, style)

    drawing = svgwrite.Drawing(size=(params.width, params.height))

    shield_bg = make_blank_shield(drawing, params)
    drawing.add(shield_bg)

    ref_g = make_ref(drawing, params)
    drawing.add(ref_g)
    
    return drawing

def make_blank_shield(drawing, params):
    background_g = drawing.g()

    if params.guide:
        background_g.add(drawing.rect(
            (params.margin/2, params.margin/2),
            (params.width - params.margin, params.height - params.margin),
            params.corner_radius - params.margin/2,
            stroke=params.bg, fill=params.fg, stroke_width=params.margin))
    else:
        background_g.add(drawing.rect(
            (0, 0), (params.width, params.height), params.corner_radius,
            stroke='none', fill=params.fg))

    background_g.add(
        drawing.rect(
            (params.vt_bg_x, params.vt_bg_y),
            (params.vt_bg_width, params.vt_bg_height),
            params.corner_radius,
            stroke='none', fill=params.bg))

    ref_bg_ul = (params.ref_bg_corner_x, params.ref_bg_y)
    ref_bg_ll = (ref_bg_ul[0], ref_bg_ul[1] + params.ref_bg_height)
    ref_bg_ur = (params.width - ref_bg_ul[0], ref_bg_ul[1])
    ref_bg_lr = (params.width - ref_bg_ll[0], ref_bg_ll[1])
    ref_bg = drawing.path(stroke='none', fill=params.bg)
    background_g.add(ref_bg)
    ref_bg.push('M', ref_bg_ul)
    ref_bg.push_arc(ref_bg_ll, 0, params.ref_bg_radius,
                    large_arc=False, angle_dir='-', absolute=True)
    ref_bg.push('H', ref_bg_lr[0])
    ref_bg.push_arc(ref_bg_ur, 0, params.ref_bg_radius,
                    large_arc=False, angle_dir='-', absolute=True)
    ref_bg.push('Z')

    background_g.add(
        render_text(
            drawing, 'VERMONT',
            params.vt_x, params.vt_y,
            params.vt_width, params.vt_height,
            params.vt_series, color=params.fg))
    
    return background_g

def make_size_func(params):
    def size_func(letter):
        if letter in set('0123456789'):
            return (params.ref_height, params.ref_series, 1.0)
        else:
            return (params.letter_height, params.letter_series, 1.0)
    return size_func


def make_ref(drawing, params):
    ref_g = render_text(
        drawing, params.ref, params.ref_x, params.ref_y,
        params.ref_width, params.ref_height, params.ref_series,
        color=params.fg, size_func=make_size_func(params))

    return ref_g
