#!/usr/bin/python

# http://wisconsindot.gov/dtsdManuals/traffic-ops/manuals-and-standards/signplate/mseries/M1-6.pdf
# http://wisconsindot.gov/dtsdManuals/traffic-ops/manuals-and-standards/signplate/mseries/M1-56.pdf
# http://wisconsindot.gov/dtsdManuals/traffic-ops/manuals-and-standards/signplate/mseries/M1-6b.pdf
# http://wisconsindot.gov/dtsdManuals/traffic-ops/manuals-and-standards/signplate/mseries/M1-56b.pdf
# http://wisconsindot.gov/dtsdManuals/traffic-ops/manuals-and-standards/signplate/mseries/M1-5a.pdf

import math
import svgwrite

from ..geometry import *
from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from .us_county import USCountyShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:WI'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        for county in WI_COUNTIES:
            network_registry['US:WI:' + county.upper()] = lambda m,r: USCountyShield(m, r, county.upper()).make_shield()
    else:
        network_registry['US:WI'] = lambda m,r: WIStateShield(m, r, style).make_shield()
        for county in WI_COUNTIES:
            network_registry['US:WI:' + county.upper()] = lambda m,r: WICountyShield(m, r, style).make_shield()
        
class WIStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(WIStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        
        self.arrow = self.ref == '32'
        
        A = 24.0
        C = 1.5
        F = 12.0
        G = 5.5
        H = 6.5
        I = 10.25
        J = 2.5
        K = 8.875
        L = 11.5
        M = 1.0
        N = 1.875
        O = 11.25
        P = 21.875

        R = 5.125
        S = 0.75
        T = 1.875
        U = 1.5
        V = 0.625
        W = 0.625
        X = 9.0
        Y = 0.5
        Z = 10.5
        
        self.width = A
        self.height = A
        self.stroke_width = self.width/2 - L
        self.ref_y = G
        self.ref_height = F
        if len(ref) <= 2:
            self.ref_series = 'D'
        else:
            self.ref_series = 'C'
            
        self.upper_left_corner = point(A/2 - I, A/2 + O - P)
        self.left_shoulder_right_end = point(A/2 - K, A/2 + O - P + J)
        self.left_shoulder_curve_right = point(A/2 - L + N, A/2 + O - P + J)
        self.left_shoulder_curve_left = point(A/2 - L, A/2 + O - P + J + N)
        self.lower_left_curve_left = point(A/2 - L, A/2 + O - N - N)
        self.lower_left_curve_right = point(A/2 - L + N, A/2 + O - N)
        self.lower_point_left = point(A/2 - M, A/2 + O - N)
        self.lower_point_center = point(A/2, A/2 + O)
        self.corner_radius = N
        self.sign_radius = C
        
        self.arrow_length = R
        self.arrow_offset = S
        self.arrow_width = T
        self.arrow_head_width = U
        self.arrow_crossbar_stroke = V
        self.arrow_stroke = W

        self.arrow_y = A - Z - R
        self.arrow_x1 = A/2 - L + S
        self.arrow_x2 = A/2 + L - S - T
        
        if self.arrow:
            self.ref_x = A/2 - X
        else:
            self.ref_x = self.left_shoulder_curve_left.real * 2
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE
        self.arrow_color = COLOR_RED

    @property
    def blank_key(self):
        if self.arrow:
            return 'us_wi-arrow'
        else:
            return 'us_wi'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.sign_radius,
                fill=self.fg))
            
        ul_corner = (self.upper_left_corner.real - self.stroke_width/2, self.upper_left_corner.imag - self.stroke_width/2)
        l_shoulder_r = (self.left_shoulder_right_end.real - self.stroke_width/2, self.left_shoulder_right_end.imag - self.stroke_width/2)
        l_shoulder_curve_r = (self.left_shoulder_curve_right.real + self.stroke_width/2, self.left_shoulder_curve_right.imag - self.stroke_width/2)
        l_shoulder_curve_l = (self.left_shoulder_curve_left.real - self.stroke_width/2, self.left_shoulder_curve_left.imag)
        ll_curve_l = (self.lower_left_curve_left.real - self.stroke_width/2, self.lower_left_curve_left.imag)
        ll_curve_r = (self.lower_left_curve_right.real, self.lower_left_curve_right.imag + self.stroke_width/2)
        point_l = (self.lower_point_left.real - self.stroke_width/2, self.lower_point_left.imag + self.stroke_width/2)
        point_c = (self.lower_point_center.real, self.lower_point_center.imag + self.stroke_width/2)

        ur_corner = (self.width - ul_corner[0], ul_corner[1])
        r_shoulder_l = (self.width - l_shoulder_r[0], l_shoulder_r[1])
        r_shoulder_curve_l = (self.width - l_shoulder_curve_r[0], l_shoulder_curve_r[1])
        r_shoulder_curve_r = (self.width - l_shoulder_curve_l[0], l_shoulder_curve_l[1])
        lr_curve_r = (self.width - ll_curve_l[0], ll_curve_l[1])
        lr_curve_l = (self.width - ll_curve_r[0], ll_curve_r[1])
        point_r = (self.width - point_l[0], point_l[1])
        
        outline = self.drawing.path(stroke=self.fg, fill=self.bg,
                                    stroke_width=self.stroke_width)
        bg_g.add(outline)
        outline.push('M', ul_corner)
        outline.push('L', l_shoulder_r)
        outline.push('H', l_shoulder_curve_r[0])
        outline.push_arc(
            l_shoulder_curve_l, 0, self.corner_radius + self.stroke_width/2,
            large_arc=False, angle_dir='-', absolute=True)
        outline.push('V', ll_curve_l[1])
        outline.push_arc(
            ll_curve_r, 0, self.corner_radius + self.stroke_width/2,
            large_arc=False, angle_dir='-', absolute=True)
        outline.push('H', point_l[0])
        outline.push('L', point_c)
        outline.push('L', point_r)
        outline.push('H', lr_curve_l[0])
        outline.push_arc(
            lr_curve_r, 0, self.corner_radius + self.stroke_width/2,
            large_arc=False, angle_dir='-', absolute=True)
        outline.push('V', r_shoulder_curve_r[1])
        outline.push_arc(
            r_shoulder_curve_l, 0, self.corner_radius + self.stroke_width/2,
            large_arc=False, angle_dir='-', absolute=True)
        outline.push('H', r_shoulder_l[0])
        outline.push('L', ur_corner)
        outline.push('Z')

        if self.arrow:
            bg_g.add(self.drawing.path(
                d=ARROW, fill=self.arrow_color,
                transform='translate({} {})'.format(self.arrow_x1, self.arrow_y)))
            bg_g.add(self.drawing.path(
                d=ARROW, fill=self.arrow_color,
                transform='translate({} {})'.format(self.arrow_x2, self.arrow_y)))
            
        return bg_g

class WICountyShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(WICountyShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        self.guide = style == STYLE_GUIDE

        if self.guide:
            if len(self.ref) <= 2:
                self.width = 24.0 # A
            else:
                self.width = 30.0 # V
            self.height = 20.0 # B
            self.corner_radius = 4.0 # C
            self.ref_height = 12.0 # F
            self.ref_y = 4.0 # G
            self.stroke_width = 0.5
            self.bg_y = self.stroke_width
            self.bg_height = self.height - self.stroke_width * 2
        else:
            self.width = 24.0 # A
            self.height = 24.0 # A
            self.ref_height = 10.0 # F
            ref_offset_y = 5.125 # H
            self.ref_y = self.height - ref_offset_y - self.ref_height
            self.corner_radius = 2.0  # L
            bg_half_width = 11.5 # M
            self.stroke_width = self.width/2 - bg_half_width
            bg_offset_y1 = 10.125  # N
            self.bg_y = self.height/2 - bg_offset_y1
            bg_offset_y2 = 9.375 # O
            self.bg_height = bg_offset_y1 + bg_offset_y2
        self.county_height = 3.0 # G
        self.county_y = 4.125 # I
        county_offset_x1 = 9.25 # J
        self.county_x = self.width/2 - county_offset_x1
        county_offset_x2 = 9.625 # K
        self.county_width = county_offset_x1 + county_offset_x2

        self.ref_x = self.stroke_width * 2
        self.ref_width = self.width - self.ref_x * 2
        
        if len(self.ref) == 1:
            self.ref_series = 'E'
        elif len(self.ref) <= 2:
            self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C'])
        else:
            self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['C', 'B'])

        self.county_series = 'E'
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_wi-county'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            
        bg_ul = (self.stroke_width/2, self.bg_y - self.stroke_width/2)
        bg_dims = (self.width - bg_ul[0] * 2, self.height - bg_ul[1] * 2)
        bg_radius = self.corner_radius + self.stroke_width/2
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.stroke_width))

        if not self.guide:
            bg_g.add(render_text(
                self.drawing, 'COUNTY', self.county_x, self.county_y,
                self.county_width, self.county_height, self.county_series,
                color=self.fg))

        return bg_g

WI_COUNTIES = [
    'Adams',
    'Ashland',
    'Barron',
    'Bayfield',
    'Brown',
    'Buffalo',
    'Burnett',
    'Calumet',
    'Chippewa',
    'Clark',
    'Columbia',
    'Crawford',
    'Dane',
    'Dodge',
    'Door',
    'Douglas',
    'Dunn',
    'Eau Claire',
    'Florence',
    'Fond du Lac',
    'Forest',
    'Grant',
    'Green',
    'Green Lake',
    'Iowa',
    'Iron',
    'Jackson',
    'Jefferson',
    'Juneau',
    'Kenosha',
    'Kewaunee',
    'La Crosse',
    'Lafayette',
    'Langlade',
    'Lincoln',
    'Manitowoc',
    'Marathon',
    'Marinette',
    'Marquette',
    'Menominee',
    'Milwaukee',
    'Monroe',
    'Oconto',
    'Oneida',
    'Outagamie',
    'Ozaukee',
    'Pepin',
    'Pierce',
    'Polk',
    'Portage',
    'Price',
    'Racine',
    'Richland',
    'Rock',
    'Rusk',
    'Sauk',
    'Sawyer',
    'Shawano',
    'Sheboygan',
    'Saint Croix',
    'Taylor',
    'Trempealeau',
    'Vernon',
    'Vilas',
    'Walworth',
    'Washburn',
    'Washington',
    'Waukesha',
    'Waupaca',
    'Waushara',
    'Winnebago',
    'Wood',
]

ARROW = 'm1.22172 3.8147v-0.97867h0.65328v-0.54666h-0.65328v-1.14133h-0.00849l0.00849-0.01467v-0.008l0.00848-0.008 0.01697-0.008h0.03394l0.01696 0.008 0.20362 0.16933h0.00849l0.03394 0.024 0.04241 0.00667 0.03395-0.00667 0.03393-0.016 0.03393-0.03066 0.01698-0.06934-0.01698-0.03066v-0.008l-0.65327-1.11733c0-0.000004-0.042426-0.0386705-0.042426-0.0386705l-0.025452-0.00800002h-0.050905l-0.025452 0.00799998-0.042421 0.0386665-0.653278 1.10933v0.008l-0.016968 0.03066 0.016968 0.06934 0.033935 0.03066 0.033938 0.016 0.033936 0.008 0.042421-0.008 0.033936-0.02266h0.008485l0.203618-0.17067h-0.008484l0.01697-0.00667h0.033935l0.016968 0.00667 0.008484 0.008v0.008l0.008486 0.016v1.148h-0.661764v0.54666h0.661764v0.97867l-0.483596 0.47066v0.70933l0.008484 0.03734 0.016967 0.032 0.033936 0.03066 0.033938 0.01467 0.033936 0.008h0.042421l0.042421-0.008 0.033936-0.02267h-0.008484l0.509048-0.46266h0.016969l0.008484-0.008 0.008484 0.008h0.016968l0.500564 0.45466v0.008l0.03394 0.02267 0.04242 0.008h0.04242l0.03394-0.008 0.03393-0.01467 0.03393-0.03066 0.01698-0.032 0.00848-0.03734h-0.00848v-0.70933l-0.49208-0.47066'
