#!/usr/bin/python

# FHWA Standard Highway Signs:
#   https://mutcd.fhwa.dot.gov/SHSe/Guide.pdf#page=4
# Page 4: M1-4 for independent use
# Page 5: M1-4 for guide sign use
# 
# Measurements:
# | Purpose           | I  | G  | Ind. | Guide | Ind. | Guide |
# | Digits            |    |    |  1,2 |   1,2 |  1,2 |     3 |
# |-------------------+----+----+------+-------+------+-------|
# | width             | A  | A  |   24 |    24 |   30 |    30 |
# | height            | B  | B  |   24 |    24 |   24 |    24 |
# | ref_y             | C  | C  |  5.5 |   5.5 |  5.5 |   5.5 |
# | ref_height        | D  | D  | 12 D |  12 D | 12 D |  12 D |
# | ref_space_below   | E* | E  |  6.5 |   6.5 |  6.5 |   6.5 |
# | ll_arc_radius     | E* | H* |  6.5 |     7 |  6.5 |     7 |
# | lower_arc_spacing | F  | F  |    1 |     1 |    4 |     4 |
# | lc_arc_radius     | G  | G* |  5.5 |     5 |  5.5 |     5 |
# | lower_arc_height  | H  | L  |  2.5 |     2 |  2.5 |     2 |
# | ul_arc_radius     | J  | H* |  7.5 |     7 |  7.5 |     7 |
# | uc_arc_width      | K  | K  |    7 |     7 |   10 |    10 |
# | uc_arc_radius     | L  | J  |  5.5 |     5 |  9.5 |     9 |
# | ul_diagonal       | M  | G* |  4.5 |     5 |  4.5 |     5 |
# | border_width      | N  |    |   .5 |       |   .5 |       |
# | corner_radius     | P  |    |  1.5 |       |  1.5 |       |
# 
# Colors:
#  * Foreground - black
#  * Background - white

# Historic US Route from Arizona's signs:
#   https://azdot.gov/business/engineering-and-construction/traffic/manual-of-approved-signs/route-marker-signs
#
# Measurements:
#  | Purpose           | I  | G  |  Ind. | Guide |
#  |-------------------+----+----+-------+-------|
#  | width             | A  | A  |    24 |    24 |
#  | height            | A  | A  |    24 |    24 |
#  | header_offset_y   | B  | B  |  3.25 |  3.25 |
#  | header_height     | C* | C* |    2E |    2E |
#  | header_margin     | D* | D* |     1 |     1 |
#  | divider_width     | E  | E  |   0.5 |   0.5 |
#  | us_margin         | D* | D* |     1 |     1 |
#  | us_height         | C* | C* |    2E |    2E |
#  | ref_margin        | F  | F  |   1.5 |   1.5 |
#  | ref_height        | G  | G  |    8D |    8D |
#  | ref_space_below   | H  | H  |     2 |   2.5 |
#  | corner_radius     | J  |    |   1.5 |       |
#  | lc_arc_radius     | K  | K  |     5 |     5 |
#  | ll_arc_offset_y   | L  |    |     5 |       |
#  | ll_arc_radius     | M  | M* |   6.5 |     7 |
#  | lower_arc_height  | N  | J  |   2.5 |     2 |
#  | ul_arc_radius     | P  | M* |   7.5 |     7 |
#  | ul_diagonal       | Q  | P  |   4.5 |     5 |
#  | border_width      | R  | N* |   0.5 |   0.5 |
#  | margin            |    | N* |       |   0.5 |
#  | uc_arc_radius     | S  | R  |   5.5 |     5 |
#  | header_offset_y   | T  | S  |  6.75 |  6.75 |
#  | us_offset_y       | U  | T  | 2.375 | 2.375 |
#  | lower_arc_spacing |    | L  |       |     1 |
#  | uc_arc_width      |    | Q  |       |     7 |


import math
import svgwrite

from ..font import get_font
from ..geometry import *
from ..shieldbase import ShieldBase
from ..utils import *

def register(network_registry, style):
    network_registry['US:US'] = lambda m,r: make_shield(m, r, style)

def make_shield(modifier, ref, style):
    if modifier == 'HISTORIC' and style != STYLE_GENERIC:
        return USHistoricShield('', ref, style).make_shield()
    else:
        return USRouteShield(modifier, ref, style).make_shield()
    
class USRouteShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(USRouteShield, self).__init__(modifier, ref)
        self.guide = style == STYLE_GUIDE
        self.sign = style == STYLE_SIGN
        
        self.border_width = 0.5
        
        if self.narrow:
            self.width = 24.0
            self.lower_arc_spacing = 1.0
            self.uc_arc_width = 7.0
        else:
            self.width = 30.0
            self.lower_arc_spacing = 4.0
            self.uc_arc_width = 10.0
            
        if self.guide:
            self.ll_arc_radius = 7.0
            self.lc_arc_radius = 5.0
            self.lower_arc_height = 2.0
            self.ul_arc_radius = 7.0
            self.ul_diagonal = 5.0
            self.extra_margin = self.border_width
            self.shield_margin = 0.0
        else:
            self.ll_arc_radius = 6.5
            self.lc_arc_radius = 5.5
            self.lower_arc_height = 2.5
            self.ul_arc_radius = 7.5
            self.ul_diagonal = 4.5
            self.corner_radius = 1.5
            self.shield_margin = self.border_width

        if self.narrow:
            if self.guide:
                self.uc_arc_radius = 7.0
            else:
                self.uc_arc_radius = 5.5
        else:
            if self.guide:
                self.uc_arc_radius = 9.0
            else:
                self.uc_arc_radius = 9.5
                
        self.height = 24.0
        self.ref_y = 5.5
        self.ref_height = 12.0
        self.ref_series = self.standard_ref_series()

        self.calc_arc_params()
        self.ref_x = self.ul_arc.center.real + self.ul_arc.radius + REF_MARGIN
        self.ref_width = self.width - self.ref_x * 2

        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    def calc_arc_params(self):
        self.uc_arc = Circle.from_chord_and_radius(
            point(self.width/2 - self.uc_arc_width, self.shield_margin),
            point(self.width/2, self.shield_margin),
            self.uc_arc_radius)
        
        self.ll_arc = Circle(
            point((self.shield_margin + self.ll_arc_radius),
                  (self.height - self.lower_arc_height - self.ll_arc_radius)),
            self.ll_arc_radius)
        
        # Find the ul arc center
        circle_from_diagonal = Circle(
            point(self.shield_margin, (self.shield_margin + self.ul_diagonal)),
            self.ul_arc_radius)
        circle_from_ll_arc = self.ll_arc.add_radius(self.ul_arc_radius)
        ul_arc_center_candidates = circle_from_diagonal.intersect_circle(circle_from_ll_arc)
        if ul_arc_center_candidates[0].real < ul_arc_center_candidates[1].real:
            ul_arc_center = ul_arc_center_candidates[0]
        else:
            ul_arc_center = ul_arc_center_candidates[1]
        self.ul_arc = Circle(ul_arc_center, self.ul_arc_radius)

        self.lc_arc = Circle.from_chord_and_radius(
            point(self.width/2, self.height - self.shield_margin),
            point(self.shield_margin + self.ll_arc_radius + self.lower_arc_spacing,
                  self.height - self.lower_arc_height),
            self.lc_arc_radius)

    @property
    def blank_key(self):
        return 'us_us-{}'.format(2 if self.narrow else 3)
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        if self.sign:
            bg_g.add(self.make_sign_background())
            border_offset = 0.0
            outline = self.drawing.path(fill=self.bg)
        else:
            border_offset = self.border_width/2
            outline = self.drawing.path(
                stroke=self.fg, fill=self.bg, stroke_width=self.border_width)
        bg_g.add(outline)

        ul_p1 = point(self.shield_margin, self.shield_margin + self.ul_diagonal)
        ul_p2 = point(self.shield_margin + self.ul_diagonal, self.shield_margin)
        ul_line = Line.from_points(ul_p1, ul_p2).translate(unit_vector(ul_p1, ul_p2) * 1j * border_offset)
        uc_arc = self.uc_arc.add_radius(-border_offset)
        ul_arc = self.ul_arc.add_radius(-border_offset)
        ll_arc = self.ll_arc.add_radius(border_offset)
        lc_arc = self.lc_arc.add_radius(-border_offset)

        ul_point = unpoint(ul_line.intersect_circle(uc_arc)[0])
        uc_point = unpoint(uc_arc.intersect_circle(uc_arc.translate(self.uc_arc_width))[0])
        ur_point = (self.width - ul_point[0], ul_point[1])
        l_point = unpoint(ul_line.intersect_circle(ul_arc)[0])
        r_point = (self.width - l_point[0], l_point[1])

        # Can't use circle intersection here because the two circles are
        # tengential.  That means that minor errors from floating point
        # math can put them just far enough apart that they don't have any
        # points in common.
        ul_to_ll_unit_vector = unit_vector(self.ul_arc.center, self.ll_arc.center)
        l_arc_transition = unpoint(self.ul_arc.center + ul_to_ll_unit_vector * (self.ul_arc.radius - border_offset))
        r_arc_transition = (self.width - l_arc_transition[0], l_arc_transition[1])

        ll_arc_right = unpoint(ll_arc.point_on_circle(x=self.shield_margin + self.ll_arc.radius)[1])
        lr_arc_left = (self.width - ll_arc_right[0], ll_arc_right[1])
        lc_arc_left = (ll_arc_right[0] + self.lower_arc_spacing, ll_arc_right[1])
        lc_arc_right = (self.width - lc_arc_left[0], lc_arc_left[1])
        lc_point = (self.width/2, self.height - self.shield_margin + border_offset)
        lc_point = unpoint(lc_arc.intersect_circle(lc_arc.translate(self.width - 2 * lc_arc.center.real))[0])
        
        outline.push('M', ul_point)
        outline.push_arc(uc_point, 0, self.uc_arc_radius - border_offset,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push_arc(ur_point, 0, self.uc_arc_radius - border_offset,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('L', r_point)
        outline.push_arc(r_arc_transition, 0, self.ul_arc_radius - border_offset,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push_arc(lr_arc_left, 0, self.ll_arc_radius + border_offset,
                         large_arc=False, angle_dir='+', absolute=True)
        outline.push('H', lc_arc_right[0])
        outline.push_arc(lc_point, 0, self.lc_arc_radius - border_offset,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push_arc(lc_arc_left, 0, self.lc_arc_radius - border_offset,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('H', ll_arc_right[0])
        outline.push_arc(l_arc_transition, 0, self.ll_arc_radius + border_offset,
                         large_arc=False, angle_dir='+', absolute=True)
        outline.push_arc(l_point, 0, self.ul_arc_radius - border_offset,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('Z')

        return bg_g

class USHistoricShield(USRouteShield):
    def __init__(self, modifier, ref, style):
        super(USHistoricShield, self).__init__(modifier, ref, style)
        self.modifier = ''
        
        self.lc_arc_radius = 5.0
        self.extra_margin = 0
        self.shield_margin = self.border_width

        if self.guide:
            self.uc_arc_radius = 7.0
            self.header_y = 3.25
        else:
            self.uc_arc_radius = 7.5
            self.header_y = self.shield_margin + 3.25

        self.header_height = 2.0
        self.header_series = 'E'
        self.us_height = 2.0
        self.us_series = 'E'
        self.us_width = 2 * 2.375
        header_margin = 1.0
        self.divider_width = 0.5
        self.divider_y = self.header_y + self.header_height + header_margin + self.divider_width/2
        us_margin = 1.0
        self.us_y = self.divider_y + self.divider_width/2 + us_margin
        self.ref_margin = 1.5
        self.ref_y = self.us_y + self.us_height + self.ref_margin
        self.ref_height = 8.0

        self.calc_arc_params()
        self.ref_x = self.ul_arc.center.real + self.ul_arc.radius + self.ref_margin
        self.ref_width = self.width - self.ref_x * 2
        self.divider_x = self.ul_arc.point_on_circle(y=self.divider_y)[0].real - self.border_width/2

        if self.guide:
            border_offset = math.sqrt(2 * (self.shield_margin + self.border_width)**2)
        else:
            border_offset = math.sqrt(2 * self.shield_margin**2)
        text_bound_line = Line.from_points(
            border_offset + self.ul_diagonal * 1j,
            self.ul_diagonal + border_offset * 1j)
        text_ul_corner = text_bound_line.point_on_line(y=self.header_y)
        self.header_x = text_ul_corner.real
        self.header_width = self.width - self.header_x * 2

        self.us_widths = [text_length(l, self.us_height, self.us_series) for l in 'US']
        self.us_xs = (
            self.width/2 - self.us_width/2,
            self.width/2 + self.us_width/2 - self.us_widths[-1])
            
        self.fg = COLOR_BROWN
        
    @property
    def blank_key(self):
        return 'us_us-historic-{}'.format(2 if self.narrow else 3)
        
    def make_blank_shield(self):
        bg_g = super(USHistoricShield, self).make_blank_shield()

        bg_g.add(self.drawing.line(
            (self.divider_x, self.divider_y),
            (self.width - self.divider_x, self.divider_y),
            stroke=self.fg, stroke_width=self.border_width))
            
        bg_g.add(render_text(
            self.drawing, 'HISTORIC', self.header_x, self.header_y,
            self.header_width, self.header_height, self.header_series,
            color=self.fg))
        bg_g.add(render_text(
            self.drawing, 'U', self.us_xs[0], self.us_y, self.us_widths[0],
            self.us_height, self.us_series, color=self.fg))
        bg_g.add(render_text(
            self.drawing, 'S', self.us_xs[1], self.us_y, self.us_widths[1],
            self.us_height, self.us_series, color=self.fg))
        
        return bg_g
