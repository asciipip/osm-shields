#!/usr/bin/python

# I couldn't find any public records on the state route marker design for
# Hawaii.  They do sell access to various of their DOT documents, but I
# didn't want to pay for them.  It looks like the design can be
# constructed from four overlapping circles (like Oregon), so I estimated
# some dimensions from a photo of one of their signs and proceeded from
# there.

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:HI'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    elif style == STYLE_SIGN:
        network_registry['US:HI'] = lambda m,r: HIStateShield(m, r, sign=True).make_shield()
    else:
        network_registry['US:HI'] = lambda m,r: HIStateShield(m, r).make_shield()

class HIStateShield(ShieldBase):
    def __init__(self, modifier, ref, sign=False):
        super(HIStateShield, self).__init__(modifier, ref)
        self.sign = sign
        
        self.ref_height = 8.5
        self.ref_y = 10.5
        self.ref_x = 3.0
        self.ref_width = self.width - self.ref_x * 2
        self.ref_series = 'C'

        self.upper_center = (self.width/2, 6.0)
        self.side_center = (25.5, 21.5)
        self.corner_center = (7.5, 15.5)
        self.lower_center = (self.width/2, 0.0)

        self.upper_radius = 4.5
        self.side_radius = 25.0
        self.corner_radius = 6.0
        self.lower_radius = 22.0

        self.border_width = 0.5
        self.sign_corner_radius = 1.5
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_hi'
    
    def make_blank_shield(self):
        outline = self.drawing.path(fill=self.bg)
        if not self.sign:
            outline['stroke'] = self.fg
            outline['stroke-width'] = self.border_width

        if self.sign:
            border_offset = 0
        else:
            border_offset = self.border_width/2
            
        # radii
        upper_radius = self.upper_radius + border_offset
        side_radius = self.side_radius + border_offset
        corner_radius = self.corner_radius + border_offset
        lower_radius = self.lower_radius + border_offset

        # Find arc transition points.  Each such point will be collinear
        # with the two arc centers.
        uc = self.upper_center[0] + self.upper_center[1] * 1j
        sc = self.side_center[0] + self.side_center[1] * 1j
        cc = self.corner_center[0] + self.corner_center[1] * 1j
        lc = self.lower_center[0] + self.lower_center[1] * 1j
        u2s = uc + (uc - sc) / abs(uc - sc) * upper_radius
        s2c = cc + (cc - sc) / abs(cc - sc) * corner_radius
        c2l = cc + (cc - lc) / abs(cc - lc) * corner_radius
        
        # Drawing
        outline.push('M', (u2s.real, u2s.imag))
        outline.push_arc((s2c.real, s2c.imag), 0, side_radius, large_arc=False,
                         angle_dir='-', absolute=True)
        outline.push_arc((c2l.real, c2l.imag), 0, corner_radius, large_arc=False,
                         angle_dir='-', absolute=True)
        outline.push_arc((self.width - c2l.real, c2l.imag), 0, lower_radius,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push_arc((self.width - s2c.real, s2c.imag), 0, corner_radius,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push_arc((self.width - u2s.real, u2s.imag), 0, side_radius,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push_arc((u2s.real, u2s.imag), 0, upper_radius,
                         large_arc=False, angle_dir='-', absolute=True)
        outline.push('Z')

        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.sign_corner_radius,
                fill=self.fg))
            bg_g.add(outline)
            return bg_g
        else:
            return outline
