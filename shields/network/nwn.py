#!/usr/bin/python


import math
import svgwrite
import syslog

from ..utils import *

def register(network_registry, style):
    network_registry['NWN'] = make_nwn_shield

def make_nwn_shield(modifier, ref):
    if ref == 'AT':
        return read_template('nwn:AT')
    syslog.syslog('unknown ref: (NWN;{};{})'.format(ref, modifier))
    return None
