#!/usr/bin/python

# Standard sign lookup:
#   http://apps.roads.maryland.gov/businesswithsha/bizStdsSpecs/desManualStdPub/publicationsonline/oots/internet_signbook.asp
# Maryland route marker designations:
#  * M1-5 - Maryland Route Shield (for Indepedent Use)
#  * M1-5(1) - Maryland Route Shield (for Guide Sign Use)
#  * M1-5(2) - Business Maryland Route Shield
#
# The business shield doesn't specify independent or guide use, which
# usually means both.  It does say "When used for independent use, all
# auxiliary signs shall be green legend on white background."  That also
# implies that the business design is used for both guide and independent
# signs.  On the other hand, there aren't many business routes in Maryland
# and some definitely get guide signs like US auxiliary routes (standard
# guide sign design in black and white, with "BUSINESS" text accompanying
# it).
#
# For the moment, until further field investigation can be performed, the
# cutout and sign styles will use green-on-white with a "BUSINESS" header,
# while the guide style will use black-on-white with an empty header and a
# "BUSINESS" banner.
#
# Also note Maryland's M1-4(1) and M1-4(2) signs.  Business US routes in
# Maryland get signed as either green-on-white with "BUSINESS" in the
# shield and no banner (for independent use) or white-on-green with
# "BUSINESS" in the shield and no banner (for guide sign use).  We're not
# doing any of that right now because there's no direct tagging for those
# specific signs.
#
# Measurements:
#  | Designation    | 5  | 5(1) | 5     | 5(1)  | 5(2)  | 5     | 5(1)  | 5(2)  |
#  | Digits         | *  | *    | 1,2   | 1,2   | 1,2   | 3     | 3     | 3     |
#  |----------------+----+------+-------+-------+-------+-------+-------+-------|
#  | width          | A  | A    | 24    | 24    | 24    | 30    | 30    | 30    |
#  | height         | B  | B    | 24    | 24    | 24    | 24    | 24    | 24    |
#  | margin         | C* | C*   | 1/2   | 1/2   | 1/2   | 1/2   | 1/2   | 1/2   |
#  | border_width   | C* | C*   | 1/2   | 1/2   | 1/2   | 1/2   | 1/2   | 1/2   |
#  | header_y       | D  |      | 2-5/8 |       | 2-5/8 | 2-5/8 |       | 2-5/8 |
#  | header_height  | E  |      | 3D    |       | 3D    | 3D    |       | 3D    |
#  | header_margin  | F  |      | 1-5/8 |       | 1-5/8 | 1-5/8 |       | 1-5/8 |
#  | divider_offset |    | D    |       | 7-1/4 |       |       | 7-1/4 |       |
#  | divider_width  | G  | E    | 1-1/4 | 1-1/4 | 1-1/4 | 1-1/4 | 1-1/4 | 1-1/4 |
#  | ref_margin     | H  | F    | 1-1/4 | 1-1/4 | 1-1/4 | 1-1/4 | 1-1/4 | 1-1/4 |
#  | ref_height     | J  | G    | 12D   | 12D   | 12D   | 12C   | 12C   | 12D   |
#  | ref_below      | K  | H    | 2-1/4 | 2-1/4 | 2-1/4 | 2-1/4 | 2-1/4 | 2-1/4 |
#  | corner_radius  | L  | J    | 1-1/2 | 1-1/2 | 1-1/2 | 1-1/2 | 1-1/2 | 1-1/2 |


import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    network_registry['US:MD'] = lambda m,r: us_md_shield(m, r, style)

def us_md_shield(modifier, ref, style):
    for r in MD_SCENIC_REFS:
        if ref.upper() == r.upper():
            return read_template('US:MD-Scenic')
    if style == STYLE_GENERIC:
        return USGenericStateShield(modifier, ref).make_shield()
    else:
        return MDStateShield(modifier, ref, style).make_shield()

class MDStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(MDStateShield, self).__init__(modifier, ref)
        self.guide = style == STYLE_GUIDE
        
        self.business = self.modifier == 'BUSINESS'
        
        if self.narrow:
            self.width = 24.0
        else:
            self.width = 30.0
        self.height = 24.0
        self.border_width = 0.5
        self.margin = 0.5
        self.header_y = 2.625
        self.header_height = 3.0
        self.header_series = 'D'
        header_margin = 1.625
        self.divider_width = 1.25
        self.divider_y = self.header_y + self.header_height + \
                         header_margin + self.divider_width/2
        self.ref_margin = 1.25
        self.ref_y = self.divider_y + self.divider_width/2 + self.ref_margin
        self.ref_height = 12.0
        self.ref_series = self.standard_ref_series()
        self.corner_radius = 1.5

        self.ref_x = self.margin + self.border_width + self.ref_margin
        self.ref_width = self.width - self.ref_x * 2
        self.header_x = self.ref_x
        self.header_width = self.width - self.header_x * 2
        
        if self.business:
            self.header_text = 'BUSINESS'
            if self.guide:
                self.fg = COLOR_BLACK
            else:
                self.fg = COLOR_GREEN
        else:
            self.header_text = 'MARYLAND'
            self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        if self.business and not self.guide:
            return 'us_md-business-{}'.format(int(self.width))
        else:
            return 'us_md-{}'.format(int(self.width))
    
    def make_shield(self):
        if self.modifier == '' or (self.business and not self.guide):
            return self.make_base_shield()
        else:
            return super(MDStateShield, self).make_shield()

    def make_blank_shield(self):
        bg_g = self.drawing.g()

        border_offset = self.margin + self.border_width/2
        bg_g.add(self.drawing.rect(
            (border_offset, border_offset),
            (self.width - border_offset * 2, self.height - border_offset * 2),
            self.corner_radius - border_offset,
            stroke=self.fg, fill=self.bg, stroke_width=self.border_width))
        bg_g.add(self.drawing.line(
            (border_offset, self.divider_y),
            (self.width - border_offset, self.divider_y),
            stroke=self.fg, stroke_width=self.divider_width))
        if not self.guide:
            bg_g.add(render_text(
                self.drawing, self.header_text, self.header_x, self.header_y,
                self.header_width, self.header_height, self.header_series,
                self.fg))
        
        return bg_g

MD_SCENIC_REFS = [
    'Historic National Road Scenic Byway',
    'Mountain Maryland Scenic Byway',
    'Chesapeake and Ohio Canal Scenic Byway',
    'Antietam Campaign Scenic Byway',
    'Catoctin Mountain Scenic Byway',
    'Old Main Streets Scenic Byway',
    'Mason and Dixon Scenic Byway',
    'Falls Road Scenic Byway',
    'Horses and Hounds Scenic Byway',
    'Lower Susquehanna Scenic Byway',
    'Charles Street Scenic Byway',
    'National Historic Seaport Scenic Byway',
    'Star-Spangled Banner Scenic Byway',
    'Booth\'s Escape Scenic Byway',
    'Roots and Tides Scenic Byway',
    'Religious Freedom Tour Scenic Byway',
    'Chesapeake Country Scenic Byway',
    'Harriet Tubman Underground Railroad Scenic Byway',
    'Blue Crab Scenic Byway',
]
