#!/usr/bin/python

# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=12
# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=13

import math
import svgwrite

from ...shieldbase import ShieldBase
from ...utils import *

class TXStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(TXStateShield, self).__init__(modifier, ref)

        if '-' in self.ref:
            parts = self.ref.split('-')
            self.ref = '-'.join(parts[:-1])
            self.suffix = parts[-1]
        else:
            self.suffix = ''

        self.width = 24.0         # A
        self.height = 24.0        # A
        self.border_width = 1.5   # B
        self.ref_series = 'D'     # D & H
        self.tx_height = 4.0      # F
        self.tx_series = 'D'      # F
        self.corner_radius = 1.5  # J/M
        if self.suffix == '':
            # M1-6T
            if len(self.ref) <= 2:
                self.ref_y = 4.0      # G
                self.ref_height = 9.0 # H
            else:
                self.ref_y = 5.0      # C
                self.ref_height = 7.0 # D
            tx_offset_y = 4.0         # E
        else:
            # M1-6TB
            if len(self.ref) <= 2:
                self.ref_y = 3.5      # K
                self.ref_height = 9.0 # L
            else:
                self.ref_y = 4.0      # C
                self.ref_height = 7.0 # D
            tx_space_below = 1.5      # G
            self.suf_height = 2.0     # H
            self.suf_series = 'D'     # H
            suf_offset_y = 2.5        # J
            tx_offset_y = tx_space_below + self.suf_height + suf_offset_y
            self.suf_y = self.height - suf_offset_y - self.suf_height

        self.tx_y = self.height - tx_offset_y - self.tx_height
        self.ref_x = self.border_width * 1.5
        self.ref_width = self.width - self.ref_x * 2
        self.tx_x = self.ref_x
        self.tx_width = self.width - self.tx_x * 2
        if self.suffix != '':
            self.suf_x = self.ref_x
            self.suf_width = self.width - self.suf_x * 2
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        if self.suffix == '':
            return 'us_tx-state'
        else:
            return 'us_tx-business'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        border_offset = self.border_width/2
        bg_ul = (border_offset, border_offset)
        bg_dims = (self.width - border_offset * 2, self.height - border_offset * 2)
        bg_radius = self.corner_radius - border_offset
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))

        bg_g.add(render_text(
            self.drawing, 'TEXAS', self.tx_x, self.tx_y, self.tx_width,
            self.tx_height, self.tx_series, self.fg))
        
        return bg_g

    def make_ref(self):
        ref = render_text(
            self.drawing, self.ref, self.ref_x, self.ref_y,
            self.ref_width, self.ref_height, self.ref_series,
            color=self.fg)

        if self.suffix == '':
            return ref
        else:
            ref_g = self.drawing.g()
            ref_g.add(ref)
            ref_g.add(render_text(
                self.drawing, self.suffix, self.suf_x, self.suf_y,
                self.suf_width, self.suf_height, self.suf_series,
                self.fg))
            return ref_g
