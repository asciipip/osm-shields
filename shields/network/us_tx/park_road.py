#!/usr/bin/python

# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=15
#
# NASA 1 also looks like this, so we hack it in.

import math
import svgwrite

from ...shieldbase import ShieldBase
from ...utils import *

class TXParkRouteShield(ShieldBase):
    def __init__(self, modifier, ref, subset='PARK'):
        super(TXParkRouteShield, self).__init__(modifier, ref)

        self.subset = subset
        
        self.width = 24.0        # A
        self.height = 24.0       # A
        self.border_width = 1.5  # B
        self.label_y1 = 3.5      # C
        self.label_height = 3.0  # D
        self.label_series = 'D'  # D
        label_spacing = 2.0      # E
        self.label_y2 = self.label_y1 + self.label_height + label_spacing
        self.ref_y = self.label_y2 + self.label_height + label_spacing
        self.ref_height = 7.0    # F
        self.ref_series = 'E'    # F
        self.corner_radius = 1.5 # H

        self.ref_width = 18.0
        self.ref_x = (self.width - self.ref_width)/2
        self.label_x = self.ref_x
        self.label_width = self.ref_width
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_tx-park'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        border_offset = self.border_width/2
        bg_ul = (border_offset, border_offset)
        bg_dims = (self.width - border_offset * 2, self.height - border_offset * 2)
        bg_radius = self.corner_radius - border_offset
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))

        bg_g.add(render_text(
            self.drawing, self.subset, self.label_x, self.label_y1,
            self.label_width, self.label_height, self.label_series,
            self.fg))
        bg_g.add(render_text(
            self.drawing, 'ROAD', self.label_x, self.label_y2,
            self.label_width, self.label_height, self.label_series,
            self.fg))
        
        return bg_g
