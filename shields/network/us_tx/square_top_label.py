#!/usr/bin/python

# http://ftp.dot.state.tx.us/pub/txdot-info/trf/shsd/2012/section_3.pdf#page=14

import math
import svgwrite

from ...shieldbase import ShieldBase
from ...utils import *

class TXSquareTopShield(ShieldBase):
    def __init__(self, label, modifier, ref):
        super(TXSquareTopShield, self).__init__(modifier, ref)

        self.label = label.upper()

        if len(self.ref) <= 2:
            self.ref_height = 9.0   # G
        elif len(self.ref) == 3:
            self.ref_height = 7.0   # E
        else:
            self.ref_height = 6.0   # H
        self.width = 24.0           # A
        self.height = 24.0          # A
        self.border_width = 1.5     # B
        self.label_y = 4.0          # C
        ref_space_above = 4.0       # C
        self.label_height = 4.0     # D
        self.ref_y = self.label_y + self.label_height + ref_space_above + 7.0/2 - self.ref_height/2
        if self.label == 'BELTWAY':
            self.label_series = 'C' # K
        else:
            self.label_series = 'D' # D
        self.ref_series = 'D'       # E, G & H
        self.ref_width = 18.0       # J
        self.ref_x = (self.width - self.ref_width)/2
        self.corner_radius = 1.5    # L
        
        self.label_x = self.ref_x
        self.label_width = self.ref_width
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_tx-square_top'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        border_offset = self.border_width/2
        bg_ul = (border_offset, border_offset)
        bg_dims = (self.width - border_offset * 2, self.height - border_offset * 2)
        bg_radius = self.corner_radius - border_offset
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.border_width))

        bg_g.add(render_text(
            self.drawing, self.label, self.label_x, self.label_y,
            self.label_width, self.label_height, self.label_series,
            self.fg))
        
        return bg_g
