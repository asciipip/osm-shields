#!/usr/bin/python

# In the absence of exact parameters from NYC, positions of the outline
# were derived empirically by overlaying test SVGs on the images in
#   https://www.dot.ny.gov/divisions/operating/oom/transportation-systems/repository/B-2011Supplement-adopted.pdf#page=256
# The result seems close enough.

import math
import re
import svgwrite

from ..shieldbase import ShieldBase
from ..font import get_font
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    network_registry['US:NY'] = lambda m,r: make_shield(m, r, style)

class NYStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(NYStateShield, self).__init__(modifier, ref)
        self.guide = style == STYLE_GUIDE
        self.sign = style == STYLE_SIGN
        
        if len(ref) <= 2:
            self.width = float(24)
        else:
            self.width = float(30)
        self.height = float(24)
        if self.guide:
            self.stroke_width = 0.5
        else:
            self.stroke_width = 1.0
        self.shoulder_y = float(4.5)
        self.bottom_curve_y = float(18)
        self.bottom_curve_radius = float(1.75)
        self.corner_radius = 1.5

        if len(ref) <= 2:
            self.ref_height = float(12)
            self.ref_series = 'D'
            self.let_height = float(10)
        elif len(ref) == 3:
            self.ref_height = float(10)
            self.ref_series = 'D'
            self.let_height = float(8)
        else:
            self.ref_height = float(10)
            self.ref_series = 'C'
            self.let_height = float(8)

        self.min_ref_y = self.shoulder_y
        self.max_ref_y = self.bottom_curve_y + self.bottom_curve_radius

        self.ref_x = self.stroke_width * 2
        self.ref_width = self.width - self.ref_x * 2
        self.ref_y = (self.min_ref_y + self.max_ref_y) / 2 - self.ref_height/2
        self.ref_size_func = self.make_size_func()
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ny-{}'.format(int(self.width))
    
    def make_size_func(self):
        def size_func(letter):
            if letter in set('0123456789'):
                return (self.ref_height, self.ref_series, 1.0)
            else:
                return (self.let_height, self.ref_series, 1.0)
        return size_func
    
    def make_blank_shield(self):
        outline = self.drawing.path(
            stroke=self.fg, fill=self.bg, stroke_width=self.stroke_width)
    
        ul_corner = (self.stroke_width/2, self.shoulder_y - self.stroke_width/2)
        u_center = (self.width/2, self.stroke_width/2)
        ur_corner = (self.width - ul_corner[0], ul_corner[1])
        ll_corner = (self.stroke_width/2, self.bottom_curve_y)
        lr_corner = (self.width - ll_corner[0], ll_corner[1])
        l_center = (self.width/2, self.height - self.stroke_width/2)
    
        ll_arc_radius = self.bottom_curve_radius + self.stroke_width/2
        ll_arc_target_c = \
            line_tangent_to_one_circle(
                l_center[0] + l_center[1] * 1j,
                (ll_corner[0] + ll_arc_radius + ll_corner[1] * 1j),
                ll_arc_radius,
                right=False)
        ll_arc_target = (ll_arc_target_c.real, ll_arc_target_c.imag)
        lr_arc_target = (self.width - ll_arc_target[0], ll_arc_target[1])
    
        outline.push('M', ul_corner)
        outline.push('C',
                     ((ul_corner[0] + u_center[0])/2, ul_corner[1]),
                     ((ul_corner[0] + u_center[0])/2, u_center[1]),
                     u_center)
        outline.push('C',
                     ((ur_corner[0] + u_center[0])/2, u_center[1]),
                     ((ur_corner[0] + u_center[0])/2, ur_corner[1]),
                     ur_corner)
        outline.push('L', lr_corner)
        outline.push_arc(lr_arc_target, 0, ll_arc_radius,
                         large_arc=False, angle_dir='+', absolute=True)
        outline.push('L', l_center)
        outline.push('L', ll_arc_target)
        outline.push_arc(ll_corner, 0, ll_arc_radius,
                         large_arc=False, angle_dir='+', absolute=True)
        outline.push('Z')

        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(outline)
            return bg_g
        else:
            return outline

class NYScenicParams:
    def __init__(self, name):
        self.name = name.replace('/', ' ')
        self.lines = name.split('/')

        self.width = float(24)
        self.height = float(24)
        self.stroke_width = float(0.5)

        self.ref_height = float(2)
        self.ref_series = 'C'
        ref_spacing = float(1)
        self.ref_ys = \
            list(reversed([21 - self.ref_height - (i * (self.ref_height + ref_spacing))
                           for i, l in enumerate(self.lines)]))

        self.leaf_y = self.stroke_width * 2
        self.leaf_height = self.ref_ys[0] - ref_spacing - self.leaf_y

        self.fg = COLOR_WHITE
        self.bg = COLOR_BROWN

class NYParkwayShield(NYStateShield):
    def __init__(self, name, style):
        super(NYParkwayShield, self).__init__('', '   ', style)
        self.ref = ''
        self.name = name

        self.lines = name.split()
        if len(self.lines) > 3:
            i = self.lines.index(min(self.lines, key=len))
            assert i > 0
            self.lines = self.lines[:i-1] + \
                         [' '.join(self.lines[i-1:i+1])] + \
                         self.lines[i+1:]

        max_ref_width = self.width - self.stroke_width * 3
        total_ref_height = self.bottom_curve_y + self.bottom_curve_radius - self.shoulder_y
        if len(self.lines) == 2:
            # Add spacing above and below
            self.ref_spacing = total_ref_height / (len(self.lines) * 3 + len(self.lines) + 1)
            self.upper_ys = [self.shoulder_y + self.ref_spacing,
                             self.shoulder_y + self.ref_spacing * 5]
        else:
            # No spacing above and below
            self.ref_spacing = total_ref_height / (len(self.lines) * 3 + len(self.lines) - 1)
            self.upper_ys = [self.shoulder_y + i * self.ref_spacing * 4 for i in range(0, len(self.lines))]
        self.upper_height = self.ref_spacing * 3
        self.lower_height = self.upper_height * 0.7
        self.upper_series = 'E'
        self.lower_series = 'F'

        # 60 degree angle for the upper case characters
        upper_angle = 60.0 / 180.0 * math.pi
        x_offset = (self.upper_height + self.ref_spacing) / math.tan(upper_angle)
        upper_font = get_font(self.upper_series)
        font_scale = self.upper_height / upper_font.cap_height
        first_offset = upper_font.glyphs[self.lines[0][0]].glyph_width * font_scale / 2
        unadjusted_xs = \
            [self.width/2 + i * x_offset + first_offset - \
             upper_font.glyphs[l[0]].glyph_width * font_scale / 2
             for i, l in enumerate(self.lines)]
        name_width = \
            max([text_length(l, self.upper_height, self.upper_series,
                             size_func=self.make_size_func()) + \
                 i * x_offset
                 for i, l in enumerate(self.lines)])
        if name_width > max_ref_width:
            ref_scale = max_ref_width / name_width
            self.upper_height *= ref_scale
            self.lower_height *= ref_scale
            name_width = \
                         max([text_length(l, self.upper_height, self.upper_series,
                                          size_func=self.make_size_func()) + \
                              i * x_offset
                              for i, l in enumerate(self.lines)])
        self.upper_xs = [x - name_width/2 for x in unadjusted_xs]

        # Adjust things downward a bit if they're too close to the top.
        if self.upper_xs[0] < self.width * 0.15:
            self.upper_ys = [y + self.stroke_width/2 for y in self.upper_ys]
            
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN

    def make_size_func(self):
        def size_func(letter):
            if letter == letter.upper():
                return (self.upper_height, self.upper_series, 1.0)
            else:
                return (self.lower_height, self.lower_series, 1.0)
        return size_func

    def make_ref(self):
        ref_g = self.drawing.g()
    
        upper_font = get_font(self.upper_series)
        upper_scale = self.upper_height / upper_font.cap_height
        for x, y, line in zip(self.upper_xs, self.upper_ys, self.lines):
            x_offset = -upper_font.glyphs[line[0]].left_space * upper_scale
            lower_offset = self.upper_height/2 - self.lower_height/2
            for i, s in enumerate(partition_upper_lower(line)):
                g_height, g_series, g_spacing = self.make_size_func()(s[0])
                g_font = get_font(g_series)
                g_scale = g_height / g_font.cap_height
                x_offset += g_font.glyphs[s[-1]].left_space * g_scale
                if i % 2 == 0:
                    # Upper case
                    ref_g.add(render_text(self.drawing, s, x + x_offset, y, self.width, self.upper_height, self.upper_series, color=self.fg, center=False))
                else:
                    # Lower case
                    ref_g.add(render_text(self.drawing, s, x + x_offset, y + lower_offset, self.width, self.lower_height, self.lower_series, color=self.fg, center=False))
                g_height, g_series, g_spacing = self.make_size_func()(s[-1])
                g_font = get_font(g_series)
                g_scale = g_height / g_font.cap_height
                x_offset += text_length(s, self.upper_height, self.upper_series,
                                        size_func=self.make_size_func()) + \
                            g_font.glyphs[s[-1]].right_space * g_scale
        return ref_g
    
class NYLighthouseParams:
    def __init__(self, ref, name):
        self.ref = ref
        lines = name.split()
        self.name1 = ' '.join(lines[0:-1])
        self.name2 = lines[-1]

        self.width = float(24)
        self.height = float(24)
        self.stroke_width = float(0.5)
        self.corner_radius = float(1.5)

        self.lighthouse_width = self.width - self.stroke_width * 5
        self.lighthouse_y = self.stroke_width * 3
        self.lighthouse_x = self.width/2 - self.lighthouse_width/2

        self.ref_width = float(14)
        self.ref_height = float(8.5)
        self.ref_x = float(8)
        self.ref_y = float(2.75)
        self.ref_series = 'D'

        self.name_width = self.width - self.stroke_width * 6
        self.name_height = float(2.5)
        self.name_x = self.stroke_width * 3
        self.name_y1 = float(16)
        self.name_y2 = float(19.5)
        for series in ['E', 'D', 'C']:
            self.name_series1 = series
            if text_length(self.name1, self.name_height, series) < self.name_width:
                break
        for series in ['E', 'D', 'C']:
            self.name_series2 = series
            if text_length(self.name2, self.name_height, series) < self.name_width:
                break
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

class NYLasalleParams:
    def __init__(self):
        self.name1 = 'LASALLE'
        self.name2 = 'EXPY'

        self.width = float(24)
        self.height = float(24)
        self.stroke_width = float(0.5)
        self.corner_radius = float(1.5)

        self.name_width = self.width - self.stroke_width * 4
        self.name_height = float(4)
        name_spacing = (self.height - self.stroke_width * 2 - self.name_height * 2) / 3
        self.name_x = self.stroke_width * 2
        self.name_y1 = self.stroke_width + name_spacing
        self.name_y2 = self.name_y1 + self.name_height + name_spacing
        self.name_series = 'C'
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN

        
SPECIAL_SHIELDS = [
    # Scenic "leaf" shields
    (('Arden Valley Road',), lambda s: make_scenic_shield('ARDEN VALLEY/ROAD')),
    (('Lake Welch Parkway',), lambda s: make_scenic_shield('LAKE WELCH/PARKWAY')),
    (('PIP', 'Palisades Interstate Parkway'), lambda s: make_scenic_shield('PALISADES/INTERSTATE/PARKWAY')),
    (('Seven Lakes Drive',), lambda s: make_scenic_shield('SEVEN LAKES/DRIVE')),
    (('Tiorati Brook Road',), lambda s: make_scenic_shield('TIORATI/BROOK ROAD')),

    # Parkways
    (('BMP', 'Bear Mountain State Parkway'), lambda s: NYParkwayShield('Bear Mtn Pkwy', s).make_shield()),
    (('BP', 'Belt Parkway'), lambda s: NYParkwayShield('Belt Pkwy', s).make_shield()),
    (('BRP', 'Bronx River Parkway'), lambda s: NYParkwayShield('Bronx River Pkwy', s).make_shield()),
    (('CCP', 'Cross Country Parkway'), lambda s: NYParkwayShield('Cross County Pkwy', s).make_shield()),
    (('CIP', 'Cross Island Parkway'), lambda s: NYParkwayShield('Cross Island Pkwy', s).make_shield()),
    (('FDR', 'FDRD', 'FDR Drive', 'Franklin D. Roosevelt East River Drive'), lambda s: NYParkwayShield('FDR Drive', s).make_shield()),
    (('HHP', 'Henry Hudson Parkway'), lambda s: NYParkwayShield('Henry Hudson Pkwy', s).make_shield()),
    (('HRD', 'Harlem River Drive'), lambda s: NYParkwayShield('Harlem River Drive', s).make_shield()),
    (('HRP', 'Hutchinson River Parkway'), lambda s: NYParkwayShield('Hutchinson River Pkwy', s).make_shield()),
    (('JRP', 'Jackie Robinson Parkway'), lambda s: NYParkwayShield('Jackie Robinson Pkwy', s).make_shield()),
    (('Korean War Veterans Parkway',), lambda s: NYParkwayShield('Korean War Veterans Pkwy', s).make_shield()),
    (('MP', 'Mosholu Parkway'), lambda s: NYParkwayShield('Mosholu Pkwy', s).make_shield()),
    (('PP', 'Pelham Parkway', 'Bronx and Pelham Parkway'), lambda s: NYParkwayShield('Pelham Pkwy', s).make_shield()),
    (('SBP', 'Sprain Brook Parkway'), lambda s: NYParkwayShield('Sprain Brook Pkwy', s).make_shield()),
    (('SMP', 'Saw Mill River Parkway'), lambda s: NYParkwayShield('Saw Mill Pkwy', s).make_shield()),
    (('TSP', 'Taconic State Parkway'), lambda s: NYParkwayShield('Taconic State Pkwy', s).make_shield()),

    # Long Island "lighthouse" parkways
    (('B', 'Bethpage State Parkway'), lambda s: make_lighthouse_shield('B', 'BETHPAGE PARKWAY')),
    (('H', 'Heckscher State Parkway'), lambda s: make_lighthouse_shield('H', 'HECKSCHER PARKWAY')),
    (('L', 'Loop Parkway'), lambda s: make_lighthouse_shield('L', 'LOOP PARKWAY')),
    (('M', 'Meadowbrook State Parkway'), lambda s: make_lighthouse_shield('M', 'MEADOWBROOK PARKWAY')),
    (('N', 'Northern State Parkway'), lambda s: make_lighthouse_shield('N', 'NORTHERN PARKWAY')),
    (('O', 'Ocean Parkway'), lambda s: make_lighthouse_shield('O', 'OCEAN PARKWAY')),
    (('RM', 'Robert Moses Causeway'), lambda s: make_lighthouse_shield('RM', 'ROBERT MOSES CAUSEWAY')),
    (('SA', 'Sagtikos State Parkway'), lambda s: make_lighthouse_shield('SA', 'SAGTIKOS PARKWAY')),
    (('SM', 'Sunken Meadow State Parkway'), lambda s: make_lighthouse_shield('SM', 'SUNKEN MEADOW PARKWAY')),
    (('SO', 'Southern State Parkway'), lambda s: make_lighthouse_shield('SO', 'SOUTHERN PARKWAY')),
    (('W', 'Wantagh State Parkway'), lambda s: make_lighthouse_shield('W', 'WANTAGH PARKWAY')),

    # The LaSalle Expressway
    (('LaSalle Expressway',), lambda s: make_lasalle_shield()),

    # One-offs where it was easier to just store a premade SVG
    (('GCP', 'Grand Central Parkway'), lambda s: read_template('US:NY-GCP')),
    (('LOSP', 'Lake Ontario State Parkway'), lambda s: read_template('US:NY-LOSP')),
    (('RMSP', 'Robert Moses State Parkway'), lambda s: read_template('US:NY-RMSP')),
]
SPECIAL_SHIELD_MAPPING = {}
for names, action in SPECIAL_SHIELDS:
    for name in names:
        assert name.upper() not in SPECIAL_SHIELD_MAPPING, 'duplicate ref? {} in {}'.format(name, names)
        SPECIAL_SHIELD_MAPPING[name.upper()] = action

def make_shield(modifier, ref, style):
    if style == STYLE_GENERIC:
        return USGenericStateShield(modifier, ref).make_shield()
    
    if ref in SPECIAL_SHIELD_MAPPING:
        return SPECIAL_SHIELD_MAPPING[ref](style)

    return NYStateShield(modifier, ref, style).make_shield()


#################
### Scenic Routes

def make_scenic_shield(name):
    params = NYScenicParams(name)

    drawing = svgwrite.Drawing(size=(params.width, params.height))

    shield_bg = make_blank_scenic_shield(drawing, params)
    drawing.add(shield_bg)

    ref_g = make_scenic_ref(drawing, params)
    drawing.add(ref_g)
    
    return drawing

def make_blank_scenic_shield(drawing, params):
    background_g = drawing.g()

    background_g.add(drawing.circle((params.width/2, params.height/2),
                                    params.height/2 - params.stroke_width/2,
                                    stroke=params.fg, fill=params.bg,
                                    stroke_width=params.stroke_width))

    
    leaf_scale = params.leaf_height / SCENIC_LEAF_HEIGHT
    leaf_x = params.width/2 - SCENIC_LEAF_WIDTH/2 * leaf_scale
    background_g.add(drawing.path(
        d=SCENIC_LEAF_D, stroke=params.fg, fill='none', stroke_width=0.5,
        transform='matrix({0} 0 0 {0} {1} {2})'.format(leaf_scale, leaf_x, params.leaf_y)))
    
    return background_g

def make_scenic_ref(drawing, params):
    ref_g = drawing.g()

    for y, line in zip(params.ref_ys, params.lines):
        ref_g.add(
            render_text(
                drawing, line, 0, y, params.width, params.ref_height,
                params.ref_series, color=params.fg))

    return ref_g


#####################################
### "Lighthouse" Long Island Parkways

def make_lighthouse_shield(ref, name):
    params = NYLighthouseParams(ref, name)

    drawing = svgwrite.Drawing(size=(params.width, params.height))

    shield_bg = make_blank_lighthouse_shield(drawing, params)
    drawing.add(shield_bg)

    ref_g = make_lighthouse_ref(drawing, params)
    drawing.add(ref_g)
    
    return drawing

def make_blank_lighthouse_shield(drawing, params):
    background_g = drawing.g()

    background_g.add(drawing.rect(
        (params.stroke_width * 1.5, params.stroke_width * 1.5),
        (params.width - params.stroke_width * 3, params.height - params.stroke_width * 3),
        params.corner_radius - params.stroke_width * 1.5,
        stroke=params.fg, fill=params.bg,
        stroke_width=params.stroke_width))

    lighthouse_scale = params.lighthouse_width / LIGHTHOUSE_WIDTH
    background_g.add(drawing.path(
        d=LIGHTHOUSE_D, fill=params.fg,
        transform='matrix({0} 0 0 {0} {1} {2})'.format(
            lighthouse_scale, params.lighthouse_x, params.lighthouse_y)))
    
    return background_g

def make_lighthouse_ref(drawing, params):
    ref_g = drawing.g()

    ref_g.add(
        render_text(drawing, params.ref, params.ref_x, params.ref_y,
                    params.ref_width, params.ref_height, params.ref_series,
                    color=params.fg))

    ref_g.add(
        render_text(drawing, params.name1, params.name_x, params.name_y1,
                    params.name_width, params.name_height, params.name_series1,
                    color=params.fg))
    ref_g.add(
        render_text(drawing, params.name2, params.name_x, params.name_y2,
                    params.name_width, params.name_height, params.name_series2,
                    color=params.fg))
    
    return ref_g


######################
### LaSalle Expressway

def make_lasalle_shield():
    params = NYLasalleParams()

    drawing = svgwrite.Drawing(size=(params.width, params.height))

    shield_bg = make_blank_lasalle_shield(drawing, params)
    drawing.add(shield_bg)

    ref_g = make_lasalle_ref(drawing, params)
    drawing.add(ref_g)
    
    return drawing

def make_blank_lasalle_shield(drawing, params):
    background_g = drawing.g()

    background_g.add(drawing.rect(
        (params.stroke_width * 0.5, params.stroke_width * 0.5),
        (params.width - params.stroke_width * 2, params.height - params.stroke_width * 2),
        params.corner_radius - params.stroke_width * 0.5,
        stroke=params.fg, fill=params.bg,
        stroke_width=params.stroke_width))
    
    return background_g

def make_lasalle_ref(drawing, params):
    ref_g = drawing.g()

    ref_g.add(
        render_text(drawing, params.name1, params.name_x, params.name_y1,
                    params.name_width, params.name_height, params.name_series,
                    color=params.fg))
    ref_g.add(
        render_text(drawing, params.name2, params.name_x, params.name_y2,
                    params.name_width, params.name_height, params.name_series,
                    color=params.fg))
    
    return ref_g

SCENIC_LEAF_WIDTH = 10.125
SCENIC_LEAF_HEIGHT = 11.0
SCENIC_LEAF_D = 'M 5.0625,11 V 9.09322 C 7.061372,10.188421 8.730274,9.600804 9.3875,8 L 8.5625,7.95 9.8125,5 9.3125,4.75 9.6375,3.75 8.8125,4 8.4375,3 7.0625,3.75 6.9375,1.5 5.6875,1.6 5.0625,0.5 4.4375,1.6 3.1875,1.5 3.0625,3.75 1.6875,3 1.3125,4 0.48749999,3.75 0.81249999,4.75 0.31249999,5 1.5625,7.95 0.73749999,8 C 1.4480924,9.399385 2.7802654,10.336586 5.0625,9.09322 6.021737,7.848051 7.58621,8.594433 8.1875,7 H 6.8875 L 8.3125,5.325 8.0625,5 8.3125,4.325 7.5625,4.5 7.3875,4.275424 6.4375,4.75 7.0875,3.75 6.8125,3.5 6.9375,2.875 6.3875,3.175 6.1875,3 5.6875,3.625 V 2.235 L 5.4375,2.25 5.0625,1.6 4.6875,2.25 4.4375,2.235 V 3.625 L 3.9375,3 3.7375,3.175 3.1875,2.875 3.3125,3.5 3.0375,3.75 3.6875,4.75 2.7375,4.275 2.5625,4.5 1.8125,4.324595 2.0625,5 1.8125,5.325 3.2375,7 H 1.9375 C 2.4316695,8.419154 4.092475,7.882955 5.0625,9.09322'
LIGHTHOUSE_WIDTH = 21.0
LIGHTHOUSE_HEIGHT = 13.0
LIGHTHOUSE_D = 'm0.0025002 12.987c-0.0030462-0.007-0.0033528-0.049-0.0006775-0.091 0.0026704-0.042 0.012147-0.353 0.021055-0.69 0.012937-0.489 0.018939-0.614 0.029812-0.622 0.007487-0.005 0.18522-0.051 0.39496-0.102s0.3872-0.098 0.39436-0.105c0.01002-0.009 0.05809-1.018 0.20839-4.3764 0.1075-2.4006 0.1923-4.3677 0.1883-4.3715-0.0039-0.0037-0.0252-0.0082-0.0474-0.01l-0.0402-0.0031v-0.5926l0.2365-0.0049 0.2366-0.005 0.0026-0.321 0.0026-0.321 0.2842-0.279c0.1563-0.15345 0.2887-0.27901 0.2944-0.27901 0.0056 0 0.138 0.12556 0.2943 0.27901l0.2842 0.279 0.0026 0.321 0.0026 0.321 0.1963 0.005 0.1963 0.0049v0.5926l-0.0503 0.0031c-0.0277 0.0017-0.0537 0.0063-0.0577 0.0102-0.0063 0.0061 0.1917 6.4216 0.2094 6.7854 0.0059 0.1215 0.0079 0.1313 0.025 0.1269 0.0102-0.0025 0.2065-0.123 0.4362-0.2675 0.2298-0.1445 0.4248-0.2628 0.4334-0.2628 0.012 0 1.4631 0.9562 1.4976 0.9869 0.0041 0.0038 0.0299 0.3018 0.0571 0.6618 0.029 0.382 0.0541 0.659 0.0603 0.666 0.0059 0.005 0.1818 0.034 0.3909 0.062l0.3802 0.053 0.5434-0.162c0.4383-0.13 0.5469-0.16 0.561-0.15 0.0096 0.006 0.251 0.257 0.5366 0.559 0.2855 0.301 0.5336 0.558 0.5512 0.571 0.0177 0.013 0.1029 0.058 0.1894 0.101l0.1572 0.077 0.2102 0.04c0.1373 0.026 0.2653 0.043 0.3695 0.05l0.1593 0.011 0.1426-0.03c0.2812-0.06 0.4052-0.09 0.4542-0.11 0.04-0.015 0.069-0.039 0.136-0.113 0.047-0.052 0.091-0.093 0.097-0.091 0.007 0.001 0.059 0.056 0.116 0.12 0.058 0.065 0.116 0.126 0.13 0.136 0.013 0.01 0.111 0.045 0.216 0.077l0.191 0.06 0.237 0.02 0.237 0.019 0.316-0.02 0.316-0.019 0.177-0.065c0.097-0.036 0.186-0.071 0.197-0.078s0.07-0.053 0.13-0.103c0.061-0.049 0.115-0.09 0.121-0.09s0.034 0.029 0.061 0.065c0.068 0.088 0.099 0.11 0.26 0.186 0.099 0.046 0.186 0.077 0.302 0.109 0.149 0.04 0.182 0.046 0.369 0.06l0.206 0.015 0.194-0.031c0.233-0.036 0.516-0.114 0.688-0.187 0.107-0.047 0.128-0.06 0.256-0.168 0.076-0.065 0.144-0.118 0.15-0.118s0.061 0.049 0.121 0.108c0.061 0.06 0.114 0.109 0.119 0.109 0.004 0 0.024 0.011 0.045 0.024s0.108 0.051 0.194 0.084c0.136 0.052 0.185 0.065 0.371 0.099 0.119 0.022 0.243 0.04 0.278 0.04 0.034 0 0.152-0.013 0.263-0.03 0.194-0.029 0.205-0.031 0.372-0.098 0.28-0.112 0.391-0.168 0.501-0.251 0.055-0.041 0.104-0.075 0.11-0.075s0.068 0.054 0.137 0.121c0.117 0.111 0.14 0.128 0.291 0.213l0.163 0.093 0.194 0.047c0.183 0.045 0.205 0.049 0.402 0.059 0.202 0.01 0.216 0.01 0.403-0.015 0.337-0.045 0.537-0.106 0.652-0.198l0.068-0.054 0.386 0.003 0.386 0.002v0.534l-10.496 0.002c-9.1061 0.002-10.497 0.001-10.501-0.012zm2.5024-1.951l0.005-0.983 0.2133-0.1383c0.1174-0.0762 0.216-0.1411 0.2192-0.1442 0.0071-0.0071-0.043-1.6073-0.0507-1.6196-0.0077-0.0122-1.5615-0.0115-1.5692 0.0006-0.008 0.0127-0.1875 3.8525-0.1806 3.8635 0.003 0.005 0.3037 0.008 0.6817 0.006l0.6762-0.002 0.0051-0.983zm0.3366-6.0932c0.0071-0.0112-0.0428-2.3109-0.0505-2.3229-0.0073-0.0117-1.2595-0.0116-1.2668 0-0.0077 0.0121-0.0576 2.3116-0.0505 2.3229 0.0076 0.0121 1.3602 0.0122 1.3678 0zm-0.7141-3.2453v-0.3161h-0.312l-0.0027 0.3061c-0.0014 0.1683-0.0004 0.3118 0.0023 0.3188 0.0039 0.0097 0.0407 0.012 0.1587 0.01l0.1537-0.0028v-0.316zm0.4731 0v-0.3161h-0.312l-0.0027 0.3061c-0.0015 0.1683-0.0004 0.3118 0.0023 0.3188 0.0038 0.0097 0.0407 0.012 0.1587 0.01l0.1537-0.0028v-0.316z'

