#!/usr/bin/python

# http://www.dtop.gov.pr/pdf/Volumen2-STS-19.pdf
#
# Puerto Rico's state routes all belong to one of four networks:
#  * Primary (M1-6A; white on blue)
#  * Urban Primary (M1-6B; black on white)
#  * Secondary (M1-6C; same as MUTCD county shield without the word "COUNTY")
#  * Tertiary (M1-6D; same as MUTCD state shield, always-circular variant)
#
# The same route number might transition between any of those multiple
# times.  When speaking of the route, people just use the number without
# distinguishing between the signing at different places along the route.
#
# I don't see any differentiation among the different networks in OSM
# tagging, so for the moment, everything gets signed as a Primary route.

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:PR'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:PR'] = lambda m,r: PRStateShield(m, r, style).make_shield()

class PRStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(PRStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        
        self.width = 24.0          # A
        self.height = 24.0         # B
        if len(self.ref) == 1:
            self.ref_height = 12.0 # E
            self.ref_series = 'D'  # E
            ref_offset = 7.12      # F
        elif len(self.ref) == 2:
            self.ref_height = 10.0 # E
            self.ref_series = 'D'  # E
            ref_offset = 8.58      # F
        else:
            self.ref_height = 8.0  # E
            self.ref_series = 'C'  # E
            ref_offset = 9.88      # F
        arc_center_offset_x = 9.27 # G
        arc_center_y = 9.57        # H
        self.arc_radius = 12.5     # J
        self.border_width = 2.0    # K
        self.corner_radius = 1.5   # L
        
        self.arc_center = (self.width - arc_center_offset_x, arc_center_y)
        self.ref_y = self.height - ref_offset - self.ref_height

        self.ref_x = self.border_width * 1.5
        self.ref_width = self.width - self.ref_x * 2
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_BLUE

    @property
    def blank_key(self):
        return 'us_pr'
    
    def make_blank_shield(self):
        if self.sign:
            border_offset = 0
            outline = self.drawing.path(fill=self.bg)
        else:
            border_offset = self.border_width/2
            outline = self.drawing.path(stroke=self.fg, fill=self.bg,
                                        stroke_width=self.border_width)

        arc_radius = self.arc_radius + border_offset

        ul = (self.border_width - border_offset, self.border_width - border_offset)
        ur = (self.width - ul[0], ul[1])
        ml = (self.border_width - border_offset, self.arc_center[1])
        mr = (self.width - ml[0], ml[1])
        b = (self.width/2, self.height - self.border_width + border_offset)
        
        # Drawing
        outline.push('M', ul)
        outline.push('V', ml[1])
        outline.push_arc(b, 0, arc_radius, large_arc=False, angle_dir='-',
                         absolute=True)
        outline.push_arc(mr, 0, arc_radius, large_arc=False, angle_dir='-',
                         absolute=True)
        outline.push('V', ur[1])
        outline.push('Z')

        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.make_sign_background())
            bg_g.add(outline)
            return bg_g
        else:
            return outline
