#!/usr/bin/python

# Ohio Sign Designs and Marking Manual, Chapter 4d, Guide Signs - Markers
# http://www.dot.state.oh.us/Divisions/Engineering/Roadway/DesignStandards/traffic/SDMM/Documents/SDMM_Chapter04d_Markers_071516Revision_041216.pdf

import math
import future
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from .us_county import USCountyShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:OH'] = lambda m,r: USGenericStateShield(m, r).make_shield()
        network_registry['US:OH:MOE'] = lambda m,r: USCountyShield(m, r, 'Monroe').make_shield()
        network_registry['US:OH:PAU'] = lambda m,r: USCountyShield(m, r, 'Paulding').make_shield()
    else:
        network_registry['US:OH'] = lambda m,r: OHStateShield(m, r, style).make_shield()
        network_registry['US:OH:MOE'] = lambda m,r: OHMonroeCountyShield(m, r).make_shield()
        network_registry['US:OH:PAU'] = lambda m,r: OHPauldingCountyShield(m, r).make_shield()
        network_registry['US:OH:LOG:PLEASANT'] = lambda m,r: OHPleasantTownshipShield(m, r).make_shield()

TURNPIKE_NAMES = [
    'OHIO TURNPIKE',
    'JAMES W. SHOCKNESSY OHIO TURNPIKE',
]
class OHStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(OHStateShield, self).__init__(modifier, ref)
        self.sign = style == STYLE_SIGN
        self.guide = style == STYLE_GUIDE
        self.turnpike = self.ref in TURNPIKE_NAMES

        if len(ref) <= 2 or self.turnpike:
            self.width = 24.0
            self.outline = OHIO_OUTLINE_2
            self.ref_height = 12.0
            ref_y_offset = 6.5
            ref_right_space = 2.0
        else:
            self.width = 30.0
            self.outline = OHIO_OUTLINE_3
            self.ref_height = 10.0
            ref_y_offset = 6.0
            ref_right_space = 2.5
        self.height = 24.0
        if self.guide:
            self.stroke_width = 0.5
        else:
            self.stroke_width = 0.75
        self.ref_y = self.height/2 - ref_y_offset
        self.ref_x = self.stroke_width * 1.5
        self.ref_width = self.width - self.ref_x - ref_right_space
        self.corner_radius = 1.5  # assumed
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])
        if self.turnpike:
            self.ohio_x = self.width/2 - 8.875
            self.ohio_height = 6.0
            self.ohio_y = self.height/2 - 1.75 - self.ohio_height
            self.ohio_width = 8.875 + 8.75
            self.ohio_series = 'D'
            self.tp_x = self.stroke_width + 1.75
            self.tp_y = self.height/2 + 1.25
            self.tp_height = 3.5
            self.tp_width = 18.313
            self.tp_series = 'C'

        if self.turnpike:
            self.fg = COLOR_GREEN
        else:
            self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        if self.turnpike:
            return 'us_oh-turnpike'
        else:
            return 'us_oh-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        outline = self.drawing.path(d=self.outline, fill=self.bg)

        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            outline['transform'] = 'matrix({0} 0 0 {0} {1} {1})'.format(
                (self.width - self.stroke_width) / self.width,
                self.stroke_width/2)
            bg_g.add(outline)
            return bg_g
        else:
            outline['stroke'] = self.fg
            outline['stroke-width'] = self.stroke_width
            return outline

    def make_ref(self):
        if self.turnpike:
            return self.make_turnpike_ref()
        return super(OHStateShield, self).make_ref()

    def make_turnpike_ref(self):
        ref_g = self.drawing.g()

        ref_g.add(render_text(
            self.drawing, 'OHIO', self.ohio_x, self.ohio_y, self.ohio_width,
            self.ohio_height, self.ohio_series, color=self.fg))
        ref_g.add(render_text(
            self.drawing, 'TURNPIKE', self.tp_x, self.tp_y, self.tp_width,
            self.tp_height, self.tp_series, color=self.fg))

        return ref_g

class OHMonroeCountyShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(OHMonroeCountyShield, self).__init__(modifier, ref)

        self.width = 24.0
        self.height = 36.0

        self.border = 0.375
        self.stroke_width = 0.625
        self.corner_radius = 1.5

        self.ref_x = 2.0
        self.ref_width = self.width - self.ref_x * 2
        self.ref_height = 12.5

        self.monroe_height = 5.5
        self.co_height = 5.5

        self.monroe_series = 'B'
        self.ref_series = 'B'
        self.co_series = 'B'
        
        spacing = (self.height - (self.border + self.stroke_width) * 2 - \
                   self.ref_height - self.monroe_height - self.co_height) / 4
        self.monroe_y = self.border + self.stroke_width + spacing
        self.ref_y = self.monroe_y + self.monroe_height + spacing
        self.co_y = self.ref_y + self.ref_height + spacing
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN

    @property
    def blank_key(self):
        return 'us_oh-MOE'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_ul = (self.border + self.stroke_width/2, self.border + self.stroke_width/2)
        bg_dims = (self.width - bg_ul[0] * 2, self.height - bg_ul[1] * 2)
        bg_radius = self.corner_radius - self.border - self.stroke_width/2
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.stroke_width))

        bg_g.add(render_text(
            self.drawing, 'MONROE', self.ref_x, self.monroe_y, self.ref_width,
            self.monroe_height, self.monroe_series, color=self.fg))
        bg_g.add(render_text(
            self.drawing, 'CO', self.ref_x, self.co_y, self.ref_width,
            self.co_height, self.co_series, color=self.fg))

        return bg_g

class OHPauldingCountyShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(OHPauldingCountyShield, self).__init__(modifier, ref)

        self.width = 24.0
        self.height = 18

        self.stroke_width = 1.0
        self.corner_radius = 0.25

        self.ref_x = 2.0
        self.ref_y = 2.5
        self.ref_width = self.width - self.ref_x * 2
        self.ref_height = 11.0
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])

        self.co_x = self.ref_x
        self.co_y = 14.5
        self.co_width = self.ref_width
        self.co_height = 1.5
        self.co_series = 'C'
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN

    @property
    def blank_key(self):
        return 'us_oh-PAU'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.drawing.rect((0, 0), (self.width, self.height), fill=self.fg))
        bg_g.add(self.drawing.rect(
            (self.stroke_width, self.stroke_width),
            (self.width - self.stroke_width * 2, self.height - self.stroke_width * 2),
            self.corner_radius, fill=self.bg))

        bg_g.add(render_text(
            self.drawing, 'PAULDING COUNTY', self.ref_x, self.co_y, self.ref_width,
            self.co_height, self.co_series, color=self.fg))

        return bg_g

class OHPleasantTownshipShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(OHPleasantTownshipShield, self).__init__(modifier, ref)

        self.width = 24.0
        self.height = 36.0

        self.border = 0.375
        self.stroke_width = 0.625
        self.corner_radius = 1.5

        self.ref_x = 2.0
        self.ref_width = self.width - self.ref_x * 2
        self.ref_height = 13.0

        self.pleasant_height = 4.0
        self.twp_height = 4.0

        self.pleasant_series = 'B'
        self.ref_series = 'B'
        self.twp_series = 'B'
        
        self.pleasant_y = 3.5
        self.twp_y = 9.0
        self.ref_y = 18.0
        
        self.fg = COLOR_WHITE
        self.bg = COLOR_GREEN

    @property
    def blank_key(self):
        return 'us_oh-LOG-PLEASANT'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_ul = (self.border + self.stroke_width/2, self.border + self.stroke_width/2)
        bg_dims = (self.width - bg_ul[0] * 2, self.height - bg_ul[1] * 2)
        bg_radius = self.corner_radius - self.border - self.stroke_width/2
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.stroke_width))

        bg_g.add(render_text(
            self.drawing, 'PLEASANT', self.ref_x, self.pleasant_y, self.ref_width,
            self.pleasant_height, self.pleasant_series, color=self.fg))
        bg_g.add(render_text(
            self.drawing, 'TOWNSHIP', self.ref_x, self.twp_y, self.ref_width,
            self.twp_height, self.twp_series, color=self.fg))

        return bg_g

OHIO_OUTLINE_2 = 'M 23.608576,9.8709668 23.609952,9.8724813 Z M 0.38756362,1.4190661 Z M 0.38966183,1.4227137 V 18.988314 C 0.37067545,19.130309 0.46560597,19.262806 0.59850927,19.291202 0.75989287,19.300659 0.921275,19.291202 1.0826571,19.262806 1.3579631,19.24388 1.6332553,19.385833 1.7661669,19.631916 3.2850636,20.493148 4.9178786,21.117797 6.607646,21.505832 7.5094956,21.732975 8.3448826,22.187259 9.0283924,22.811892 9.4745689,23.180998 9.996691,23.427067 10.566268,23.540639 11.126363,23.654209 11.70545,23.63528 12.25605,23.474388 12.664253,23.313498 13.091436,23.247247 13.528103,23.285104 14.040783,23.294568 14.54383,23.086355 14.904564,22.717249 15.066006,22.575281 15.293788,22.537428 15.483721,22.632073 15.948918,22.527958 16.357135,22.262964 16.6324,21.884399 16.926659,21.505832 17.287393,21.193501 17.705106,20.966373 18.122821,20.739231 18.578521,20.606719 19.053078,20.568867 19.689214,20.313329 20.27773,19.944234 20.799908,19.489949 21.31245,19.035664 21.749158,18.496205 22.090899,17.899954 22.423142,17.303702 22.660559,16.650671 22.783878,15.978714 22.907333,15.297288 22.907333,14.606405 22.802871,13.934434 22.736394,13.413901 22.907333,12.902838 23.258572,12.524273 23.476857,11.946947 23.457863,11.312843 23.201592,10.744987 23.144612,10.640886 23.135115,10.517901 23.173102,10.404342 23.220585,10.290648 23.315552,10.205583 23.429373,10.16773 23.552829,10.12038 23.628803,9.9973255 23.609809,9.8742703 V 0.67502467 C 23.619306,0.59931972 23.590816,0.51411733 23.533837,0.45740748 23.476857,0.40056002 23.401021,0.37220509 23.315552,0.39106247 21.549867,0.99683923 19.708069,1.3943588 17.847419,1.5836211 17.173433,1.6688235 16.527937,1.8959381 15.948918,2.2366102 15.635529,2.37866 15.303285,2.4353699 14.971039,2.407015 14.638795,2.3691626 14.316047,2.2461077 14.040783,2.0473479 13.803367,1.876943 13.518606,1.7823808 13.224348,1.7633858 12.930063,1.7445283 12.626281,1.8107357 12.369968,1.9621455 12.218077,2.0378504 12.047192,2.0568455 11.876321,2.0284905 11.714934,1.990638 11.563044,1.9054357 11.449138,1.7728832 10.67069,1.6404686 9.873276,1.611976 9.0853446,1.6783209 8.4777941,1.6876809 7.8892233,1.4889211 7.4145568,1.1198941 H 0.61749841 C 0.46560597,1.1387516 0.37067545,1.2713039 0.38966183,1.4227137 Z M 0.61598651,1.1161089 Z'
OHIO_OUTLINE_3 = 'M 29.610193,9.8744353 Z M 29.610193,0.68485829 Z M 29.067127,10.412434 C 29.014061,10.5175 29.024727,10.6435 29.098994,10.74858 29.41726,11.315778 29.43846,11.946003 29.17326,12.523695 28.727662,12.901828 28.515529,13.416506 28.589795,13.931185 28.727662,14.61393 28.717128,15.296662 28.568595,15.979407 28.409396,16.651632 28.122996,17.302857 27.698731,17.901575 27.274332,18.50028 26.722733,19.035959 26.075535,19.487625 25.417803,19.939277 24.685938,20.306903 23.879674,20.569502 23.285542,20.601008 22.702077,20.737555 22.182345,20.968634 21.651946,21.189207 21.19568,21.504326 20.835015,21.882459 20.485015,22.260578 19.96515,22.523177 19.381685,22.628217 19.137685,22.544177 18.861953,22.575697 18.660353,22.722743 18.204221,23.090372 17.578223,23.28994 16.920491,23.279438 16.379559,23.247926 15.827827,23.310948 15.329295,23.468503 14.629163,23.626057 13.897165,23.657569 13.197007,23.542028 12.486249,23.426488 11.82853,23.174401 11.266279,22.817276 10.407001,22.187058 9.3567766,21.735406 8.2216861,21.504326 6.0893981,21.115687 4.0313633,20.485462 2.1324747,19.634678 1.9521285,19.393092 1.6126626,19.246039 1.2625808,19.256545 1.0610213,19.288052 0.85946185,19.298558 0.65790369,19.298558 0.48816945,19.267038 0.36086843,19.130492 0.39269368,18.993946 V 1.4214564 C 0.36086843,1.2743901 0.48816945,1.1378572 0.67911964,1.1167905 H 9.2400849 C 9.8341501,1.4949229 10.576748,1.6945224 11.340546,1.6839891 12.327131,1.6105226 13.324315,1.6419892 14.310899,1.7785222 14.448765,1.9045219 14.639698,1.999055 14.851964,2.0306549 15.064097,2.0621215 15.27623,2.0411882 15.467163,1.9675884 15.796095,1.8205221 16.167294,1.7470556 16.538627,1.7679889 16.909959,1.7890555 17.270625,1.8835886 17.578224,2.0515882 17.91769,2.2511877 18.320889,2.3771874 18.734621,2.4087873 19.158887,2.4402539 19.583286,2.3877207 19.965151,2.2406544 20.69715,1.8941219 21.513948,1.6734558 22.362612,1.589456 24.70714,1.4003898 27.009134,1.0013242 29.247528,0.39199235 29.342995,0.38159238 29.448994,0.41305897 29.512728,0.46559217 29.586994,0.52865868 29.618727,0.60212516 29.608194,0.68612495 V 9.876902 C 29.629394,10.002902 29.533927,10.128901 29.385395,10.170901 29.236862,10.212901 29.120195,10.297034 29.067129,10.412501 Z'
