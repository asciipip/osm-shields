#!/usr/bin/python

# General outline, but no specific dimensions:
#   http://www.in.gov/dot/div/contracts/standards/drawings/sep17/e/800e/e800%20combined%20pdfs/E802-SNGS.pdf#page=2

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:IN'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:IN'] = make_in_shield

def make_in_shield(modifier, ref):
    if ref == 'INDIANA TOLL ROAD':
        return INTollShield().make_shield()
    return INStateShield(modifier, ref).make_shield()

class INStateShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(INStateShield, self).__init__(modifier, ref)

        if len(ref) <= 2:
            self.width = 24.0
            self.ref_series = 'D'
        else:
            self.width = 30.0
            if '1' in self.ref:
                self.ref_series = 'D'
            else:
                self.ref_series = 'C'
        self.height = 24.0
        self.ref_height = 12.0

        # All following dimensions are assumed/estimated.
        self.border = 0.375
        self.stroke_width = 0.625
        self.corner_radius = 1.5

        self.ref_x = self.border * 2 + self.stroke_width
        self.ref_width = self.width - self.ref_x * 2
        self.ref_height = 12.0
        self.in_height = 2.5
        self.in_series = 'D'
        self.in_x = self.ref_x
        self.in_width = self.ref_width

        available_vertical_space = self.height - self.border * 2 - self.stroke_width * 2 - \
                                   self.ref_height - self.in_height
        item_spacing = available_vertical_space / 3.0
        self.in_y = self.border + self.stroke_width + item_spacing
        self.ref_y = self.in_y + self.in_height + item_spacing
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_in-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_ul = (self.border + self.stroke_width/2, self.border + self.stroke_width/2)
        bg_dims = (self.width - bg_ul[0] * 2, self.height - bg_ul[1] * 2)
        bg_radius = self.corner_radius - bg_ul[0]
        bg_g.add(self.drawing.rect(
            bg_ul, bg_dims, bg_radius, stroke=self.fg, fill=self.bg,
            stroke_width=self.stroke_width))

        bg_g.add(render_text(
            self.drawing, 'INDIANA', self.in_x, self.in_y, self.in_width,
            self.in_height, self.in_series, color=self.fg))
        
        return bg_g

class INTollShield(ShieldBase):
    def __init__(self):
        super(INTollShield, self).__init__('', '')

        self.width = 24.0
        self.height = 24.0

        self.border_width = 0.5

        self.circle_radius = 11.0
        circle_offset_angle = 30.0 / 180.0 * math.pi
        circle_offset = (self.width - self.border_width*2 - 2*self.circle_radius)/2
        circle_offset_x = circle_offset * math.cos(circle_offset_angle)
        circle_offset_y = circle_offset * math.sin(circle_offset_angle)
        self.circle_x = self.width/2 + circle_offset_x
        self.circle_y = self.height/2 - circle_offset_y

        self.upper_dash_corners = [
            (16.5, 4.125), (18.25, 4.125),
            (16.75, 8.625), (12.375, 8.625),
        ]
        self.lower_dash_corners = [
            (3.375, 18.125),
            (11.25, 9.75),
            (16.25, 9.75),
            (12.125, 21.5)
        ]
        corner_y = self.lower_dash_corners[-1][1]
        corner_r = self.width/2 - self.border_width
        corner_x = -math.sqrt(corner_r**2 - (corner_y - self.width/2)**2) + self.width/2
        self.lower_dash_corners.append((corner_x, corner_y))
        
        self.border_color = COLOR_BLACK
        self.bg = COLOR_WHITE
        self.circle_color = COLOR_ORANGE

    @property
    def blank_key(self):
        return 'us_in-toll'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        bg_g.add(self.drawing.circle(
            (self.width/2, self.height/2), self.width/2 - self.border_width/2,
            stroke=self.border_color, fill=self.bg, stroke_width=self.border_width))
        bg_g.add(self.drawing.circle(
            (self.circle_x, self.circle_y), self.circle_radius,
            fill=COLOR_ORANGE))

        upper_dash = self.drawing.path(fill=self.bg)
        bg_g.add(upper_dash)
        upper_dash.push('M', self.upper_dash_corners[0])
        for c in self.upper_dash_corners[1:]:
            upper_dash.push('L', c)
        upper_dash.push('Z')
        
        lower_dash = self.drawing.path(fill=self.bg)
        bg_g.add(lower_dash)
        lower_dash.push('M', self.lower_dash_corners[0])
        for c in self.lower_dash_corners[1:]:
            lower_dash.push('L', c)
        lower_dash.push('Z')
        
        return bg_g

    def make_ref(self):
        return None
