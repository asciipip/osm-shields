#!/usr/bin/python
"""Shields for the belt system around Pittsburgh."""

import math
import svgwrite
import syslog

from ..banner import banner
from ..utils import *

def register(network_registry, style):
    network_registry['US:PA:BELT'] = make_shield

class BeltParams:
    def __init__(self):
        self.width = 24.0
        self.height = 24.0
        self.stroke_width = 0.5
        self.circle_diameter = 11.0
        self.circle_stroke_width = 0.35
        self.text_height = 3.5
        self.text_series = 'D'
        self.text_spacing = 1.0
        self.corner_radius = 1.5
        
PARAMS = BeltParams()

def make_shield(modifier, ref):
    color_found = False
    for color in ['RED', 'ORANGE', 'YELLOW', 'GREEN', 'BLUE']:
        if ref.upper().startswith(color):
            color_found = True
            ref = color

    if not color_found:
        syslog.syslog('unknown ref: US:PA:Belt {} ({})'.format(ref, modifier))
        return None
    
    if modifier == '':
        return make_base_shield(ref)
    else:
        shield_svg = make_base_shield(ref)
        banner_svgs = [banner(mod, int(shield_svg['width'])) for mod in modifier.split(';')]
        return compose_shields(banner_svgs + [shield_svg])

def make_base_shield(ref):
    drawing = svgwrite.Drawing(size=(PARAMS.width, PARAMS.height))

    shield_bg = make_blank_shield(drawing)
    drawing.add(shield_bg)

    ref_g = make_ref(drawing, ref)
    drawing.add(ref_g)
    
    return drawing

def make_blank_shield(drawing):
    background_g = drawing.g()

    outline = drawing.rect(
        (PARAMS.stroke_width * 1.5, PARAMS.stroke_width * 1.5),
        (PARAMS.width - PARAMS.stroke_width * 3, PARAMS.height - PARAMS.stroke_width * 3),
        PARAMS.corner_radius,
        stroke=COLOR_BLACK, fill=COLOR_WHITE, stroke_width=PARAMS.stroke_width)
    background_g.add(outline)

    belt_g = render_text(drawing, 'BELT',
                         0, PARAMS.height - (PARAMS.stroke_width * 2 + PARAMS.text_spacing + PARAMS.text_height),
                         PARAMS.width, PARAMS.text_height, PARAMS.text_series, color=COLOR_BLACK)
    background_g.add(belt_g)
    
    return background_g

def make_ref(drawing, ref):
    ref_g = drawing.g()

    color_g = render_text(drawing, ref.upper(),
                         0, PARAMS.stroke_width * 2 + PARAMS.text_spacing,
                         PARAMS.width, PARAMS.text_height, PARAMS.text_series, color=COLOR_BLACK)
    ref_g.add(color_g)

    color_circle = drawing.circle(
        (PARAMS.width/2, PARAMS.height/2),
        PARAMS.circle_diameter/2 - PARAMS.circle_stroke_width/2,
        stroke=COLOR_BLACK, fill=ref.lower(), stroke_width=PARAMS.circle_stroke_width)
    ref_g.add(color_circle)
    
    return ref_g
