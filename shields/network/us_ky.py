#!/usr/bin/python

# For the most part, Kentucky uses the standard state signs.  See:
# https://transportation.ky.gov/TrafficOperations/Documents/T012%20Gen%20Typ%20Std%20Signs%20Detail%20Sheet.pdf
#
# Unfortunately, there's *one* route in the US:KY network that gets its
# own shield (AA Highway), so we have to do our own thing here and deal
# with it ourselves.
#
# Also, the Kentucky parkway system has its own network (US:KY:Parkway)
# and individual shields for the parkways.

import math
import re
import svgwrite
import syslog

from ..shieldbase import ShieldBase
from ..utils import *
from ..us_state import USGenericStateShield, STYLE_ELONGATED

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:KY'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:KY'] = lambda m,r: make_ky_shield(m, r, style)
    network_registry['US:KY:PARKWAY'] = lambda m,r: KYParkwayShield(m, r).make_shield()

def make_ky_shield(modifier, ref, style):
    if ref == 'AA':
        return KYAAShield(modifier, ref).make_shield()
    else:
        return USGenericStateShield(modifier, ref, STYLE_ELONGATED, sign=style == STYLE_SIGN).make_shield()

class KYAAShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(KYAAShield, self).__init__(modifier, ref)

        self.width = 24.0
        self.height = 24.0

        self.corner_radius = 1.5

        self.ref_x = 1.0
        self.ref_y = 3.5
        self.ref_width = self.width - self.ref_x * 2
        self.ref_height = 12.0
        self.ref_series = series_for_text(self.ref, self.ref_height, self.ref_width, ['D', 'C', 'B'])

        self.hw_x = self.ref_x
        self.hw_y = 18.5
        self.hw_width = self.ref_width
        self.hw_height = 3.0
        self.hw_series = 'D'

        self.transition = 16.0
        
        self.fg = COLOR_BLUE
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ky-AA'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        upper_bg = self.drawing.path(fill=self.bg)
        bg_g.add(upper_bg)
        upper_bg.push('M', (0, self.transition))
        upper_bg.push('V', self.corner_radius)
        upper_bg.push_arc((self.corner_radius, 0), 0, self.corner_radius,
                          large_arc=False, angle_dir='-', absolute=True)
        upper_bg.push('H', self.width - self.corner_radius)
        upper_bg.push_arc((self.width, self.corner_radius), 0, self.corner_radius,
                          large_arc=False, angle_dir='-', absolute=True)
        upper_bg.push('V', self.transition)
        upper_bg.push('Z')

        lower_bg = self.drawing.path(fill=self.fg)
        bg_g.add(lower_bg)
        lower_bg.push('M', (0, self.transition))
        lower_bg.push('V', self.height - self.corner_radius)
        lower_bg.push_arc((self.corner_radius, self.height), 0, self.corner_radius,
                          large_arc=False, angle_dir='-', absolute=True)
        lower_bg.push('H', self.width - self.corner_radius)
        lower_bg.push_arc((self.width, self.height - self.corner_radius), 0, self.corner_radius,
                          large_arc=False, angle_dir='-', absolute=True)
        lower_bg.push('V', self.transition)
        lower_bg.push('Z')
        
        bg_g.add(render_text(
            self.drawing, 'HIGHWAY', self.hw_x, self.hw_y, self.hw_width,
            self.hw_height, self.hw_series, color=self.bg))

        return bg_g

def parse_parkway_ref(ref):
    if ref == 'AUDUBON PARKWAY':
        return (None, ['AUDUBON', 'PARKWAY'])
    elif re.search('^(BERT T\\.? COMBS )?MOUNTAIN PARKWAY$', ref) is not None:
        return ('BERT T. COMBS', ['MOUNTAIN', 'PARKWAY'])
    elif re.search('^(EDWARD T\\.? BREATHITT )?PENNYRILE PARKWAY$', ref) is not None:
        return ('EDWARD T. BREATHITT', ['PENNYRILE', 'PARKWAY'])
    elif ref == 'HAL ROGERS PARKWAY':
        return (None, ['HAL ROGERS', 'PARKWAY'])
    elif re.search('^(JULIAN M\\.? CARROLL )?PURCHASE PARKWAY$', ref) is not None:
        return ('JULIAN M. CARROLL', ['PURCHASE', 'PARKWAY'])
    elif re.search('^(LOUIS B\\.? NUNN )?CUMBERLAND PARKWAY$', ref) is not None:
        return ('LOUIS B. NUNN', ['CUMBERLAND', 'PARKWAY'])
    elif re.search('^(MARTHA LAYNE COLLINS )?BLUE ?GRASS PARKWAY$', ref) is not None:
        return ('MARTHA LAYNE COLLINS', ['BLUEGRASS', 'PARKWAY'])
    elif re.search('^(WENDELL H\\.? FORD )?WESTERN K(ENTUCK)?Y PARKWAY$', ref) is not None:
        return ('WENDELL H. FORD', ['WESTERN KY', 'PARKWAY'])
    elif re.search('^(WILLIAM H\\.? )?NATCHER (GREEN RIVER )?PARKWAY$', ref) is not None:
        return (None, ['WILLIAM H.', 'NATCHER', 'PARKWAY'])
    return (None, None)
    
    
class KYParkwayShield(ShieldBase):
    def __init__(self, modifier, ref):
        super(KYParkwayShield, self).__init__(modifier, ref)

        self.header, self.ref_lines = parse_parkway_ref(ref)
        if self.ref_lines is None:
            syslog.syslog('Unknown ref: (US:KY:Parkway;{};{})'.format(ref, modifier))
            self.ref = None
            return
        
        self.width = 30.0
        self.height = 24.0
        self.corner_radius = 1.5
        self.upper_transition = 5.0
        self.lower_transition = 17.0

        if self.header is not None:
            self.header_height = 3.0
            self.header_x = self.corner_radius/2
            self.header_y = self.upper_transition/2 - self.header_height/2
            self.header_width = self.width - self.header_x * 2
            self.header_series = series_for_text(
                self.header, self.header_height, self.header_width, ['D', 'C', 'B'])
            top_y = self.upper_transition
        else:
            top_y = 0

        self.ref_height = 4.0
        self.ref_x = 0.0
        self.ref_width = self.width - self.ref_x * 2
        self.ref_spacing = 0.8
        self.ref_series = min(
            [series_for_text(line, self.ref_height, self.ref_width, ['D', 'C', 'B'],
                             spacing=self.ref_spacing)
             for line in self.ref_lines])
        total_ref_height = self.lower_transition - top_y
        total_spacing = total_ref_height - len(self.ref_lines) * self.ref_height
        individual_space = total_spacing / (len(self.ref_lines) + 1)
        self.ref_ys = [top_y + individual_space + (self.ref_height + individual_space) * i
                       for i in range(0, len(self.ref_lines))]

        self.fg = COLOR_BLUE
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ky-parkway'
    
    def make_blank_shield(self):
        bg_g = self.drawing.g()

        upper_bg = self.drawing.path(fill=self.bg)
        bg_g.add(upper_bg)
        upper_bg.push('M', (0, self.lower_transition))
        upper_bg.push('V', self.corner_radius)
        upper_bg.push_arc((self.corner_radius, 0), 0, self.corner_radius,
                          large_arc=False, angle_dir='-', absolute=True)
        upper_bg.push('H', self.width - self.corner_radius)
        upper_bg.push_arc((self.width, self.corner_radius), 0, self.corner_radius,
                          large_arc=False, angle_dir='-', absolute=True)
        upper_bg.push('V', self.lower_transition)
        upper_bg.push('Z')

        lower_bg = self.drawing.path(fill=self.fg)
        bg_g.add(lower_bg)
        lower_bg.push('M', (0, self.lower_transition))
        lower_bg.push('V', self.height - self.corner_radius)
        lower_bg.push_arc((self.corner_radius, self.height), 0, self.corner_radius,
                          large_arc=False, angle_dir='-', absolute=True)
        lower_bg.push('H', self.width - self.corner_radius)
        lower_bg.push_arc((self.width, self.height - self.corner_radius), 0, self.corner_radius,
                          large_arc=False, angle_dir='-', absolute=True)
        lower_bg.push('V', self.lower_transition)
        lower_bg.push('Z')
        
        return bg_g

    def make_ref(self):
        ref_g = self.drawing.g()

        if self.header is not None:
            header_bg = self.drawing.path(fill=self.fg)
            ref_g.add(header_bg)
            header_bg.push('M', (0, self.upper_transition))
            header_bg.push('V', self.corner_radius)
            header_bg.push_arc((self.corner_radius, 0), 0, self.corner_radius,
                              large_arc=False, angle_dir='+', absolute=True)
            header_bg.push('H', self.width - self.corner_radius)
            header_bg.push_arc((self.width, self.corner_radius), 0, self.corner_radius,
                              large_arc=False, angle_dir='+', absolute=True)
            header_bg.push('V', self.upper_transition)
            header_bg.push('Z')

            ref_g.add(render_text(
                self.drawing, self.header, self.header_x, self.header_y,
                self.header_width, self.header_height, self.header_series,
                color=self.bg))

        for y, line in zip(self.ref_ys, self.ref_lines):
            ref_g.add(render_text(
                self.drawing, line, self.ref_x, y, self.ref_width, self.ref_height,
                self.ref_series, color=self.fg, spacing=self.ref_spacing))
            
        return ref_g
    
