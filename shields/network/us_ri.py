#!/usr/bin/python

# http://www.dot.ri.gov/documents/doingbusiness/RIDOT_Std_Details.pdf#page=215

import math
import svgwrite

from ..shieldbase import ShieldBase
from ..us_state import USGenericStateShield
from ..utils import *

def register(network_registry, style):
    if style == STYLE_GENERIC:
        network_registry['US:RI'] = lambda m,r: USGenericStateShield(m, r).make_shield()
    else:
        network_registry['US:RI'] = lambda m,r: RIStateShield(m, r, style).make_shield()

class RIStateShield(ShieldBase):
    def __init__(self, modifier, ref, style):
        super(RIStateShield, self).__init__(modifier, ref)
        self.guide = style == STYLE_GUIDE
        
        if len(ref) <= 2:
            self.width = 24.0
        else:
            self.width = 30.0
        self.height = 24.0
        self.corner_radius = 1.5
        if self.guide:
            self.margin = 0.0
            self.border_width = 0.5
        else:
            self.margin = 0.375
            self.border_width = 0.625

        self.ri_height = 4.0
        self.ri_x = (self.margin + self.border_width) * 2
        self.ri_width = self.width - self.ri_x * 2
        self.ri_series = 'D'
        
        self.ref_height = 12.0
        self.ref_x = self.ri_x
        self.ref_width = self.width - self.ref_x * 2

        for series in ['E', 'D', 'C', 'B']:
            self.ref_series = series
            if text_length(self.ref, self.ref_height, series) < self.ref_width:
                break

        available_space = self.height - 2 * (self.margin + self.border_width) - \
                          self.ri_height - self.ref_height
        spacing = available_space / 3
        self.ri_y = self.margin + self.border_width + spacing
        self.ref_y = self.ri_y + self.ri_height + spacing
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        return 'us_ri-{}'.format(int(self.width))
    
    def make_blank_shield(self):
        background_g = self.drawing.g()

        border_offset = self.margin + self.border_width/2
        background_g.add(
            self.drawing.rect(
                (border_offset, border_offset),
                (self.width - border_offset * 2, self.height - border_offset * 2),
                self.corner_radius - border_offset,
                stroke=self.fg, fill=self.bg, stroke_width=self.border_width))
    
        background_g.add(
            render_text(
                self.drawing, 'R.I.', self.ri_x, self.ri_y,
                self.ri_width, self.ri_height, self.ri_series,
                color=self.fg))
        
        return background_g
