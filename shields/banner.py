#!/usr/bin/python

import svgwrite

from .utils import *

class BannerParams:
    def __init__(self, A, B, C, D, E, F, FFont, G, H, J, spacing):
        self.width = float(A)
        self.height = float(B)
        self.white_stroke = float(C)
        self.black_stroke = float(D)
        self.name_y = float(E)
        self.name_height = float(F)
        self.name_series = FFont
        self.name_x_left = float(G)
        self.name_x_right = float(H)
        self.corner_radius = float(J)
        self.spacing_adjustment = float(spacing)

class BypassParams:
    def __init__(self, A, B, C, D, E, F, FFont, G, H, J, K, L, M, spacing):
        self.width = float(A)
        self.height = float(B)
        self.white_stroke = float(C)
        self.black_stroke = float(D)
        self.name_y = float(E)
        self.name_height = float(F)
        self.name_series = FFont
        self.name_x_left = float(G)
        self.name_x_right = float(H)
        self.hyphen_length = float(J)
        self.hyphen_stroke = float(K)
        self.hyphen_x = float(L)
        self.corner_radius = float(M)

        self.spacing_adjustment = float(spacing)
        
class UnknownParams:
    def __init__(self, width):
        self.width = float(width)
        self.height = self.width / 2
        self.white_stroke = 0.375
        self.black_stroke = 0.625
        self.name_height = round(self.width / 4.0)
        self.name_series = 'D'
        self.corner_radius = 1.5
        self.spacing_adjustment = 1.0

        self.name_x = 2.0
        self.name_width = self.width - self.name_x*2
        self.name_x_left = self.name_width / 2
        self.name_x_right = self.name_width / 2

    @property
    def name_y(self):
        return self.height/2 - self.name_height/2

    def text_length(self, text):
        return text_length(text, self.name_height, self.name_series, self.spacing_adjustment)
    
PARAMS = {
    'ALTERNATE': {
        24: BannerParams(24, 12, .375, .625, 4,   4, 'B',  9.688,  9.563, 1.5, 1.0),
        30: BannerParams(30, 15, .375, .625, 4.5, 6, 'B', 12.938, 12.813, 1.5, 0.5),
    },
    'ALT': {
        24: BannerParams(24, 12, .375, .625, 3,   6, 'D', 6.625, 7.25, 1.5, 1.0),
        30: BannerParams(30, 15, .375, .625, 3.5, 8, 'D', 9,     9.5,  1.5, 1.0),
    },
    'BUSINESS': {
        24: BannerParams(24, 12, .375, .625, 3.5, 5, 'B',  9.313,  9.563, 1.5, 0.6),
        30: BannerParams(30, 15, .375, .625, 4,   7, 'B', 12.375, 12.75,  1.5, 0.6),
    },
    'BYPASS': {
        24: BypassParams(24, 12, .375, .625, 3.5, 5, 'B',  9.375, 9.625,  .75, .625, 3.375, 1.5, 1.0),
        30: BypassParams(30, 15, .375, .625, 4,   7, 'B', 12.25, 12.625, 1,    .875, 3.875, 1.5, 1.0),
    },
    'CITY': {
        24: BannerParams(24, 12, .375, .625, 3,   6, 'D', 10, 10, 1.5, 1.0),
        30: BannerParams(30, 15, .375, .625, 3.5, 8, 'D', 13, 13, 1.5, 1.0),
    },
    'FUTURE': {
        24: BannerParams(24, 12, .375, .625, 3.5, 5, 'C', 10, 10, 1.5, 1.0),
        30: BannerParams(30, 15, .375, .625, 4,   7, 'C', 13, 13, 1.5, 0.6),
    },
    'SCENIC': {
        24: BannerParams(24, 12, .375, .625, 3.4, 5, 'C', 10, 10, 1.5, 1.0),
        30: BannerParams(30, 15, .375, .625, 4,   7, 'C', 13, 13, 1.5, 1.0),
    },
    'SPUR': {
        24: BannerParams(24, 12, .375, .625, 3,   6, 'C',  9.875,  9.875, 1.5, 1.0),
        30: BannerParams(30, 15, .375, .625, 3.5, 8, 'C', 12.25,  12.25,  1.5, 1.0),
    },
    'TOLL': {
        24: BannerParams(24, 12, .375, .625, 3,   6, 'D', 10, 10, 1.5, 1.0),
        30: BannerParams(30, 15, .375, .625, 3.5, 8, 'D', 13, 13, 1.5, 1.0),
    },
    'TRUCK': {
        24: BannerParams(24, 12, .375, .625, 3,   6, 'C',  9.875,  9.875, 1.5, 0.8),
        30: BannerParams(30, 15, .375, .625, 3.5, 8, 'C', 12.25,  12.25,  1.5, 0.5),
    },
}

def adjust_unknown_size(text, width):
    params = UnknownParams(width)
    for series in ['D', 'C', 'B']:
        params.name_series = series
        if params.text_length(text) <= params.name_width:
            return params
    for spacing in [0.9, 0.8, 0.7, 0.6, 0.5, 0.4, 0.3]:
        params.spacing_adjustment = spacing
        if params.text_length(text) <= params.name_width:
            return params
    return params

def banner(name, width, bg=None, fg=None, border=None):
    if width <= 25:
        width = 24
    else:
        width = 30
    name = name.upper()
    if bg is None:
        if name == 'TOLL':
            bg = COLOR_YELLOW
        else:
            bg = COLOR_WHITE
    if fg is None:
        fg = COLOR_BLACK
    if border is None:
        border = fg

    if name in PARAMS:
        params = PARAMS[name][width]
    else:
        params = adjust_unknown_size(name, width)
    
    drawing = svgwrite.Drawing(size=(params.width, params.height), profile='full')

    g = drawing.g()
    drawing.add(g)
    
    if bg == COLOR_WHITE:
        outline_stroke = params.black_stroke
        outline_corner = (params.white_stroke + params.black_stroke/2, params.white_stroke + params.black_stroke/2)
    else:
        outline_stroke = 0.5
        outline_corner = (params.white_stroke/2, params.white_stroke/2)
    outline_dims = (params.width - 2*outline_corner[0], params.height - 2*outline_corner[1])
    outline_radius = params.corner_radius - outline_corner[0]

    outline = drawing.rect(outline_corner, outline_dims, outline_radius,
                           stroke=border, fill=bg, stroke_width=outline_stroke)
    g.add(outline)

    if name.upper() == 'BYPASS':
        name_text = 'BY-PASS'
    else:
        name_text = name.upper()
    name_g = render_text(drawing, name_text,
                           params.width/2 - params.name_x_left, params.name_y,
                           params.name_x_left + params.name_x_right,
                           params.name_height, params.name_series, color=fg,
                           spacing=params.spacing_adjustment)
    g.add(name_g)

    return drawing
