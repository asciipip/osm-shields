#!/usr/bin/python

__all__ = ['list_styles', 'list_networks', 'render', 'render_and_save', 'cluster_and_save']

import importlib
import os
import pkgutil
import re
import syslog

from .cluster import cluster_shields
from .utils import STYLES, split_num_char

syslog.openlog('shields')

REGISTRIES = {}

# These are networks which we'll never support (usually because they
# aren't signed on the ground), so it's pointless to log messages about
# them.
IGNORED_NETWORKS = [
    'NY:US',  # Wrong element order for 'US:NY'
    'US:ADHS',  # Not signed, as far as I can tell.  (Some other routes might be signed differently when part of an ADHS corridor, but I'm just ignoring those for now.)
    'US:AUTO_TRAIL',  # Historical; no longer signed
    'US:GA:GRIP',  # Not signed
    'US:MA:OLD_TURNPIKE',  # Not signed
    'US:MD:FORMER',  # Not signed
    'US:MD:LEGISLATIVE', # No longer signed
    'US:NHS HIGH PRIORITY CORRIDORS',  # Not signed
]

def list_styles():
    """List all available shield styles."""
    return STYLES

def list_networks(style):
    """List available network designators for the given shield style."""
    if style not in REGISTRIES:
        REGISTRIES[style] = initialize_registry(style)
    return REGISTRIES[style].keys()
    
def initialize_registry(style):
    registry = {}
    for importer, modname, ispkg in pkgutil.iter_modules(['/'.join([p, 'network']) for p in __path__]):
        importlib.import_module('.network.{}'.format(modname), __name__).register(registry, style)
    return registry

def render(network, modifier, ref, style):
    """Creates and returns a shield for the given route.

    :param network: Route network in OpenStreetMap format, e.g. "US:US"
        for US Highways.  See :func:`list_networks` to get a list of valid
        network designators.
    :param modifier: Route modifier, e.g. "TOLL" for a tolled route.  Use
        `None` if the route has no modifier.
    :param ref: The route number or name, as a string.
    :param style: The style of shield to create.  See :func:`list_styles`
        to get a list of valid styles.
    :type network: str
    :type modifier: str or None
    :type ref: str
    :type style: str

    :return: An :class:`svgwrite.drawing.Drawing` object containing an
        SVG rendering of the requested shield.
    :rtype: svgwrite.drawing.Drawing"""
    if style not in REGISTRIES:
        REGISTRIES[style] = initialize_registry(style)
    
    network = network.upper()
    if modifier is None:
        modifier = ''
    else:
        modifier = modifier.upper()

    try:
        return REGISTRIES[style][network](modifier, ref)
    except Exception as e:
        syslog.syslog('exception: ({};{};{};{}): {}'.format(network, ref, modifier, style, e.message))
        raise
        

def render_and_save(network, modifier, ref, base_directory, style='guide', force_render=False):
    """Creates and saves a shield for the given route.

    :param network: Route network in OpenStreetMap format, e.g. "US:US"
        for US Highways.  See :func:`list_networks` to get a list of valid
        network designators.
    :param modifier: Route modifier, e.g. "TOLL" for a tolled route.  Use
        `None` if the route has no modifier.
    :param ref: The route number or name, as a string.
    :param base_directory: The directory to use as the base for saving the
        file.  See below for information about the subdirectories this
        function will create or use.
    :param style: The style of shield to create.  See :func:`list_styles`
        to get a list of valid styles.  Default: "guide".
    :param force_render: If `False` and a file already exists for this
        shield, no rendering will be done.  If `True`, a new shield will
        always be generated and saved.  Default: `False`.
    :type network: str
    :type modifier: str or None
    :type ref: str
    :type base_directory: str
    :type style: str
    :type force_render: bool

    :return: The full path to the saved SVG.
    :rtype: str

    The function will create subdirectories under the base directory
    according to the network, modifier, and style.  Callers would be best
    served by treating the subdirectories as opaque and just using
    `render_and_save` every time they want the path to a shield image
    file."""
    if style not in REGISTRIES:
        REGISTRIES[style] = initialize_registry(style)
    registry = REGISTRIES[style]
    
    network, modifier, ref = process_ref(network, modifier, ref, registry)
    if network is None:
        return None
    
    filename = get_filename(base_directory, style, network, modifier, ref)
    if not force_render and os.path.exists(filename):
        return filename
    
    drawing = render(network, modifier, ref, style)
    if drawing is None:
        return None
    
    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))
    drawing.saveas(filename)
    return filename

def cluster_and_save(fullrefs, orientation, base_directory, style='guide', force_render=False):
    """Creates and saves an SVG of a shield cluster for the given set of routes.

    :param fullrefs: A sequence of "fullref" objects, where a fullref
        object is a dictionary with "network" "modifier" and "ref" keys.

    :param network: Route network in OpenStreetMap format, e.g. "US:US"
        for US Highways.  See :func:`list_networks` to get a list of valid
        network designators.
    :param modifier: A string giving the compass orientation of the
        underlying route.  May be a cardinal direction ("N", "S", "E",
        "W"), an ordinal direction ("NW", "SE", "NE", "SW"), or a half
        wind ("NNE", "ENE", "ESE", etc.)
    :param base_directory: The directory to use as the base for saving the
        file.  See below for information about the subdirectories this
        function will create or use.
    :param style: The style of shield to create.  See :func:`list_styles`
        to get a list of valid styles.  Default: "guide".
    :param force_render: If `False` and a file already exists for this
        shield, no rendering will be done.  If `True`, a new shield will
        always be generated and saved.  Default: `False`.
    :type network: list
    :type orientation: str
    :type base_directory: str
    :type style: str
    :type force_render: bool

    :return: The full path to the saved SVG.
    :rtype: str

    The function will create subdirectories under the base directory
    according to the networks, modifiers, refs, orientation, and style.
    Callers would be best served by treating the subdirectories as opaque
    and just using `cluster_and_save` every time they want the path to a
    clustered shield image file."""
    if style not in REGISTRIES:
        REGISTRIES[style] = initialize_registry(style)
    registry = REGISTRIES[style]
    
    refs = sorted(parse_fullrefs(fullrefs, registry), key=fullref_sort_key)

    filename = get_cluster_filename(base_directory, style, refs, orientation)
    if not force_render and os.path.exists(filename):
        return filename

    shields = []
    for network, modifier, ref in refs:
        shield = render(network, modifier, ref, style)
        if shield is not None:
            shields.append(shield)

    if len(shields) == 0:
        return None
    
    cluster = cluster_shields(shields, orientation)
    if not os.path.exists(os.path.dirname(filename)):
        os.makedirs(os.path.dirname(filename))
    cluster.saveas(filename)
    
    return filename

def process_ref(network, modifier, ref, registry):
    """Does various preprocessing on the route data.  Returns None if nothing
    can be rendered from the route."""
    network = network.upper()
    modifier = modifier.upper() if modifier is not None else ''
    ref = ref.upper()

    if network in IGNORED_NETWORKS:
        return None, None, None
    
    if modifier == '' and network not in registry:
        for test_modifier in ['ALT', 'ALTERNATE', 'BUSINESS', 'BYPASS', 'CONNECTOR', 'FUTURE', 'LINK', 'SCENIC', 'SPUR', 'TOLL', 'TRUCK']:
            if network.endswith(':' + test_modifier):
                syslog.syslog('warning: inferred modifier {} for ({};{};{})'.format(test_modifier, network, ref, modifier))
                modifier = test_modifier
                break
    if modifier != '':
        import sys
        if network.endswith(':' + modifier):
            network = network[:-(len(modifier)+1)]
        match = re.search('^(?:{0}\s*)?(.*?)(?:\s*{0})?$'.format(re.escape(modifier)), ref)
        if match is not None:
            ref = match.group(1)
        
    if network not in registry:
        syslog.syslog('unknown network: ({};{};{})'.format(network, ref, modifier))
        return None, None, None

    return network, modifier, ref
    
def clean_dirname(dirname):
    return re.sub('[^A-Za-z0-9_-]', '', dirname)
    
def get_filename(base, style, network, modifier, ref, extension='.svg'):
    network_segments = network.split(':')
    if modifier == '' or modifier is None:
        pieces = [base, style] + network_segments + ['{}{}'.format(ref, extension)]
    else:
        pieces = [base, style] + network_segments + [modifier, '{}{}'.format(ref, extension)]
    return os.path.join(*pieces)

def get_cluster_filename(base_directory, style, refs, orientation):
    elements = [base_directory, style, 'cluster']
    for network, modifier, ref in refs:
        if modifier is None or modifier == '':
            elements.append(clean_dirname('{}-{}'.format(network.replace(':', '_'), ref)))
        else:
            elements.append(clean_dirname('{}-{}-{}'.format(network.replace(':', '_'), ref, modifier.replace(';', '_'))))
    elements.append('{}.svg'.format(clean_dirname(orientation)))
    return os.path.join(*elements)

def parse_fullrefs(fullrefs, registry):
    result = []
    for fr in fullrefs:
        network, modifier, ref = process_ref(fr['network'], fr['modifier'], fr['ref'], registry)
        if network is not None:
            result.append((network, modifier, ref))
    return result

def fullref_sort_key(fullref):
    network, modifier, ref = fullref
    if network == 'US:I':
        sort_base = 0.0
    elif network.startswith('US:I:'):
        sort_base = 0.25
    elif network == 'US:US':
        sort_base = 1.0
    elif network.startswith('US:US:'):
        sort_base = 1.2
    else:
        sort_base = float(len(network.split(':')))
    if modifier is not None and modifier != '':
        sort_base += 0.5

    return (sort_base, network.split(':'), modifier.split(';'), list(split_num_char(ref)))
