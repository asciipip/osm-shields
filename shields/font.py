#!/usr/bin/python

import xml.etree.ElementTree as ET

from pkg_resources import resource_stream

FHWA_HEIGHT = 4.0

class Glyph:
    def __init__(self, glyph_element, font, spacing):
        self.name = glyph_element.attrib['glyph-name']
        if 'd' in glyph_element.attrib:
            self.d = glyph_element.attrib['d']
        else:
            self.d = None
        if 'horiz-adv-x' in glyph_element.attrib:
            self.width = int(glyph_element.attrib['horiz-adv-x'])
        else:
            self.width = font.cap_height
        if 'unicode' in glyph_element.attrib:
            self.char = glyph_element.attrib['unicode']
        else:
            self.char = None

        if self.char is not None and self.char in spacing:
            l, c, r = spacing[self.char]
            self.left_space  = l
            self.right_space = r
            self.glyph_width = c
        else:
            self.left_space  = 0
            self.right_space = 0
            self.glyph_width = self.width

    def __repr__(self):
        return '<Glyph({})>'.format(self.char)
    
            
class Font:
    def __init__(self, font_file, spacing_file):
        ns = {'svg': 'http://www.w3.org/2000/svg'}
        tree = ET.parse(font_file)
        font = tree.getroot().find('svg:defs', ns).find('svg:font', ns)
        font_face = font.find('svg:font-face', ns)
        self.height = int(font_face.attrib['units-per-em'])
        self.ascent = int(font_face.attrib['ascent'])
        self.descent = int(font_face.attrib['descent'])
        self.x_height = int(font_face.attrib['x-height'])
        self.cap_height = int(font_face.attrib['cap-height'])

        spacing = {}
        for line in spacing_file:
            char, l, c, r = line.split()
            spacing[char] = (float(l) / FHWA_HEIGHT * self.cap_height,
                             float(c) / FHWA_HEIGHT * self.cap_height,
                             float(r) / FHWA_HEIGHT * self.cap_height)
        
        self.glyphs = {}
        for g in font.findall('svg:glyph', ns):
            glyph = Glyph(g, self, spacing)
            self.glyphs[glyph.char] = glyph

FONTS = {}
def get_font(series):
    if series in FONTS:
        return FONTS[series]
    font = Font(resource_stream(__name__, 'roadgeek/font-{}.svg'.format(series)),
                resource_stream(__name__, 'roadgeek/spacing-{}.dat'.format(series)))
    FONTS[series] = font
    return font
