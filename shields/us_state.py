#!/usr/bin/python
# -*- coding: utf-8 -*-
"""Standard US state shields.

These cover every state that hasn't made its own unique shield style.
Note that some states differ in how they apply the MUTCD's definitions to
wide-style shields.

"""

# MUTCD 2009 § 2D.11 ¶ 11: "State Route signs (see Figure 2D-3) should be
# rectangular and should be approximately the same size as the U.S. Route
# sign. State Route signs should also be similar to the U.S. Route sign by
# containing approximately the same size black numerals on a white area
# surrounded by a rectangular black background without a border. The shape
# of the white area should be circular in the absence of any determination
# to the contrary by the individual State concerned."
#
# That gives us a standard 24x24 shield with a circle, a border width of
# 0.5 and a ref height of 12.  The wide style shields have some variation,
# unfortunately.  Some states use a 30x24 ellipse, while others cut the
# 24x24 circle in half and then use horizontal lines of length 6 between
# the halves.  Still others just shrink the route number to fit in the
# 24x24 circle.

import math
import svgwrite

from .shieldbase import ShieldBase
from .utils import *

STYLE_CIRCLE = 'circle'
STYLE_ELLIPSE = 'ellipse'
STYLE_ELONGATED = 'elongated'

class USGenericStateShield(ShieldBase):
    def __init__(self, modifier, ref, wide_style=STYLE_ELLIPSE, sign=False):
        super(USGenericStateShield, self).__init__(modifier, ref)
        self.sign = sign
        self.wide_style = wide_style
        
        if len(self.ref) <= 2:
            self.width = 24.0
            self.ref_series = 'D'
        else:
            self.width = 30.0
            if len(self.ref) == 3:
                if '1' in self.ref:
                    self.ref_series = 'D'
                else:
                    self.ref_series = 'C'
            else:
                self.ref_series = 'B'
        self.height = 24.0
        self.ref_height = 12.0
        self.border_width = 0.5
        self.ref_y = (self.height - self.ref_height) / 2
        self.corner_radius = 1.5
        
        center = self.width/2 + self.height/2*1j
        text_ul_corner = center.real - \
                         math.sqrt((center.real - 2*self.border_width)**2 - \
                                   (center.imag - self.ref_y)**2) + \
                         self.ref_y * 1j
        available_text_width = self.width - 2 * text_ul_corner.real
        needed_text_width = text_length(self.ref, self.ref_height, self.ref_series)
    
        if available_text_width >= needed_text_width:
            self.ref_x = text_ul_corner.real
        else:
            rect_width, rect_height = rectangle_in_ellipse_with_ratio(
                self.width - 4*self.border_width,
                self.height - 4*self.border_width,
                needed_text_width / self.ref_height)
            real_ul_corner = center.real - rect_width/2 + (center.imag - rect_height/2) * 1j
            self.ref_x = real_ul_corner.real
            self.ref_y = real_ul_corner.imag
            self.ref_height = rect_height
        self.ref_width = self.width - self.ref_x * 2

        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

    @property
    def blank_key(self):
        if int(self.width) == 24:
            return 'us_state-24'
        else:
            return 'us_state-{}-{}'.format(self.wide_style, int(self.width))
        
    def make_blank_shield(self):
        if self.sign:
            border_offset = self.border_width
        else:
            border_offset = self.border_width / 2
        if self.width == self.height:
            outline = self.drawing.circle(
                center=(self.width/2, self.height/2),
                r=self.width/2 - border_offset)
        elif self.wide_style == STYLE_ELLIPSE:
            outline = self.drawing.ellipse(
                center=(self.width/2, self.height/2),
                r=(self.width/2-border_offset,
                   self.height/2-border_offset))
        elif self.wide_style == STYLE_ELONGATED:
            outline = self.drawing.path()
            outline.push('M', (self.height/2, border_offset))
            outline.push_arc((self.height/2, self.height - border_offset),
                             0, self.height/2 - border_offset,
                             large_arc=True, angle_dir='-', absolute=True)
            outline.push('H', self.width - self.height/2)
            outline.push_arc((self.width - self.height/2, border_offset),
                             0, self.height/2 - border_offset,
                             large_arc=True, angle_dir='-', absolute=True)
            outline.push('Z')
        else:
            assert False
        outline['fill'] = COLOR_WHITE

        if self.sign:
            bg_g = self.drawing.g()
            bg_g.add(self.drawing.rect(
                (0, 0), (self.width, self.height), self.corner_radius,
                fill=self.fg))
            bg_g.add(outline)
            return bg_g
        else:
            outline['stroke'] = COLOR_BLACK
            outline['stroke-width'] = self.border_width
            return outline
