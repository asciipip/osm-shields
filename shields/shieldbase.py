#!/usr/bin/python

import math
import svgwrite

from .banner import banner
from .utils import *

class ShieldBase(object):
    """Base class for drawing individual shields.

    Children of this class *must* provide some instance variables and
    override some methods.

    Children must override:
     * blank_key()
     * make_blank_shield()

    Children must provide the following instance variables:
     * ref_x
     * ref_y
     * ref_height
     * ref_width
     * ref_series
    OR they must override:
     * make_ref()
    OR they must set `ref` to None

    Children MAY (re)define the following instance variables:
     * width - The image's width (default 24)
     * height - The image's height (default 24)
     * wide_threshold - If the ref has this many characters or more,
       wide() will return True (and narrow() will return False)
       (default 3)
     * fg - The color for the image's text (default black)
     * bg - The color for the image's background (default white)
     * ref_spacing - The factor applied to spacing between the ref's
       characters (default 1.0)
     * ref_size_func - A function passed to `render_text` when rendering
       the ref (default no function)
     * border_color - The color of the border around the image (default
       is the value of `fg`)
     * ref - if this has the value None, make_ref() will not be called
     * extra_margin - An extra amount added to all of the image's sides.
       This amount is not visible to the elements returned by make_ref()
       and make_blank_shield()
     * scale - The scale factor for this shield.  Most commonly set to 1/2.54
       when the shield units are in centimeters (because the standard scale
       for shields is one inch per pixel).

    Children MAY override the following methods:
     * make_ref()
     * make_shield() - mostly useful if there are some values for
       `modifier` that should NOT result in a banner being added to the
       base shield image.

    """
    def __init__(self, modifier, ref):
        self.modifier = modifier.upper()
        self.ref = ref

        self.scale = None
        self.width  = 24.0
        self.height = 24.0
        self.extra_margin = 0
        self.ref_spacing = 1.0
        self.ref_size_func = None

        self.wide_threshold = 3
        
        self.fg = COLOR_BLACK
        self.bg = COLOR_WHITE

        self.drawing = None

    @property
    def blank_key(self):
        """Returns a string that uniquely represents the shield blank for this
        object.  The "shield blank" is the part of the shield that will
        always be the same, even if the modifier and ref change.  This
        string should by globally unique."""
        assert False, 'blank_key() wasn\'t overridden in ' + self.__class__.__name__

    @property
    def wide(self):
        return len(self.ref) >= self.wide_threshold

    @property
    def narrow(self):
        return not self.wide

    def standard_ref_series(self, with_E=False):
        """Returns the "standard" series for `ref`.

        That means: "D" for one or two characters, or three characters if
        at least one is a 1; "C" for three characters if none of them are
        1s; and "B" otherwise.  This works for most shield designs with a
        24-pixel narrow variant and a 30-pixel wide variant.

        If `with_E` is True, "E" is returned for single-character ref
        values.

        """
        ref_len = len(self.ref)
        if ref_len <= 1 and with_E:
            return 'E'
        if ref_len <= 2:
            return 'D'
        if ref_len == 3:
            if '1' in self.ref:
                return 'D'
            return 'C'
        return 'B'

    def make_sign_background(self):
        """Requires `corner_radius` attribute."""
        return self.drawing.rect(
            (0, 0), (self.width, self.height), self.corner_radius,
            fill=self.border_color if hasattr(self, 'border_color') else self.fg)
    
    def make_shield(self):
        if self.ref is None:
            return None
        
        if self.modifier == '':
            return self.make_base_shield()
        else:
            shield_svg = self.make_base_shield()
            banner_svgs = [banner(mod, int(shield_svg['width']), fg=self.fg, bg=self.bg,
                                  border=self.border_color if hasattr(self, 'border_color') else None)
                           for mod in self.modifier.split(';')]
            return compose_shields(banner_svgs + [shield_svg])

    def make_base_shield(self):
        base_width = self.width + self.extra_margin * 2
        base_height = self.height + self.extra_margin * 2
        if self.scale is None:
            self.drawing = svgwrite.Drawing(size=(base_width, base_height))
        else:
            self.drawing = svgwrite.Drawing(
                size=(base_width * self.scale, base_height * self.scale),
                viewBox='0 0 {} {}'.format(base_width, base_height))

        self.add_blank_shield_to_defs()
        self.drawing.add(self.drawing.use('#' + self.blank_key))
        ref_g = self.make_ref()
        if ref_g is not None:
            self.add_extra_margin(ref_g)
            self.drawing.add(ref_g)
    
        return self.drawing

    def make_blank_shield(self):
        """Returns an SVG element containing the entirety of the "blank" shield,
        which is the part of the shield that does not depend on the value
        of `ref`.  Every shield with the same value for `blank_key()` (in
        a particular style) should have an identical blank shield."""
        assert False, 'make_blank_shield() MUST be overridden'

    def make_ref(self):
        assert hasattr(self, 'ref_x'), '"ref_x" not defined'
        assert hasattr(self, 'ref_y'), '"ref_y" not defined'
        assert hasattr(self, 'ref_width'), '"ref_width" not defined'
        assert hasattr(self, 'ref_height'), '"ref_height" not defined'
        assert hasattr(self, 'ref_series'), '"ref_series" not defined'
        return render_text(
            self.drawing, self.ref, self.ref_x, self.ref_y,
            self.ref_width, self.ref_height, self.ref_series,
            color=self.fg, spacing=self.ref_spacing,
            size_func=self.ref_size_func)

    def add_blank_shield_to_defs(self):
        defs = set([e['id'] for e in self.drawing.defs.elements])
        if self.blank_key not in defs:
            blank = self.make_blank_shield()
            self.add_extra_margin(blank)
            blank['id'] = self.blank_key
            self.drawing.defs.add(blank)

    def add_extra_margin(self, element):
        if self.extra_margin == 0:
            return

        translate = 'translate({0} {0})'.format(self.extra_margin)
        if 'transform' in element.attribs:
            element['transform'] = translate + ' ' + element['transform']
        else:
            element['transform'] = translate
